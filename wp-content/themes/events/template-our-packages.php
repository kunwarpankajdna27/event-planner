<?php

// Template Name:Our Packages

get_header('inner');


?>

<a id="eltdf-back-to-top" href="#">
    <span class="eltdf-text-stack"> top </span>
</a>

<section class="inner-banner">
	<div class="container-fluid">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="tp-mask-wrap" style="position: absolute; right: 0px; display: block; overflow: visible;"><div class="tp-caption tp-resizeme tp-static-layer" id="slider-11-layer-13" data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['0','0','0','0']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="image" data-basealign="slide" data-responsive_offset="on" data-startslide="0" data-endslide="2" data-frames="[{&quot;delay&quot;:500,&quot;speed&quot;:1000,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;opacity:0;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]" data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; visibility: inherit; transition: none 0s ease 0s; text-align: inherit; line-height: 0px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 0px; font-weight: 400; font-size: 10px; white-space: nowrap; min-height: 0px; min-width: 0px; max-height: none; max-width: none; opacity: 1; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform-origin: 50% 50% 0px;">
    <img src="http://localhost/events/wp-content/themes/events/images/05-h1-slider-1-img-2.png" alt="a" data-ww="['805px','540px','420px','300px']" data-hh="['322px','216px','168px','120px']" width="805" height="322" data-no-retina="" style="width: 487.293px; height: 194.917px; transition: none 0s ease 0s; text-align: inherit; line-height: 0px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 0px; font-weight: 400; font-size: 9px;">
</div></div>

<section class="our-packages-block">
	<div class="container-fluid">
		<div class="container">
			<div class="row">

				<?php

	                $arraydata = array(
	                  'post_type' => 'our_packages' ,
	                  'post_status' => 'publish' ,
	                  'order' => 'DESC' ,
	                  'orderby' => 'data',

	                );

	                $post_data = new WP_Query($arraydata);
	                $i =1;
	                while( $post_data->have_posts() ) 
	                {
	                  $post_data->the_post();

                ?>
				
				<div class="col-md-4 col-xs-12 col-lg-4 col-sm-12 singlePackageBlock">
					<div class="singlePackage<?php echo $i ?>">
						<table class="table table-striped table-responsive">
							<thead>
								<tr>
									<th colspan="2"><h3><?php echo strtoupper(get_the_title()); ?><span> Plan</span></h3></th>
								</tr>
								<tr>
									<th colspan="2"><h4>Price - <?php echo get_field('price',get_the_id()) ?>/- ₹</h4></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Traditional Photography</td>
									<td><?php 
											echo get_field('traditional_photography',get_the_id()) ? get_field('traditional_photography',get_the_id()) : '-----'
										?>
									</td>
								</tr>
								<tr>
									<td>Candid Photography</td>
									<td>
										<?php 
											echo get_field('candid_photography',get_the_id()) ? get_field('candid_photography',get_the_id()) : '-----'
										?>
									</td>
								</tr>
								<tr>
									<td>Traditional Videography (Full HD)</td>
									<td>
										<?php 
											echo get_field('traditional_videography',get_the_id()) ? get_field('traditional_videography',get_the_id()) : '-----'
										?>
									</td>
								</tr>
								<tr>
									<td>Candid Videography</td>
									<td>
										<?php 
											echo get_field('candid_videography',get_the_id()) ? get_field('candid_videography',get_the_id()) : '-----'
										?>
									</td>
								</tr>
								<tr>
									<td>Pre Wedding Shoot</td>
									<td>
										<?php 
											echo get_field('pre_wedding_shoot',get_the_id()) ? get_field('pre_wedding_shoot',get_the_id()) : '-----'
										?>
									</td>
								</tr>
								<tr>
									<td>Post Wedding Shot</td>
									<td>
										<?php 
											echo get_field('post_wedding_shoot',get_the_id()) ? get_field('post_wedding_shoot',get_the_id()) : '-----'
										?>
									</td>
								</tr>
								<tr>
									<td>Specail Equipment</td>
									<td><?php 
											echo get_field('specail_equipment',get_the_id()) ? get_field('specail_equipment',get_the_id()) : '-----'
										?>
									</td>
								</tr>
								<tr>
									<?php
										$heightfield = get_field('wedding_highlight',get_the_id()); 
									?>
									<td>Wedding Highlight (<?php echo $heightfield['minutes'] ?> min.)</td>
									<td>
										<?php  
											echo $heightfield['highlight']!="" ? $heightfield['highlight'] : '-----'
										?>
									</td>
								</tr>
								<tr>
									<td>Blu Ray Disc (or) Pen drive</td>
									<td>
										<?php  
											echo get_field('pen_drive',get_the_id()) ? get_field('pen_drive',get_the_id()) : '-----'
										?>
									</td>
								</tr>
								<tr>
									<td>Non Tearable Album Traditional</td>
									<td>
										<?php  
											echo get_field('non_tearable_album_traditional',get_the_id()) ? get_field('non_tearable_album_traditional',get_the_id()) : '-----'
										?>
									</td>
								</tr>
								<tr>
									<td>Non Tearable Album Candid</td>
									<td>
										<?php  
											echo get_field('non_tearable_album_candid',get_the_id()) ? get_field('non_tearable_album_candid',get_the_id()) : '-----'
										?>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

				<?php 
				if ($i==3){
				
				?>

				<div class='col-md-12 mb-5 mt-5 text-center'>
					<img width='37' height='27' src='http://localhost/events/wp-content/themes/events/images/04-h1-custom-icon-img-1.png' class='attachment-full size-full' alt='a'>
				</div>

				<?php
				}
				if ($i==6)
				{
					$i=1;
				}
				else
				{
					$i++;
				}
				}
				?>

			</div>
		</div>
	</div>
</section>

<?php

get_footer();

?>