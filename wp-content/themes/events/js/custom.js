//<![CDATA[
var gtm4wp_datalayer_name = "dataLayer";
var dataLayer = dataLayer || [];
//]]>


var mejsL10n = {
language: "en",
strings: {
    "mejs.install-flash": "You are using a browser that does not have Flash player enabled or installed. Please turn on your Flash player plugin or download the latest version from https:\/\/get.adobe.com\/flashplayer\/",
    "mejs.fullscreen-off": "Turn off Fullscreen",
    "mejs.fullscreen-on": "Go Fullscreen",
    "mejs.download-video": "Download Video",
    "mejs.fullscreen": "Fullscreen",
    "mejs.time-jump-forward": ["Jump forward 1 second", "Jump forward %1 seconds"],
    "mejs.loop": "Toggle Loop",
    "mejs.play": "Play",
    "mejs.pause": "Pause",
    "mejs.close": "Close",
    "mejs.time-slider": "Time Slider",
    "mejs.time-help-text": "Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.",
    "mejs.time-skip-back": ["Skip back 1 second", "Skip back %1 seconds"],
    "mejs.captions-subtitles": "Captions\/Subtitles",
    "mejs.captions-chapters": "Chapters",
    "mejs.none": "None",
    "mejs.mute-toggle": "Mute Toggle",
    "mejs.volume-help-text": "Use Up\/Down Arrow keys to increase or decrease volume.",
    "mejs.unmute": "Unmute",
    "mejs.mute": "Mute",
    "mejs.volume-slider": "Volume Slider",
    "mejs.video-player": "Video Player",
    "mejs.audio-player": "Audio Player",
    "mejs.ad-skip": "Skip ad",
    "mejs.ad-skip-info": ["Skip in 1 second", "Skip in %1 seconds"],
    "mejs.source-chooser": "Source Chooser",
    "mejs.stop": "Stop",
    "mejs.speed-rate": "Speed Rate",
    "mejs.live-broadcast": "Live Broadcast",
    "mejs.afrikaans": "Afrikaans",
    "mejs.albanian": "Albanian",
    "mejs.arabic": "Arabic",
    "mejs.belarusian": "Belarusian",
    "mejs.bulgarian": "Bulgarian",
    "mejs.catalan": "Catalan",
    "mejs.chinese": "Chinese",
    "mejs.chinese-simplified": "Chinese (Simplified)",
    "mejs.chinese-traditional": "Chinese (Traditional)",
    "mejs.croatian": "Croatian",
    "mejs.czech": "Czech",
    "mejs.danish": "Danish",
    "mejs.dutch": "Dutch",
    "mejs.english": "English",
    "mejs.estonian": "Estonian",
    "mejs.filipino": "Filipino",
    "mejs.finnish": "Finnish",
    "mejs.french": "French",
    "mejs.galician": "Galician",
    "mejs.german": "German",
    "mejs.greek": "Greek",
    "mejs.haitian-creole": "Haitian Creole",
    "mejs.hebrew": "Hebrew",
    "mejs.hindi": "Hindi",
    "mejs.hungarian": "Hungarian",
    "mejs.icelandic": "Icelandic",
    "mejs.indonesian": "Indonesian",
    "mejs.irish": "Irish",
    "mejs.italian": "Italian",
    "mejs.japanese": "Japanese",
    "mejs.korean": "Korean",
    "mejs.latvian": "Latvian",
    "mejs.lithuanian": "Lithuanian",
    "mejs.macedonian": "Macedonian",
    "mejs.malay": "Malay",
    "mejs.maltese": "Maltese",
    "mejs.norwegian": "Norwegian",
    "mejs.persian": "Persian",
    "mejs.polish": "Polish",
    "mejs.portuguese": "Portuguese",
    "mejs.romanian": "Romanian",
    "mejs.russian": "Russian",
    "mejs.serbian": "Serbian",
    "mejs.slovak": "Slovak",
    "mejs.slovenian": "Slovenian",
    "mejs.spanish": "Spanish",
    "mejs.swahili": "Swahili",
    "mejs.swedish": "Swedish",
    "mejs.tagalog": "Tagalog",
    "mejs.thai": "Thai",
    "mejs.turkish": "Turkish",
    "mejs.ukrainian": "Ukrainian",
    "mejs.vietnamese": "Vietnamese",
    "mejs.welsh": "Welsh",
    "mejs.yiddish": "Yiddish",
},
};

/* <![CDATA[ */
var _wpmejsSettings = { pluginPath: "\/wp-includes\/js\/mediaelement\/", classPrefix: "mejs-", stretching: "responsive" };
/* ]]> */

//<![CDATA[
dataLayer.push({ pagePostType: "frontpage", pagePostType2: "single-page", pagePostAuthor: "Tyler Clark" }); //]]>



function setREVStartSize(e) {
try {
    e.c = jQuery(e.c);
    var i = jQuery(window).width(),
        t = 9999,
        r = 0,
        n = 0,
        l = 0,
        f = 0,
        s = 0,
        h = 0;
    if (
        (e.responsiveLevels &&
            (jQuery.each(e.responsiveLevels, function (e, f) {
                f > i && ((t = r = f), (l = e)), i > f && f > r && ((r = f), (n = e));
            }),
            t > r && (l = n)),
        (f = e.gridheight[l] || e.gridheight[0] || e.gridheight),
        (s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth),
        (h = i / s),
        (h = h > 1 ? 1 : h),
        (f = Math.round(h * f)),
        "fullscreen" == e.sliderLayout)
    ) {
        var u = (e.c.width(), jQuery(window).height());
        if (void 0 != e.fullScreenOffsetContainer) {
            var c = e.fullScreenOffsetContainer.split(",");
            if (c)
                jQuery.each(c, function (e, i) {
                    u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u;
                }),
                    e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0
                        ? (u -= (jQuery(window).height() * parseInt(e.fullScreenOffset, 0)) / 100)
                        : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0));
        }
        f = u;
    } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
    e.c.closest(".rev_slider_wrapper").css({ height: f });
} catch (d) {
    console.log("Failure at Presize of Slider:" + d);
}
}

var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
var htmlDivCss = "";
if (htmlDiv) {
    htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
} else {
    var htmlDiv = document.createElement("div");
    htmlDiv.innerHTML = "<style>" + htmlDivCss + "";
    document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
}

if (setREVStartSize !== undefined)
setREVStartSize({
    c: "#rev_slider_11_1",
    responsiveLevels: [1920, 1441, 1025, 480],
    gridwidth: [1100, 1100, 600, 300],
    gridheight: [750, 600, 600, 500],
    sliderLayout: "fullscreen",
    fullScreenAutoWidth: "off",
    fullScreenAlignForce: "off",
    fullScreenOffsetContainer: "",
    fullScreenOffset: "",
});

var revapi11, tpj;
(function () {
if (!/loaded|interactive|complete/.test(document.readyState)) document.addEventListener("DOMContentLoaded", onLoad);
else onLoad();
function onLoad() {
    if (tpj === undefined) {
        tpj = jQuery;
        if ("off" == "on") tpj.noConflict();
    }
    if (tpj("#rev_slider_11_1").revolution == undefined) {
        revslider_showDoubleJqueryError("#rev_slider_11_1");
    } else {
        revapi11 = tpj("#rev_slider_11_1")
            .show()
            .revolution({
                sliderType: "standard",
                jsFileLocation: "//gretnagreen.qodeinteractive.com/wp-content/plugins/revslider/public/assets/js/",
                sliderLayout: "fullscreen",
                dottedOverlay: "none",
                delay: 1300,
                navigation: {
                    onHoverStop: "off",
                },
                viewPort: {
                    enable: true,
                    outof: "wait",
                    visible_area: "99%",
                    presize: false,
                },
                responsiveLevels: [1920, 1441, 1025, 480],
                visibilityLevels: [1920, 1441, 1025, 480],
                gridwidth: [1100, 1100, 600, 300],
                gridheight: [750, 600, 600, 500],
                lazyType: "none",
                shadow: 0,
                spinner: "off",
                stopLoop: "off",
                stopAfterLoops: -1,
                stopAtSlide: -1,
                shuffle: "off",
                autoHeight: "off",
                fullScreenAutoWidth: "off",
                fullScreenAlignForce: "off",
                fullScreenOffsetContainer: "",
                fullScreenOffset: "",
                disableProgressBar: "on",
                hideThumbsOnMobile: "off",
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                debugMode: false,
                fallbacks: {
                    simplifyAll: "off",
                    nextSlideOnWindowFocus: "off",
                    disableFocusListener: false,
                },
            });
    } /* END OF revapi call */
} /* END OF ON LOAD FUNCTION */
})(); /* END OF WRAPPING FUNCTION */