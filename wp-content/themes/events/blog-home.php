<!DOCTYPE html>
<html lang="en-US"><head><meta property="og:url" content="https://gretnagreen.qodeinteractive.com/blog-home/"><meta property="og:type" content="article"><meta property="og:title" content="Blog Home"><meta property="og:description" content=""><meta property="og:image" content="https://gretnagreen.qodeinteractive.com/wp-content/uploads/2019/04/open_graph.jpg"><meta charset="UTF-8"><link rel="profile" href="https://gmpg.org/xfn/11"><meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=yes"><title>Blog Home &ndash; GretnaGreen</title><script data-cfasync="false" type="text/javascript">//<![CDATA[
	var gtm4wp_datalayer_name = "dataLayer";
	var dataLayer = dataLayer || [];
//]]>
</script><link rel="dns-prefetch" href="//toolbar.qodeinteractive.com"><link rel="dns-prefetch" href="//maps.googleapis.com"><link rel="dns-prefetch" href="//fonts.googleapis.com"><link rel="dns-prefetch" href="//s.w.org"><link rel="alternate" type="application/rss+xml" title="GretnaGreen &raquo; Feed" href="https://gretnagreen.qodeinteractive.com/feed/"><link rel="alternate" type="application/rss+xml" title="GretnaGreen &raquo; Comments Feed" href="https://gretnagreen.qodeinteractive.com/comments/feed/"><script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/gretnagreen.qodeinteractive.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.2"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script><style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style><link rel="stylesheet" id="wp-block-library-css" href="css/block-library-style.min.css" type="text/css" media="all"><link rel="stylesheet" id="contact-form-7-css" href="css/css-styles.css" type="text/css" media="all"><link rel="stylesheet" id="rabbit_css-css" href="https://toolbar.qodeinteractive.com/_toolbar/assets/css/rbt-modules.css?ver=5.2.2" type="text/css" media="all"><link rel="stylesheet" id="rs-plugin-settings-css" href="css/css-settings.css" type="text/css" media="all"><style id="rs-plugin-settings-inline-css" type="text/css">
#rs-demo-id {}
</style><style id="woocommerce-inline-inline-css" type="text/css">
.woocommerce form .form-row .required { visibility: visible; }
</style><link rel="stylesheet" id="gretnagreen-elated-default-style-css" href="css/gretnagreen-style.css" type="text/css" media="all"><link rel="stylesheet" id="gretnagreen-elated-modules-css" href="css/css-modules.min.css" type="text/css" media="all"><link rel="stylesheet" id="eltdf-dripicons-css" href="css/dripicons-dripicons.css" type="text/css" media="all"><link rel="stylesheet" id="eltdf-font_elegant-css" href="css/elegant-icons-style.min.css" type="text/css" media="all"><link rel="stylesheet" id="eltdf-font_awesome-css" href="css/css-fontawesome-all.min.css" type="text/css" media="all"><link rel="stylesheet" id="eltdf-ion_icons-css" href="css/css-ionicons.min.css" type="text/css" media="all"><link rel="stylesheet" id="eltdf-linea_icons-css" href="css/linea-icons-style.css" type="text/css" media="all"><link rel="stylesheet" id="eltdf-linear_icons-css" href="css/linear-icons-style.css" type="text/css" media="all"><link rel="stylesheet" id="eltdf-simple_line_icons-css" href="css/simple-line-icons-simple-line-icons.css" type="text/css" media="all"><link rel="stylesheet" id="mediaelement-css" href="css/mediaelement-mediaelementplayer-legacy.min.css" type="text/css" media="all"><link rel="stylesheet" id="wp-mediaelement-css" href="css/mediaelement-wp-mediaelement.min.css" type="text/css" media="all"><link rel="stylesheet" id="gretnagreen-elated-woo-css" href="css/css-woocommerce.min.css" type="text/css" media="all"><style id="gretnagreen-elated-woo-inline-css" type="text/css">
.page-id-562 .eltdf-content .eltdf-content-inner > .eltdf-container > .eltdf-container-inner, .page-id-562 .eltdf-content .eltdf-content-inner > .eltdf-full-width > .eltdf-full-width-inner { padding: 30px 0 70px 0;}.page-id-562 .eltdf-content .eltdf-content-inner > .eltdf-container > .eltdf-container-inner, .page-id-562 .eltdf-content .eltdf-content-inner > .eltdf-full-width > .eltdf-full-width-inner { padding: 30px 0 70px 0;}
</style><link rel="stylesheet" id="gretnagreen-elated-woo-responsive-css" href="css/css-woocommerce-responsive.min.css" type="text/css" media="all"><link rel="stylesheet" id="gretnagreen-elated-style-dynamic-css" href="css/css-style_dynamic.css" type="text/css" media="all"><link rel="stylesheet" id="gretnagreen-elated-modules-responsive-css" href="css/css-modules-responsive.min.css" type="text/css" media="all"><link rel="stylesheet" id="gretnagreen-elated-style-dynamic-responsive-css" href="css/css-style_dynamic_responsive.css" type="text/css" media="all"><link rel="stylesheet" id="gretnagreen-elated-google-fonts-css" href="https://fonts.googleapis.com/css?family=Playfair+Display%3A300%2C400%2C400i%2C700%7COswald%3A300%2C400%2C400i%2C700&amp;subset=latin-ext&amp;ver=1.0.0" type="text/css" media="all"><!--[if lt IE 9]>
<link rel='stylesheet' id='vc_lte_ie9-css'  href='https://gretnagreen.qodeinteractive.com/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css?ver=6.0.5' type='text/css' media='screen' />
<![endif]--><script src="js/minify-eff97.js"></script><script type="text/javascript">
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"https:\/\/gretnagreen.qodeinteractive.com\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
/* ]]> */
</script><script src="js/minify-f1253.js"></script><script type="text/javascript">
var mejsL10n = {"language":"en","strings":{"mejs.install-flash":"You are using a browser that does not have Flash player enabled or installed. Please turn on your Flash player plugin or download the latest version from https:\/\/get.adobe.com\/flashplayer\/","mejs.fullscreen-off":"Turn off Fullscreen","mejs.fullscreen-on":"Go Fullscreen","mejs.download-video":"Download Video","mejs.fullscreen":"Fullscreen","mejs.time-jump-forward":["Jump forward 1 second","Jump forward %1 seconds"],"mejs.loop":"Toggle Loop","mejs.play":"Play","mejs.pause":"Pause","mejs.close":"Close","mejs.time-slider":"Time Slider","mejs.time-help-text":"Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.","mejs.time-skip-back":["Skip back 1 second","Skip back %1 seconds"],"mejs.captions-subtitles":"Captions\/Subtitles","mejs.captions-chapters":"Chapters","mejs.none":"None","mejs.mute-toggle":"Mute Toggle","mejs.volume-help-text":"Use Up\/Down Arrow keys to increase or decrease volume.","mejs.unmute":"Unmute","mejs.mute":"Mute","mejs.volume-slider":"Volume Slider","mejs.video-player":"Video Player","mejs.audio-player":"Audio Player","mejs.ad-skip":"Skip ad","mejs.ad-skip-info":["Skip in 1 second","Skip in %1 seconds"],"mejs.source-chooser":"Source Chooser","mejs.stop":"Stop","mejs.speed-rate":"Speed Rate","mejs.live-broadcast":"Live Broadcast","mejs.afrikaans":"Afrikaans","mejs.albanian":"Albanian","mejs.arabic":"Arabic","mejs.belarusian":"Belarusian","mejs.bulgarian":"Bulgarian","mejs.catalan":"Catalan","mejs.chinese":"Chinese","mejs.chinese-simplified":"Chinese (Simplified)","mejs.chinese-traditional":"Chinese (Traditional)","mejs.croatian":"Croatian","mejs.czech":"Czech","mejs.danish":"Danish","mejs.dutch":"Dutch","mejs.english":"English","mejs.estonian":"Estonian","mejs.filipino":"Filipino","mejs.finnish":"Finnish","mejs.french":"French","mejs.galician":"Galician","mejs.german":"German","mejs.greek":"Greek","mejs.haitian-creole":"Haitian Creole","mejs.hebrew":"Hebrew","mejs.hindi":"Hindi","mejs.hungarian":"Hungarian","mejs.icelandic":"Icelandic","mejs.indonesian":"Indonesian","mejs.irish":"Irish","mejs.italian":"Italian","mejs.japanese":"Japanese","mejs.korean":"Korean","mejs.latvian":"Latvian","mejs.lithuanian":"Lithuanian","mejs.macedonian":"Macedonian","mejs.malay":"Malay","mejs.maltese":"Maltese","mejs.norwegian":"Norwegian","mejs.persian":"Persian","mejs.polish":"Polish","mejs.portuguese":"Portuguese","mejs.romanian":"Romanian","mejs.russian":"Russian","mejs.serbian":"Serbian","mejs.slovak":"Slovak","mejs.slovenian":"Slovenian","mejs.spanish":"Spanish","mejs.swahili":"Swahili","mejs.swedish":"Swedish","mejs.tagalog":"Tagalog","mejs.thai":"Thai","mejs.turkish":"Turkish","mejs.ukrainian":"Ukrainian","mejs.vietnamese":"Vietnamese","mejs.welsh":"Welsh","mejs.yiddish":"Yiddish"}};
</script><script src="js/minify-864c2.js"></script><script type="text/javascript">
/* <![CDATA[ */
var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/","classPrefix":"mejs-","stretching":"responsive"};
/* ]]> */
</script><link rel="https://api.w.org/" href="https://gretnagreen.qodeinteractive.com/wp-json/"><link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://gretnagreen.qodeinteractive.com/xmlrpc.php?rsd"><link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://gretnagreen.qodeinteractive.com/wp-includes/wlwmanifest.xml"><meta name="generator" content="WordPress 5.2.2"><meta name="generator" content="WooCommerce 3.4.1"><link rel="canonical" href="https://gretnagreen.qodeinteractive.com/blog-home/"><link rel="shortlink" href="https://gretnagreen.qodeinteractive.com/?p=562"><link rel="alternate" type="application/json+oembed" href="https://gretnagreen.qodeinteractive.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fgretnagreen.qodeinteractive.com%2Fblog-home%2F"><link rel="alternate" type="text/xml+oembed" href="https://gretnagreen.qodeinteractive.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fgretnagreen.qodeinteractive.com%2Fblog-home%2F&amp;format=xml"><script data-cfasync="false" type="text/javascript">//<![CDATA[
	dataLayer.push({"pagePostType":"page","pagePostType2":"single-page","pagePostAuthor":"Tyler Clark"});//]]>
</script><script data-cfasync="false">//<![CDATA[
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.'+'js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KLJLSX7');//]]>
</script><noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript><meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."><meta name="generator" content="Powered by Slider Revolution 5.4.7.2 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface."><link rel="icon" href="favicons/05-favicon.png" sizes="32x32"><link rel="icon" href="favicons/05-favicon.png" sizes="192x192"><link rel="apple-touch-icon-precomposed" href="favicons/05-favicon.png"><meta name="msapplication-TileImage" content="https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/05/favicon.png"><script type="text/javascript">function setREVStartSize(e){									
						try{ e.c=jQuery(e.c);var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
							if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})					
						}catch(d){console.log("Failure at Presize of Slider:"+d)}						
					};</script><style type="text/css" id="wp-custom-css">
			.row-with-map-links .wpb_text_column a:hover {
	color: #fbb8ac!important;
}		</style><noscript><style> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript></head><body class="page-template page-template-blog-centered page page-id-562 gretnagreen-core-1.0 woocommerce-no-js gretna green-ver-1.0 eltdf-grid-1100 eltdf-wide-dropdown-menu-content-in-grid eltdf-sticky-header-on-scroll-down-up eltdf-dropdown-default eltdf-header-bottom eltdf-menu-area-shadow-disable eltdf-menu-area-in-grid-shadow-disable eltdf-menu-area-border-disable eltdf-menu-area-in-grid-border-disable eltdf-logo-area-border-disable eltdf-header-vertical-shadow-disable eltdf-header-vertical-border-disable eltdf-side-menu-slide-from-right eltdf-woocommerce-columns-3 eltdf-woo-small-space eltdf-woo-pl-info-below-image eltdf-woo-single-thumb-below-image eltdf-woo-single-has-pretty-photo eltdf-default-mobile-header eltdf-sticky-up-mobile-header eltdf-fullscreen-search eltdf-search-fade wpb-js-composer js-comp-ver-6.0.5 vc_responsive" itemscope itemtype="http://schema.org/WebPage">
<section class="eltdf-side-menu"><a class="eltdf-close-side-menu eltdf-close-side-menu-svg-path" href="#">
<svg class="eltdf-close-icon" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewbox="0 0 17 17" enable-background="new 0 0 17 17" xml:space="preserve"><line class="eltdf-close-line-1" stroke-width="2" stroke-miterlimit="10" x1="16" y1="1.2" x2="1" y2="15.8"></line><line class="eltdf-close-line-2" stroke-width="2" stroke-miterlimit="10" x1="1" y1="1.2" x2="16" y2="15.8"></line></svg></a>
<div id="text-6" class="widget eltdf-sidearea widget_text"> <div class="textwidget"><div class="eltdf-invitation-holder ">
<div class="eltdf-invitation-image" style="background-image: url(https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/05/sidearea-img-1.jpg)">
<img width="276" height="329" src="images/05-sidearea-img-1.jpg" class="attachment-full size-full" alt="a" srcset="https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/05/sidearea-img-1.jpg 276w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/05/sidearea-img-1-252x300.jpg 252w" sizes="(max-width: 276px) 100vw, 276px"></div>
<div class="eltdf-invitation-text-holder">
<div class="eltdf-invitation-text-holder-inner">
<span>
<img width="37" height="27" src="images/04-h1-custom-icon-img-1.png" class="attachment-full size-full" alt="a"></span>
<h2 class="eltdf-invitation-title">Emma Marie</h2>
<p class="eltdf-invitation-separator">and</p>
<h2 class="eltdf-invitation-title">Roko Button</h2>
<p class="eltdf-invitation-date">15. October 2018.</p>
<p class="eltdf-invitation-text">at 7 o&rsquo;clock in the evening<br>
Villa Macadamiana second at Marianberg<br>
Hilltop Dr Menomonee Falls, WI 53051 US</p>
<p class="eltdf-invitation-bottom-text">
<a itemprop="url" href="https://www.google.rs/maps/place/Hilltop+Dr,+Menomonee+Falls,+WI+53051,+USA/@43.1623818,-88.1004267,17z/data=!3m1!4b1!4m5!3m4!1s0x8804fe033b89cdb9:0x7e975b12876d2d38!8m2!3d43.1623779!4d-88.098238" target="_blank" class="eltdf-btn eltdf-btn-medium eltdf-btn-condensed" rel="noopener noreferrer"> <span class="eltdf-btn-text">LOCATION</span> </a> </p>
</div>
</div>
</div>
</div>
</div> <div class="eltdf-side-area-bottom">
<div class="widget eltdf-social-icons-group-widget text-align-center"> <a class="eltdf-social-icon-widget-holder eltdf-icon-has-hover" data-hover-color="#fbb8ac" style="color: #212121;;font-size: 13px;margin: 0 10px;" href="https://www.tumblr.com/" target="_blank">
<span class="eltdf-social-icon-widget fab fa-tumblr"></span> </a>
<a class="eltdf-social-icon-widget-holder eltdf-icon-has-hover" data-hover-color="#fbb8ac" style="color: #212121;;font-size: 13px;margin: 0 10px;" href="https://www.pinterest.com/elatedhemes/" target="_blank">
<span class="eltdf-social-icon-widget fab fa-pinterest-p"></span> </a>
<a class="eltdf-social-icon-widget-holder eltdf-icon-has-hover" data-hover-color="#fbb8ac" style="color: #212121;;font-size: 13px;margin: 0 10px;" href="https://www.facebook.com/Elated-Themes-1889004714470831/" target="_blank">
<span class="eltdf-social-icon-widget fab fa-facebook-f"></span> </a>
<a class="eltdf-social-icon-widget-holder eltdf-icon-has-hover" data-hover-color="#fbb8ac" style="color: #212121;;font-size: 13px;margin: 0 10px;" href="https://www.instagram.com/elatedthemes/" target="_blank">
<span class="eltdf-social-icon-widget fab fa-instagram"></span> </a>
</div> </div>
</section><div class="eltdf-wrapper">
<div class="eltdf-wrapper-inner">
<header class="eltdf-mobile-header"><div class="eltdf-mobile-header-inner">
<div class="eltdf-mobile-header-holder">
<div class="eltdf-grid">
<div class="eltdf-vertical-align-containers">
<div class="eltdf-vertical-align-containers">
<div class="eltdf-mobile-menu-opener eltdf-mobile-menu-opener-icon-pack">
<a href="javascript:void(0)">
<span class="eltdf-mobile-menu-icon">
<span aria-hidden="true" class="eltdf-icon-font-elegant icon_menu "></span> </span>
</a>
</div>
<div class="eltdf-position-center"><div class="eltdf-position-center-inner">
<div class="eltdf-mobile-logo-wrapper">
<a itemprop="url" href="gretnagreen.qodeinteractive.html" style="height: 21px">
<img itemprop="image" src="images/05-logo-standard-header-mobile.png" width="350" height="43" alt="Mobile Logo"></a>
</div>
</div>
</div>
<div class="eltdf-position-right"><div class="eltdf-position-right-inner">
</div>
</div>
</div>
</div>
</div>
</div>
<nav class="eltdf-mobile-nav"><div class="eltdf-grid">
<ul id="menu-main-menu-navigation" class=""><li id="mobile-menu-item-10" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children eltdf-active-item has_sub"><a href="#" class=" current  eltdf-mobile-no-link"><span>Home</span></a><span class="mobile_arrow"><i class="eltdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
<ul class="sub_menu"><li id="mobile-menu-item-251" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home "><a href="gretnagreen.qodeinteractive.html" class=""><span>Main Home</span></a></li>
<li id="mobile-menu-item-1165" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="announcement-home.html" class=""><span>Announcement Home</span></a></li>
<li id="mobile-menu-item-1484" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="pinterest-gallery.html" class=""><span>Pinterest Gallery</span></a></li>
<li id="mobile-menu-item-702" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="event-home.html" class=""><span>Event Home</span></a></li>
<li id="mobile-menu-item-701" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-562 current_page_item "><a href="blog-home.html" class=""><span>Blog Home</span></a></li>
<li id="mobile-menu-item-700" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="cake-shop.html" class=""><span>Cake Shop</span></a></li>
<li id="mobile-menu-item-861" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="invitation-home.html" class=""><span>Invitation Home</span></a></li>
<li id="mobile-menu-item-1047" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="wedding-home.html" class=""><span>Wedding Home</span></a></li>
<li id="mobile-menu-item-1021" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="venue-home.html" class=""><span>Venue Home</span></a></li>
<li id="mobile-menu-item-730" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="bridal-dresses.html" class=""><span>Bridal Dresses</span></a></li>
<li id="mobile-menu-item-396" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="photo-album.html" class=""><span>Photo Album</span></a></li>
<li id="mobile-menu-item-1108" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="wedding-planner.html" class=""><span>Wedding Planner</span></a></li>
<li id="mobile-menu-item-3335" class="menu-item menu-item-type-post_type menu-item-object-page "><a target="_blank" href="landing.html" class=""><span>Landing</span></a></li>
</ul></li>
<li id="mobile-menu-item-11" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub"><a href="#" class=" eltdf-mobile-no-link"><span>Pages</span></a><span class="mobile_arrow"><i class="eltdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
<ul class="sub_menu"><li id="mobile-menu-item-1926" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="our-story.html" class=""><span>Our Story</span></a></li>
<li id="mobile-menu-item-1750" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="meet-the-team.html" class=""><span>Meet The Team</span></a></li>
<li id="mobile-menu-item-1885" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="our-services.html" class=""><span>Our Services</span></a></li>
<li id="mobile-menu-item-1748" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="gift-registry.html" class=""><span>Gift Registry</span></a></li>
<li id="mobile-menu-item-2035" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="rsvp.html" class=""><span>RSVP</span></a></li>
<li id="mobile-menu-item-1749" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="contact-us.html" class=""><span>Contact Us</span></a></li>
<li id="mobile-menu-item-4060" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="404-error-page.html" class=""><span>404 Error Page</span></a></li>
</ul></li>
<li id="mobile-menu-item-12" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub"><a href="#" class=" eltdf-mobile-no-link"><span>Gallery</span></a><span class="mobile_arrow"><i class="eltdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
<ul class="sub_menu"><li id="mobile-menu-item-1198" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children  has_sub"><a href="portfolio-standard.html" class=""><span>Standard</span></a><span class="mobile_arrow"><i class="eltdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
<ul class="sub_menu"><li id="mobile-menu-item-3078" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="in-grid.html" class=""><span>In Grid</span></a></li>
<li id="mobile-menu-item-3079" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="wide.html" class=""><span>Wide</span></a></li>
</ul></li>
<li id="mobile-menu-item-1197" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children  has_sub"><a href="portfolio-gallery.html" class=""><span>Gallery</span></a><span class="mobile_arrow"><i class="eltdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
<ul class="sub_menu"><li id="mobile-menu-item-3087" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="in-grid.html" class=""><span>In Grid</span></a></li>
<li id="mobile-menu-item-3089" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="joined-in-grid.html" class=""><span>Joined / In Grid</span></a></li>
<li id="mobile-menu-item-3090" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="wide.html" class=""><span>Wide</span></a></li>
<li id="mobile-menu-item-3088" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="joined-wide.html" class=""><span>Joined / Wide</span></a></li>
</ul></li>
<li id="mobile-menu-item-3097" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children  has_sub"><a href="portfolio-masonry.html" class=""><span>Masonry</span></a><span class="mobile_arrow"><i class="eltdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
<ul class="sub_menu"><li id="mobile-menu-item-3101" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="in-grid.html" class=""><span>In Grid</span></a></li>
<li id="mobile-menu-item-3100" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="masonry-joined.html" class=""><span>Joined / In Grid</span></a></li>
<li id="mobile-menu-item-3098" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="masonry-wide.html" class=""><span>Wide</span></a></li>
<li id="mobile-menu-item-3099" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="joined-wide.html" class=""><span>Joined / Wide</span></a></li>
</ul></li>
<li id="mobile-menu-item-3107" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children  has_sub"><a href="portfolio-pinterest.html" class=""><span>Pinterest</span></a><span class="mobile_arrow"><i class="eltdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
<ul class="sub_menu"><li id="mobile-menu-item-3108" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="in-grid.html" class=""><span>In Grid</span></a></li>
<li id="mobile-menu-item-3110" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="wide.html" class=""><span>Wide</span></a></li>
</ul></li>
<li id="mobile-menu-item-2168" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="portfolio-carousel.html" class=""><span>Carousel</span></a></li>
<li id="mobile-menu-item-1183" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub"><a href="#" class=" eltdf-mobile-no-link"><span>Gallery Layouts</span></a><span class="mobile_arrow"><i class="eltdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
<ul class="sub_menu"><li id="mobile-menu-item-1203" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="two-columns.html" class=""><span>Two Columns</span></a></li>
<li id="mobile-menu-item-1202" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="three-columns.html" class=""><span>Three Columns</span></a></li>
<li id="mobile-menu-item-1201" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="three-columns-wide.html" class=""><span>Three Columns Wide</span></a></li>
<li id="mobile-menu-item-1207" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="four-columns.html" class=""><span>Four Columns</span></a></li>
<li id="mobile-menu-item-1200" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="four-columns-wide.html" class=""><span>Four Columns Wide</span></a></li>
<li id="mobile-menu-item-1199" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="five-columns-wide.html" class=""><span>Five Columns Wide</span></a></li>
</ul></li>
<li id="mobile-menu-item-1184" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub"><a href="#" class=" eltdf-mobile-no-link"><span>Gallery Single</span></a><span class="mobile_arrow"><i class="eltdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
<ul class="sub_menu"><li id="mobile-menu-item-1208" class="menu-item menu-item-type-post_type menu-item-object-portfolio-item "><a href="champagne-roses.html" class=""><span>Small Images</span></a></li>
<li id="mobile-menu-item-1210" class="menu-item menu-item-type-post_type menu-item-object-portfolio-item "><a href="spring-wedding.html" class=""><span>Small Slider</span></a></li>
<li id="mobile-menu-item-1211" class="menu-item menu-item-type-post_type menu-item-object-portfolio-item "><a href="diy-vow-books.html" class=""><span>Big Images</span></a></li>
<li id="mobile-menu-item-1213" class="menu-item menu-item-type-post_type menu-item-object-portfolio-item "><a href="romantic-wedding.html" class=""><span>Big Slider</span></a></li>
<li id="mobile-menu-item-1214" class="menu-item menu-item-type-post_type menu-item-object-portfolio-item "><a href="creative-styling-ideas.html" class=""><span>Small Gallery</span></a></li>
<li id="mobile-menu-item-1215" class="menu-item menu-item-type-post_type menu-item-object-portfolio-item "><a href="best-wedding-songs.html" class=""><span>Big Gallery</span></a></li>
<li id="mobile-menu-item-1216" class="menu-item menu-item-type-post_type menu-item-object-portfolio-item "><a href="dreamy-locations.html" class=""><span>Masonry</span></a></li>
</ul></li>
</ul></li>
<li id="mobile-menu-item-13" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub"><a href="#" class=" eltdf-mobile-no-link"><span>Blog</span></a><span class="mobile_arrow"><i class="eltdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
<ul class="sub_menu"><li id="mobile-menu-item-254" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="right-sidebar.html" class=""><span>Right Sidebar</span></a></li>
<li id="mobile-menu-item-253" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="left-sidebar.html" class=""><span>Left Sidebar</span></a></li>
<li id="mobile-menu-item-252" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="without-sidebar.html" class=""><span>Without Sidebar</span></a></li>
<li id="mobile-menu-item-261" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub"><a href="#" class=" eltdf-mobile-no-link"><span>Post Types</span></a><span class="mobile_arrow"><i class="eltdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
<ul class="sub_menu"><li id="mobile-menu-item-260" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="wedding-ceremony.html" class=""><span>Standard</span></a></li>
<li id="mobile-menu-item-259" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="wedding-venues.html" class=""><span>Gallery</span></a></li>
<li id="mobile-menu-item-258" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="engagement-rings.html" class=""><span>Link</span></a></li>
<li id="mobile-menu-item-257" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="dress-ideas.html" class=""><span>Quote</span></a></li>
<li id="mobile-menu-item-256" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="double-wedding.html" class=""><span>Video</span></a></li>
<li id="mobile-menu-item-255" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="white-wedding.html" class=""><span>Audio</span></a></li>
</ul></li>
</ul></li>
<li id="mobile-menu-item-14" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub"><a href="#" class=" eltdf-mobile-no-link"><span>Shop</span></a><span class="mobile_arrow"><i class="eltdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
<ul class="sub_menu"><li id="mobile-menu-item-399" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="product-list.html" class=""><span>Product List</span></a></li>
<li id="mobile-menu-item-403" class="menu-item menu-item-type-post_type menu-item-object-product "><a href="pastel-petals.html" class=""><span>Product Single</span></a></li>
<li id="mobile-menu-item-401" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub"><a href="#" class=" eltdf-mobile-no-link"><span>Shop Layouts</span></a><span class="mobile_arrow"><i class="eltdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
<ul class="sub_menu"><li id="mobile-menu-item-469" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="three-columns.html" class=""><span>Three Columns</span></a></li>
<li id="mobile-menu-item-468" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="four-columns.html" class=""><span>Four Columns</span></a></li>
<li id="mobile-menu-item-467" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="full-width.html" class=""><span>Full Width</span></a></li>
</ul></li>
<li id="mobile-menu-item-402" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub"><a href="#" class=" eltdf-mobile-no-link"><span>Shop Pages</span></a><span class="mobile_arrow"><i class="eltdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
<ul class="sub_menu"><li id="mobile-menu-item-486" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="my-account.html" class=""><span>My account</span></a></li>
<li id="mobile-menu-item-485" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="cart.html" class=""><span>Cart</span></a></li>
<li id="mobile-menu-item-484" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="checkout.html" class=""><span>Checkout</span></a></li>
</ul></li>
</ul></li>
<li id="mobile-menu-item-15" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub"><a href="#" class=" eltdf-mobile-no-link"><span>Elements</span></a><span class="mobile_arrow"><i class="eltdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
<ul class="sub_menu"><li id="mobile-menu-item-1381" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub"><h6><span>Classic</span></h6><span class="mobile_arrow"><i class="eltdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
<ul class="sub_menu"><li id="mobile-menu-item-1360" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="accordions.html" class=""><span>Accordions</span></a></li>
<li id="mobile-menu-item-1380" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="tabs.html" class=""><span>Tabs</span></a></li>
<li id="mobile-menu-item-1364" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="clients.html" class=""><span>Clients</span></a></li>
<li id="mobile-menu-item-1363" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="buttons.html" class=""><span>Buttons</span></a></li>
<li id="mobile-menu-item-1372" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="icon-with-text.html" class=""><span>Icon With Text</span></a></li>
<li id="mobile-menu-item-1371" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="google-maps.html" class=""><span>Google Maps</span></a></li>
<li id="mobile-menu-item-1366" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="contact-form.html" class=""><span>Contact Form</span></a></li>
</ul></li>
<li id="mobile-menu-item-1383" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub"><h6><span>Infographic</span></h6><span class="mobile_arrow"><i class="eltdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
<ul class="sub_menu"><li id="mobile-menu-item-1362" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="blog-list.html" class=""><span>Blog List</span></a></li>
<li id="mobile-menu-item-1379" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="shop-list.html" class=""><span>Shop List</span></a></li>
<li id="mobile-menu-item-1474" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="invitation.html" class=""><span>Invitation</span></a></li>
<li id="mobile-menu-item-1376" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="progress-bar.html" class=""><span>Progress Bar</span></a></li>
<li id="mobile-menu-item-1368" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="counters.html" class=""><span>Counters</span></a></li>
<li id="mobile-menu-item-1367" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="countdown.html" class=""><span>Countdown</span></a></li>
<li id="mobile-menu-item-1374" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="pie-chart.html" class=""><span>Pie Chart</span></a></li>
</ul></li>
<li id="mobile-menu-item-1382" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub"><h6><span>Presentation</span></h6><span class="mobile_arrow"><i class="eltdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
<ul class="sub_menu"><li id="mobile-menu-item-1478" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="item-showcase.html" class=""><span>Item Showcase</span></a></li>
<li id="mobile-menu-item-2763" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="image-gallery.html" class=""><span>Image Gallery</span></a></li>
<li id="mobile-menu-item-1943" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="image-with-text.html" class=""><span>Image With Text</span></a></li>
<li id="mobile-menu-item-1389" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="testimonials.html" class=""><span>Testimonials</span></a></li>
<li id="mobile-menu-item-1388" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="team.html" class=""><span>Team</span></a></li>
<li id="mobile-menu-item-1373" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="parallax-section.html" class=""><span>Parallax Section</span></a></li>
<li id="mobile-menu-item-1390" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="video-button.html" class=""><span>Video Button</span></a></li>
</ul></li>
<li id="mobile-menu-item-1385" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub"><h6><span>Typography</span></h6><span class="mobile_arrow"><i class="eltdf-sub-arrow fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>
<ul class="sub_menu"><li id="mobile-menu-item-1391" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="headings.html" class=""><span>Headings</span></a></li>
<li id="mobile-menu-item-1365" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="columns.html" class=""><span>Columns</span></a></li>
<li id="mobile-menu-item-1377" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="section-title.html" class=""><span>Section Title</span></a></li>
<li id="mobile-menu-item-1361" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="blockquote.html" class=""><span>Blockquote</span></a></li>
<li id="mobile-menu-item-1370" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="dropcaps-highlights.html" class=""><span>Dropcaps &amp; Highlights</span></a></li>
<li id="mobile-menu-item-1378" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="separators.html" class=""><span>Separators</span></a></li>
<li id="mobile-menu-item-1369" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="custom-font.html" class=""><span>Custom Font</span></a></li>
</ul></li>
</ul></li>
</ul></div>
</nav></div>
</header><a id="eltdf-back-to-top" href="#">
<span class="eltdf-text-stack">
top </span>
</a>
<div class="eltdf-content">
<div class="eltdf-content-inner"> <div class="eltdf-slider">
<div class="eltdf-slider-inner">
<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400" rel="stylesheet" property="stylesheet" type="text/css" media="all"><div id="rev_slider_2_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">

<div id="rev_slider_2_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.7.2">
<ul><li data-index="rs-2" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">

<img src="images/images-transparent.png" alt="" title="Blog Home" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina><div class="tp-caption   tp-resizeme" id="slide-2-layer-1" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['0','0','0','0']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="image" data-responsive_offset="on" data-frames='[{"delay":150,"speed":1000,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5;"><img src="images/05-h5-slide-1-img-1.png" alt="a" data-ww="['694px','694px','400px','250px']" data-hh="['222px','222px','128px','80']" width="694" height="222" data-no-retina></div>

<div class="tp-caption   tp-resizeme" id="slide-2-layer-2" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['9','16','8','15']" data-fontsize="['150','160','90','45']" data-lineheight="['160','170','100','55']" data-color="['rgb(233,233,233)','rgba(233,233,233,0.8)','rgba(233,233,233,0.8)','rgba(233,233,233,0.8)']" data-width="['none','none','none','284']" data-height="none" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":300,"split":"words","splitdelay":0.05,"speed":1100,"split_direction":"forward","frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" data-blendmode="multiply" style="z-index: 6; white-space: nowrap; font-size: 150px; line-height: 160px; font-weight: 400; color: #e9e9e9; letter-spacing: 0px;font-family:Playfair Display;">Gretna Green </div>
</li>
</ul><div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div> </div>
<script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss="";
				if(htmlDiv) {
					htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
				}else{
					var htmlDiv = document.createElement("div");
					htmlDiv.innerHTML = "<style>" + htmlDivCss + "";
					document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
				}
			</script><script type="text/javascript">
if (setREVStartSize!==undefined) setREVStartSize(
	{c: '#rev_slider_2_1', responsiveLevels: [1920,1441,1025,480], gridwidth: [1100,1100,600,260], gridheight: [320,320,190,110], sliderLayout: 'auto'});
			
var revapi2,
	tpj;	
(function() {			
	if (!/loaded|interactive|complete/.test(document.readyState)) document.addEventListener("DOMContentLoaded",onLoad); else onLoad();	
	function onLoad() {				
		if (tpj===undefined) { tpj = jQuery; if("off" == "on") tpj.noConflict();}
	if(tpj("#rev_slider_2_1").revolution == undefined){
		revslider_showDoubleJqueryError("#rev_slider_2_1");
	}else{
		revapi2 = tpj("#rev_slider_2_1").show().revolution({
			sliderType:"hero",
			jsFileLocation:"//gretnagreen.qodeinteractive.com/wp-content/plugins/revslider/public/assets/js/",
			sliderLayout:"auto",
			dottedOverlay:"none",
			delay:4000,
			viewPort: {
				enable:true,
				outof:"wait",
				visible_area:"99%",
				presize:false
			},
			responsiveLevels:[1920,1441,1025,480],
			visibilityLevels:[1920,1441,1025,480],
			gridwidth:[1100,1100,600,260],
			gridheight:[320,320,190,110],
			lazyType:"none",
			shadow:0,
			spinner:"off",
			autoHeight:"off",
			disableProgressBar:"on",
			hideThumbsOnMobile:"off",
			hideSliderAtLimit:0,
			hideCaptionAtLimit:0,
			hideAllCaptionAtLilmit:0,
			debugMode:false,
			fallbacks: {
				simplifyAll:"off",
				disableFocusListener:false,
			}
		});
	}; /* END OF revapi call */
	
 }; /* END OF ON LOAD FUNCTION */
}()); /* END OF WRAPPING FUNCTION */
</script></div> </div>
</div>
<div class="eltdf-fullscreen-search-holder">
<a class="eltdf-search-close eltdf-search-close-icon-pack" href="javascript:void(0)">
<span aria-hidden="true" class="eltdf-icon-font-elegant icon_close "></span> </a>
<div class="eltdf-fullscreen-search-table" style="background-image:url(https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/05/search-background-img-1.jpg);">
<div class="eltdf-fullscreen-search-cell">
<div class="eltdf-fullscreen-search-inner">
<form action="https://gretnagreen.qodeinteractive.com/" class="eltdf-fullscreen-search-form" method="get">
<div class="eltdf-form-holder">
<div class="eltdf-form-holder-inner">
<div class="eltdf-field-holder">
<input type="text" placeholder="Search for..." name="s" class="eltdf-search-field" autocomplete="off"></div>
<button type="submit" class="eltdf-search-submit eltdf-search-submit-icon-pack">
<span aria-hidden="true" class="eltdf-icon-font-elegant icon_search "></span> </button>
<div class="eltdf-line"></div>
</div>
</div>
</form>
</div>
</div>
</div>
</div><header class="eltdf-page-header"><div class="eltdf-fixed-wrapper">
<div class="eltdf-menu-area">
<div class="eltdf-grid">
<div class="eltdf-vertical-align-containers">
<div class="eltdf-position-left"><div class="eltdf-position-left-inner">
<nav class="eltdf-main-menu eltdf-drop-down eltdf-default-nav"><ul id="menu-main-menu-navigation-1" class="clearfix"><li id="nav-menu-item-10" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children eltdf-active-item has_sub narrow"><a href="#" class=" current "><span class="item_outer"><span class="item_text">Home</span><i class="eltdf-menu-arrow fa fa-angle-down"></i></span></a>
<div class="second"><div class="inner"><ul><li id="nav-menu-item-251" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home "><a href="gretnagreen.qodeinteractive.html" class=""><span class="item_outer"><span class="item_text">Main Home</span></span></a></li>
<li id="nav-menu-item-1165" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="announcement-home.html" class=""><span class="item_outer"><span class="item_text">Announcement Home</span></span></a></li>
<li id="nav-menu-item-1484" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="pinterest-gallery.html" class=""><span class="item_outer"><span class="item_text">Pinterest Gallery</span></span></a></li>
<li id="nav-menu-item-702" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="event-home.html" class=""><span class="item_outer"><span class="item_text">Event Home</span></span></a></li>
<li id="nav-menu-item-701" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-562 current_page_item "><a href="blog-home.html" class=""><span class="item_outer"><span class="item_text">Blog Home</span></span></a></li>
<li id="nav-menu-item-700" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="cake-shop.html" class=""><span class="item_outer"><span class="item_text">Cake Shop</span></span></a></li>
<li id="nav-menu-item-861" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="invitation-home.html" class=""><span class="item_outer"><span class="item_text">Invitation Home</span></span></a></li>
<li id="nav-menu-item-1047" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="wedding-home.html" class=""><span class="item_outer"><span class="item_text">Wedding Home</span></span></a></li>
<li id="nav-menu-item-1021" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="venue-home.html" class=""><span class="item_outer"><span class="item_text">Venue Home</span></span></a></li>
<li id="nav-menu-item-730" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="bridal-dresses.html" class=""><span class="item_outer"><span class="item_text">Bridal Dresses</span></span></a></li>
<li id="nav-menu-item-396" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="photo-album.html" class=""><span class="item_outer"><span class="item_text">Photo Album</span></span></a></li>
<li id="nav-menu-item-1108" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="wedding-planner.html" class=""><span class="item_outer"><span class="item_text">Wedding Planner</span></span></a></li>
<li id="nav-menu-item-3335" class="menu-item menu-item-type-post_type menu-item-object-page "><a target="_blank" href="landing.html" class=""><span class="item_outer"><span class="item_text">Landing</span></span></a></li>
</ul></div></div>
</li>
<li id="nav-menu-item-11" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow"><a href="#" class=""><span class="item_outer"><span class="item_text">Pages</span><i class="eltdf-menu-arrow fa fa-angle-down"></i></span></a>
<div class="second"><div class="inner"><ul><li id="nav-menu-item-1926" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="our-story.html" class=""><span class="item_outer"><span class="item_text">Our Story</span></span></a></li>
<li id="nav-menu-item-1750" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="meet-the-team.html" class=""><span class="item_outer"><span class="item_text">Meet The Team</span></span></a></li>
<li id="nav-menu-item-1885" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="our-services.html" class=""><span class="item_outer"><span class="item_text">Our Services</span></span></a></li>
<li id="nav-menu-item-1748" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="gift-registry.html" class=""><span class="item_outer"><span class="item_text">Gift Registry</span></span></a></li>
<li id="nav-menu-item-2035" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="rsvp.html" class=""><span class="item_outer"><span class="item_text">RSVP</span></span></a></li>
<li id="nav-menu-item-1749" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="contact-us.html" class=""><span class="item_outer"><span class="item_text">Contact Us</span></span></a></li>
<li id="nav-menu-item-4060" class="menu-item menu-item-type-custom menu-item-object-custom "><a href="404-error-page.html" class=""><span class="item_outer"><span class="item_text">404 Error Page</span></span></a></li>
</ul></div></div>
</li>
<li id="nav-menu-item-12" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow"><a href="#" class=""><span class="item_outer"><span class="item_text">Gallery</span><i class="eltdf-menu-arrow fa fa-angle-down"></i></span></a>
<div class="second"><div class="inner"><ul><li id="nav-menu-item-1198" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children sub"><a href="portfolio-standard.html" class=""><span class="item_outer"><span class="item_text">Standard</span></span></a>
<ul><li id="nav-menu-item-3078" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="in-grid.html" class=""><span class="item_outer"><span class="item_text">In Grid</span></span></a></li>
<li id="nav-menu-item-3079" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="wide.html" class=""><span class="item_outer"><span class="item_text">Wide</span></span></a></li>
</ul></li>
<li id="nav-menu-item-1197" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children sub"><a href="portfolio-gallery.html" class=""><span class="item_outer"><span class="item_text">Gallery</span></span></a>
<ul><li id="nav-menu-item-3087" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="in-grid.html" class=""><span class="item_outer"><span class="item_text">In Grid</span></span></a></li>
<li id="nav-menu-item-3089" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="joined-in-grid.html" class=""><span class="item_outer"><span class="item_text">Joined / In Grid</span></span></a></li>
<li id="nav-menu-item-3090" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="wide.html" class=""><span class="item_outer"><span class="item_text">Wide</span></span></a></li>
<li id="nav-menu-item-3088" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="joined-wide.html" class=""><span class="item_outer"><span class="item_text">Joined / Wide</span></span></a></li>
</ul></li>
<li id="nav-menu-item-3097" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children sub"><a href="portfolio-masonry.html" class=""><span class="item_outer"><span class="item_text">Masonry</span></span></a>
<ul><li id="nav-menu-item-3101" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="in-grid.html" class=""><span class="item_outer"><span class="item_text">In Grid</span></span></a></li>
<li id="nav-menu-item-3100" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="masonry-joined.html" class=""><span class="item_outer"><span class="item_text">Joined / In Grid</span></span></a></li>
<li id="nav-menu-item-3098" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="masonry-wide.html" class=""><span class="item_outer"><span class="item_text">Wide</span></span></a></li>
<li id="nav-menu-item-3099" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="joined-wide.html" class=""><span class="item_outer"><span class="item_text">Joined / Wide</span></span></a></li>
</ul></li>
<li id="nav-menu-item-3107" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children sub"><a href="portfolio-pinterest.html" class=""><span class="item_outer"><span class="item_text">Pinterest</span></span></a>
<ul><li id="nav-menu-item-3108" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="in-grid.html" class=""><span class="item_outer"><span class="item_text">In Grid</span></span></a></li>
<li id="nav-menu-item-3110" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="wide.html" class=""><span class="item_outer"><span class="item_text">Wide</span></span></a></li>
</ul></li>
<li id="nav-menu-item-2168" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="portfolio-carousel.html" class=""><span class="item_outer"><span class="item_text">Carousel</span></span></a></li>
<li id="nav-menu-item-1183" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub"><a href="#" class=""><span class="item_outer"><span class="item_text">Gallery Layouts</span></span></a>
<ul><li id="nav-menu-item-1203" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="two-columns.html" class=""><span class="item_outer"><span class="item_text">Two Columns</span></span></a></li>
<li id="nav-menu-item-1202" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="three-columns.html" class=""><span class="item_outer"><span class="item_text">Three Columns</span></span></a></li>
<li id="nav-menu-item-1201" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="three-columns-wide.html" class=""><span class="item_outer"><span class="item_text">Three Columns Wide</span></span></a></li>
<li id="nav-menu-item-1207" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="four-columns.html" class=""><span class="item_outer"><span class="item_text">Four Columns</span></span></a></li>
<li id="nav-menu-item-1200" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="four-columns-wide.html" class=""><span class="item_outer"><span class="item_text">Four Columns Wide</span></span></a></li>
<li id="nav-menu-item-1199" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="five-columns-wide.html" class=""><span class="item_outer"><span class="item_text">Five Columns Wide</span></span></a></li>
</ul></li>
<li id="nav-menu-item-1184" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub"><a href="#" class=""><span class="item_outer"><span class="item_text">Gallery Single</span></span></a>
<ul><li id="nav-menu-item-1208" class="menu-item menu-item-type-post_type menu-item-object-portfolio-item "><a href="champagne-roses.html" class=""><span class="item_outer"><span class="item_text">Small Images</span></span></a></li>
<li id="nav-menu-item-1210" class="menu-item menu-item-type-post_type menu-item-object-portfolio-item "><a href="spring-wedding.html" class=""><span class="item_outer"><span class="item_text">Small Slider</span></span></a></li>
<li id="nav-menu-item-1211" class="menu-item menu-item-type-post_type menu-item-object-portfolio-item "><a href="diy-vow-books.html" class=""><span class="item_outer"><span class="item_text">Big Images</span></span></a></li>
<li id="nav-menu-item-1213" class="menu-item menu-item-type-post_type menu-item-object-portfolio-item "><a href="romantic-wedding.html" class=""><span class="item_outer"><span class="item_text">Big Slider</span></span></a></li>
<li id="nav-menu-item-1214" class="menu-item menu-item-type-post_type menu-item-object-portfolio-item "><a href="creative-styling-ideas.html" class=""><span class="item_outer"><span class="item_text">Small Gallery</span></span></a></li>
<li id="nav-menu-item-1215" class="menu-item menu-item-type-post_type menu-item-object-portfolio-item "><a href="best-wedding-songs.html" class=""><span class="item_outer"><span class="item_text">Big Gallery</span></span></a></li>
<li id="nav-menu-item-1216" class="menu-item menu-item-type-post_type menu-item-object-portfolio-item "><a href="dreamy-locations.html" class=""><span class="item_outer"><span class="item_text">Masonry</span></span></a></li>
</ul></li>
</ul></div></div>
</li>
<li id="nav-menu-item-13" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow"><a href="#" class=""><span class="item_outer"><span class="item_text">Blog</span><i class="eltdf-menu-arrow fa fa-angle-down"></i></span></a>
<div class="second"><div class="inner"><ul><li id="nav-menu-item-254" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="right-sidebar.html" class=""><span class="item_outer"><span class="item_text">Right Sidebar</span></span></a></li>
<li id="nav-menu-item-253" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="left-sidebar.html" class=""><span class="item_outer"><span class="item_text">Left Sidebar</span></span></a></li>
<li id="nav-menu-item-252" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="without-sidebar.html" class=""><span class="item_outer"><span class="item_text">Without Sidebar</span></span></a></li>
<li id="nav-menu-item-261" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub"><a href="#" class=""><span class="item_outer"><span class="item_text">Post Types</span></span></a>
<ul><li id="nav-menu-item-260" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="wedding-ceremony.html" class=""><span class="item_outer"><span class="item_text">Standard</span></span></a></li>
<li id="nav-menu-item-259" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="wedding-venues.html" class=""><span class="item_outer"><span class="item_text">Gallery</span></span></a></li>
<li id="nav-menu-item-258" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="engagement-rings.html" class=""><span class="item_outer"><span class="item_text">Link</span></span></a></li>
<li id="nav-menu-item-257" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="dress-ideas.html" class=""><span class="item_outer"><span class="item_text">Quote</span></span></a></li>
<li id="nav-menu-item-256" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="double-wedding.html" class=""><span class="item_outer"><span class="item_text">Video</span></span></a></li>
<li id="nav-menu-item-255" class="menu-item menu-item-type-post_type menu-item-object-post "><a href="white-wedding.html" class=""><span class="item_outer"><span class="item_text">Audio</span></span></a></li>
</ul></li>
</ul></div></div>
</li>
<li id="nav-menu-item-14" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub narrow"><a href="#" class=""><span class="item_outer"><span class="item_text">Shop</span><i class="eltdf-menu-arrow fa fa-angle-down"></i></span></a>
<div class="second"><div class="inner"><ul><li id="nav-menu-item-399" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="product-list.html" class=""><span class="item_outer"><span class="item_text">Product List</span></span></a></li>
<li id="nav-menu-item-403" class="menu-item menu-item-type-post_type menu-item-object-product "><a href="pastel-petals.html" class=""><span class="item_outer"><span class="item_text">Product Single</span></span></a></li>
<li id="nav-menu-item-401" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub"><a href="#" class=""><span class="item_outer"><span class="item_text">Shop Layouts</span></span></a>
<ul><li id="nav-menu-item-469" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="three-columns.html" class=""><span class="item_outer"><span class="item_text">Three Columns</span></span></a></li>
<li id="nav-menu-item-468" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="four-columns.html" class=""><span class="item_outer"><span class="item_text">Four Columns</span></span></a></li>
<li id="nav-menu-item-467" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="full-width.html" class=""><span class="item_outer"><span class="item_text">Full Width</span></span></a></li>
</ul></li>
<li id="nav-menu-item-402" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub"><a href="#" class=""><span class="item_outer"><span class="item_text">Shop Pages</span></span></a>
<ul><li id="nav-menu-item-486" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="my-account.html" class=""><span class="item_outer"><span class="item_text">My account</span></span></a></li>
<li id="nav-menu-item-485" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="cart.html" class=""><span class="item_outer"><span class="item_text">Cart</span></span></a></li>
<li id="nav-menu-item-484" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="checkout.html" class=""><span class="item_outer"><span class="item_text">Checkout</span></span></a></li>
</ul></li>
</ul></div></div>
</li>
<li id="nav-menu-item-15" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children  has_sub wide"><a href="#" class=""><span class="item_outer"><span class="item_text">Elements</span><i class="eltdf-menu-arrow fa fa-angle-down"></i></span></a>
<div class="second"><div class="inner"><ul><li id="nav-menu-item-1381" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub"><a href="#" class=" no_link" onclick="JavaScript: return false;"><span class="item_outer"><span class="item_text">Classic</span></span></a>
<ul><li id="nav-menu-item-1360" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="accordions.html" class=""><span class="item_outer"><span class="item_text">Accordions</span></span></a></li>
<li id="nav-menu-item-1380" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="tabs.html" class=""><span class="item_outer"><span class="item_text">Tabs</span></span></a></li>
<li id="nav-menu-item-1364" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="clients.html" class=""><span class="item_outer"><span class="item_text">Clients</span></span></a></li>
<li id="nav-menu-item-1363" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="buttons.html" class=""><span class="item_outer"><span class="item_text">Buttons</span></span></a></li>
<li id="nav-menu-item-1372" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="icon-with-text.html" class=""><span class="item_outer"><span class="item_text">Icon With Text</span></span></a></li>
<li id="nav-menu-item-1371" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="google-maps.html" class=""><span class="item_outer"><span class="item_text">Google Maps</span></span></a></li>
<li id="nav-menu-item-1366" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="contact-form.html" class=""><span class="item_outer"><span class="item_text">Contact Form</span></span></a></li>
</ul></li>
<li id="nav-menu-item-1383" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub"><a href="#" class=" no_link" onclick="JavaScript: return false;"><span class="item_outer"><span class="item_text">Infographic</span></span></a>
<ul><li id="nav-menu-item-1362" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="blog-list.html" class=""><span class="item_outer"><span class="item_text">Blog List</span></span></a></li>
<li id="nav-menu-item-1379" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="shop-list.html" class=""><span class="item_outer"><span class="item_text">Shop List</span></span></a></li>
<li id="nav-menu-item-1474" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="invitation.html" class=""><span class="item_outer"><span class="item_text">Invitation</span></span></a></li>
<li id="nav-menu-item-1376" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="progress-bar.html" class=""><span class="item_outer"><span class="item_text">Progress Bar</span></span></a></li>
<li id="nav-menu-item-1368" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="counters.html" class=""><span class="item_outer"><span class="item_text">Counters</span></span></a></li>
<li id="nav-menu-item-1367" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="countdown.html" class=""><span class="item_outer"><span class="item_text">Countdown</span></span></a></li>
<li id="nav-menu-item-1374" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="pie-chart.html" class=""><span class="item_outer"><span class="item_text">Pie Chart</span></span></a></li>
</ul></li>
<li id="nav-menu-item-1382" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub"><a href="#" class=" no_link" onclick="JavaScript: return false;"><span class="item_outer"><span class="item_text">Presentation</span></span></a>
<ul><li id="nav-menu-item-1478" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="item-showcase.html" class=""><span class="item_outer"><span class="item_text">Item Showcase</span></span></a></li>
<li id="nav-menu-item-2763" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="image-gallery.html" class=""><span class="item_outer"><span class="item_text">Image Gallery</span></span></a></li>
<li id="nav-menu-item-1943" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="image-with-text.html" class=""><span class="item_outer"><span class="item_text">Image With Text</span></span></a></li>
<li id="nav-menu-item-1389" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="testimonials.html" class=""><span class="item_outer"><span class="item_text">Testimonials</span></span></a></li>
<li id="nav-menu-item-1388" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="team.html" class=""><span class="item_outer"><span class="item_text">Team</span></span></a></li>
<li id="nav-menu-item-1373" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="parallax-section.html" class=""><span class="item_outer"><span class="item_text">Parallax Section</span></span></a></li>
<li id="nav-menu-item-1390" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="video-button.html" class=""><span class="item_outer"><span class="item_text">Video Button</span></span></a></li>
</ul></li>
<li id="nav-menu-item-1385" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children sub"><a href="#" class=" no_link" onclick="JavaScript: return false;"><span class="item_outer"><span class="item_text">Typography</span></span></a>
<ul><li id="nav-menu-item-1391" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="headings.html" class=""><span class="item_outer"><span class="item_text">Headings</span></span></a></li>
<li id="nav-menu-item-1365" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="columns.html" class=""><span class="item_outer"><span class="item_text">Columns</span></span></a></li>
<li id="nav-menu-item-1377" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="section-title.html" class=""><span class="item_outer"><span class="item_text">Section Title</span></span></a></li>
<li id="nav-menu-item-1361" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="blockquote.html" class=""><span class="item_outer"><span class="item_text">Blockquote</span></span></a></li>
<li id="nav-menu-item-1370" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="dropcaps-highlights.html" class=""><span class="item_outer"><span class="item_text">Dropcaps &amp; Highlights</span></span></a></li>
<li id="nav-menu-item-1378" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="separators.html" class=""><span class="item_outer"><span class="item_text">Separators</span></span></a></li>
<li id="nav-menu-item-1369" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="custom-font.html" class=""><span class="item_outer"><span class="item_text">Custom Font</span></span></a></li>
</ul></li>
</ul></div></div>
</li>
</ul></nav></div>
</div>
<div class="eltdf-position-right"><div class="eltdf-position-right-inner">
<div class="eltdf-bottom-menu-right-widget-holder">
<div class="eltdf-shopping-cart-holder" style="margin: 17px 0 0 0">
<div class="eltdf-shopping-cart-inner">
<a itemprop="url" class="eltdf-header-cart eltdf-header-cart-icon-pack" href="cart.html">
<span class="eltdf-cart-icon"><span aria-hidden="true" class="eltdf-icon-font-elegant icon_bag_alt "></span></span>
</a>
<div class="eltdf-shopping-cart-dropdown">
<ul><li class="eltdf-empty-cart">No products in the cart.</li>
</ul></div>
</div>
</div>
<a style="margin: 18px 0 0 19px;" class="eltdf-search-opener eltdf-icon-has-hover eltdf-search-opener-icon-pack" href="javascript:void(0)">
<span class="eltdf-search-opener-wrapper">
<span aria-hidden="true" class="eltdf-icon-font-elegant icon_search "></span> </span>
</a>
<a class="eltdf-side-menu-button-opener eltdf-icon-has-hover eltdf-side-menu-button-opener-svg-path" href="javascript:void(0)" style="margin: 17px 0 0 21px">
<span class="eltdf-side-menu-icon">
<svg class="eltdf-burger" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewbox="0 0 20 16" enable-background="new 0 0 20 16" xml:space="preserve"><rect class="eltdf-burger-bar-1" y="0" width="20" height="2"></rect><rect class="eltdf-burger-bar-2" y="7" width="20" height="2"></rect><rect class="eltdf-burger-bar-3" y="14" width="20" height="2"></rect></svg></span>
</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</header><div class="eltdf-container">
<div class="eltdf-container-inner clearfix">
<div class="eltdf-grid-row eltdf-grid-large-gutter">
<div class="eltdf-page-content-holder eltdf-grid-col-9">
<div class="eltdf-blog-holder eltdf-blog-centered eltdf-blog-pagination-standard" data-blog-type="centered" data-next-page="2" data-max-num-pages="2" data-post-number="6" data-excerpt-length="55">
<div class="eltdf-blog-holder-inner">
<article id="post-9" class="eltdf-post-has-media post-9 post type-post status-publish format-standard has-post-thumbnail hentry category-dancing tag-bouquet tag-bride tag-cake"><div class="eltdf-post-content">
<div class="eltdf-post-heading">
<div class="eltdf-post-image">
<a itemprop="url" href="wedding-ceremony.html" title="Wedding Ceremony">
<img width="1300" height="800" src="images/04-blog-1-img-1.jpg" class="attachment-full size-full wp-post-image" alt="a" srcset="https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-1.jpg 1300w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-1-300x185.jpg 300w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-1-768x473.jpg 768w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-1-1024x630.jpg 1024w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-1-800x492.jpg 800w" sizes="(max-width: 1300px) 100vw, 1300px"></a>
</div>
</div>
<div class="eltdf-post-text">
<div class="eltdf-post-text-inner">
<div class="eltdf-post-text-main">
<h2 itemprop="name" class="entry-title eltdf-post-title">
<a itemprop="url" href="wedding-ceremony.html" title="Wedding Ceremony">
Wedding Ceremony </a>
</h2> <div class="eltdf-post-info-category">
<a href="dancing.html" rel="category tag">Dancing</a></div> <div class="eltdf-post-excerpt-holder">
<p itemprop="description" class="eltdf-post-excerpt">
Aenean commodo ligula eget dolorm aenean massa. Cum sociis theme natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam lorem ante, dapibus in, viverra quis, feugiat a tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi nam eget dui. Etiam rhoncus </p>
</div>
</div>
<div class="eltdf-post-info-bottom clearfix">
<div class="eltdf-post-info-bottom-left">
<div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
<a itemprop="url" href="05.html">
10. May 2018. </a>
<meta itemprop="interactionCount" content="UserComments: 0"></div> </div>
<div class="eltdf-post-info-bottom-center">
<div class="eltdf-blog-share">
<div class="eltdf-social-share-holder eltdf-list">
<ul><li class="eltdf-twitter-share">
<a itemprop="url" class="eltdf-share-link" href="#" onclick="window.open('http://twitter.com/home?status=Aenean+commodo+ligula+eget+dolorm+aenean+massa.+Cum+sociis+theme+natoque+penatibus+et+magnis+dis+parturient+montes%2C+https://gretnagreen.qodeinteractive.com/wedding-ceremony/', 'popupwindow', 'scrollbars=yes,width=800,height=400');">
<span class="eltdf-social-network-icon social_twitter"></span>
</a>
</li><li class="eltdf-pinterest-share">
<a itemprop="url" class="eltdf-share-link" href="#" onclick="popUp=window.open('http://pinterest.com/pin/create/button/?url=https%3A%2F%2Fgretnagreen.qodeinteractive.com%2Fwedding-ceremony%2F&amp;description=Wedding Ceremony&amp;media=https%3A%2F%2Fgretnagreen.qodeinteractive.com%2Fwp-content%2Fuploads%2F2018%2F04%2Fblog-1-img-1.jpg', 'popupwindow', 'scrollbars=yes,width=800,height=400');popUp.focus();return false;">
<span class="eltdf-social-network-icon social_pinterest"></span>
</a>
</li><li class="eltdf-facebook-share">
<a itemprop="url" class="eltdf-share-link" href="#" onclick="window.open('http://www.facebook.com/sharer.php?u=https%3A%2F%2Fgretnagreen.qodeinteractive.com%2Fwedding-ceremony%2F', 'sharer', 'toolbar=0,status=0,width=620,height=280');">
<span class="eltdf-social-network-icon social_facebook"></span>
</a>
</li><li class="eltdf-tumblr-share">
<a itemprop="url" class="eltdf-share-link" href="#" onclick="popUp=window.open('http://www.tumblr.com/share/link?url=https%3A%2F%2Fgretnagreen.qodeinteractive.com%2Fwedding-ceremony%2F&amp;name=Wedding+Ceremony&amp;description=Aenean+commodo+ligula+eget+dolorm+aenean+massa.+Cum+sociis+theme+natoque+penatibus+et+magnis+dis+parturient+montes%2C+nascetur+ridiculus+mus.+Aliquam+lorem+ante%2C+dapibus+in%2C+viverra+quis%2C+feugiat+a+tellus.+Phasellus+viverra+nulla+ut+metus+varius+laoreet.+Quisque+rutrum.+Aenean+imperdiet+etiam+ultricies+nisi+vel+augue.+Curabitur+ullamcorper+ultricies+nisi+nam+eget+dui.+Etiam+rhoncus+maecenas+tempus%2C+tellus+eget+condimentum+rhoncus%2C+sem+quam+semper+libero%2C+sit+amet+adipiscing+sem+neque+sed+ipsum.+Nam+quam+nunc%2C+blandit+vel%2C+luctus+pulvinar%2C+hendrerit+id%2C+lorem.+', 'popupwindow', 'scrollbars=yes,width=800,height=400');popUp.focus();return false;">
<span class="eltdf-social-network-icon social_tumblr"></span>
</a>
</li> </ul></div> </div>
</div>
<div class="eltdf-post-info-bottom-right">
<div class="eltdf-post-info-comments-holder">
<a itemprop="url" class="eltdf-post-info-comments" href="https://gretnagreen.qodeinteractive.com/wedding-ceremony/#comments">
2 Comments </a>
</div>
</div>
</div>
</div>
</div>
</div>
</article><article id="post-157" class="eltdf-post-has-media post-157 post type-post status-publish format-gallery has-post-thumbnail hentry category-dancing tag-celebration tag-groom tag-love post_format-post-format-gallery"><div class="eltdf-post-content">
<div class="eltdf-post-heading">
<div class="eltdf-post-image">
<div class="eltdf-blog-gallery eltdf-owl-slider" data-slider-animate-in="fadeIn" data-slider-animate-out="fadeOut">
<div>
<a itemprop="url" href="wedding-venues.html">
<img width="1300" height="800" src="images/04-blog-1-img-2.jpg" class="attachment-full size-full" alt="a" srcset="https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-2.jpg 1300w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-2-300x185.jpg 300w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-2-768x473.jpg 768w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-2-1024x630.jpg 1024w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-2-800x492.jpg 800w" sizes="(max-width: 1300px) 100vw, 1300px"></a>
</div>
<div>
<a itemprop="url" href="wedding-venues.html">
<img width="1300" height="800" src="images/04-blog-1-img-1.jpg" class="attachment-full size-full" alt="a" srcset="https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-1.jpg 1300w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-1-300x185.jpg 300w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-1-768x473.jpg 768w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-1-1024x630.jpg 1024w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-1-800x492.jpg 800w" sizes="(max-width: 1300px) 100vw, 1300px"></a>
</div>
<div>
<a itemprop="url" href="wedding-venues.html">
<img width="1300" height="800" src="images/04-blog-1-img-4.jpg" class="attachment-full size-full" alt="a" srcset="https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-4.jpg 1300w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-4-300x185.jpg 300w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-4-768x473.jpg 768w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-4-1024x630.jpg 1024w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-4-800x492.jpg 800w" sizes="(max-width: 1300px) 100vw, 1300px"></a>
</div>
</div>
</div>
</div>
<div class="eltdf-post-text">
<div class="eltdf-post-text-inner">
<div class="eltdf-post-text-main">
<h2 itemprop="name" class="entry-title eltdf-post-title">
<a itemprop="url" href="wedding-venues.html" title="Wedding Venues">
Wedding Venues </a>
</h2> <div class="eltdf-post-info-category">
<a href="dancing.html" rel="category tag">Dancing</a></div> <div class="eltdf-post-excerpt-holder">
<p itemprop="description" class="eltdf-post-excerpt">
Aenean commodo ligula eget dolorm aenean massa. Cum sociis theme natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam lorem ante, dapibus in, viverra quis, feugiat a tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi nam eget dui. Etiam rhoncus </p>
</div>
</div>
<div class="eltdf-post-info-bottom clearfix">
<div class="eltdf-post-info-bottom-left">
<div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
<a itemprop="url" href="05.html">
10. May 2018. </a>
<meta itemprop="interactionCount" content="UserComments: 0"></div> </div>
<div class="eltdf-post-info-bottom-center">
<div class="eltdf-blog-share">
<div class="eltdf-social-share-holder eltdf-list">
<ul><li class="eltdf-twitter-share">
<a itemprop="url" class="eltdf-share-link" href="#" onclick="window.open('http://twitter.com/home?status=Aenean+commodo+ligula+eget+dolorm+aenean+massa.+Cum+sociis+theme+natoque+penatibus+et+magnis+dis+parturient+montes%2C+https://gretnagreen.qodeinteractive.com/wedding-venues/', 'popupwindow', 'scrollbars=yes,width=800,height=400');">
<span class="eltdf-social-network-icon social_twitter"></span>
</a>
</li><li class="eltdf-pinterest-share">
<a itemprop="url" class="eltdf-share-link" href="#" onclick="popUp=window.open('http://pinterest.com/pin/create/button/?url=https%3A%2F%2Fgretnagreen.qodeinteractive.com%2Fwedding-venues%2F&amp;description=Wedding Venues&amp;media=https%3A%2F%2Fgretnagreen.qodeinteractive.com%2Fwp-content%2Fuploads%2F2018%2F04%2Fblog-1-img-2.jpg', 'popupwindow', 'scrollbars=yes,width=800,height=400');popUp.focus();return false;">
<span class="eltdf-social-network-icon social_pinterest"></span>
</a>
</li><li class="eltdf-facebook-share">
<a itemprop="url" class="eltdf-share-link" href="#" onclick="window.open('http://www.facebook.com/sharer.php?u=https%3A%2F%2Fgretnagreen.qodeinteractive.com%2Fwedding-venues%2F', 'sharer', 'toolbar=0,status=0,width=620,height=280');">
<span class="eltdf-social-network-icon social_facebook"></span>
</a>
</li><li class="eltdf-tumblr-share">
<a itemprop="url" class="eltdf-share-link" href="#" onclick="popUp=window.open('http://www.tumblr.com/share/link?url=https%3A%2F%2Fgretnagreen.qodeinteractive.com%2Fwedding-venues%2F&amp;name=Wedding+Venues&amp;description=Aenean+commodo+ligula+eget+dolorm+aenean+massa.+Cum+sociis+theme+natoque+penatibus+et+magnis+dis+parturient+montes%2C+nascetur+ridiculus+mus.+Aliquam+lorem+ante%2C+dapibus+in%2C+viverra+quis%2C+feugiat+a+tellus.+Phasellus+viverra+nulla+ut+metus+varius+laoreet.+Quisque+rutrum.+Aenean+imperdiet+etiam+ultricies+nisi+vel+augue.+Curabitur+ullamcorper+ultricies+nisi+nam+eget+dui.+Etiam+rhoncus+maecenas+tempus%2C+tellus+eget+condimentum+rhoncus%2C+sem+quam+semper+libero%2C+sit+amet+adipiscing+sem+neque+sed+ipsum.+Nam+quam+nunc%2C+blandit+vel%2C+luctus+pulvinar%2C+hendrerit+id%2C+lorem.+', 'popupwindow', 'scrollbars=yes,width=800,height=400');popUp.focus();return false;">
<span class="eltdf-social-network-icon social_tumblr"></span>
</a>
</li> </ul></div> </div>
</div>
<div class="eltdf-post-info-bottom-right">
<div class="eltdf-post-info-comments-holder">
<a itemprop="url" class="eltdf-post-info-comments" href="https://gretnagreen.qodeinteractive.com/wedding-venues/#comments">
1 Comment </a>
</div>
</div>
</div>
</div>
</div>
</div>
</article><article id="post-161" class="eltdf-post-has-media post-161 post type-post status-publish format-quote has-post-thumbnail hentry category-bijou-brides tag-bride tag-love post_format-post-format-quote"><div class="eltdf-post-content" style="background-image: url( https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-3.jpg)">
<div class="eltdf-post-text">
<div class="eltdf-post-text-inner">
<div class="eltdf-post-info-top">
<div class="eltdf-post-mark">
<span class="icon_quotations eltdf-quote-mark"></span>
</div>
<div class="eltdf-post-info-category">
<a href="bijou-brides.html" rel="category tag">Bijou Brides</a></div> </div>
<div class="eltdf-post-text-main">
<div class="eltdf-post-quote-holder">
<div class="eltdf-post-quote-holder-inner">
<h5 itemprop="name" class="eltdf-quote-title eltdf-post-title">
<a itemprop="url" href="dress-ideas.html" title="Dress Ideas">
Love isn't something you find. Love is something that finds you. </a>
</h5>
<span class="eltdf-quote-author">
Lorreta Young </span>
</div>
</div> </div>
<div class="eltdf-post-info-bottom clearfix">
<div class="eltdf-post-info-bottom-left">
<div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
<a itemprop="url" href="05.html">
10. May 2018. </a>
<meta itemprop="interactionCount" content="UserComments: 0"></div> </div>
<div class="eltdf-post-info-bottom-right">
<div class="eltdf-blog-share">
<div class="eltdf-social-share-holder eltdf-list">
<ul><li class="eltdf-twitter-share">
<a itemprop="url" class="eltdf-share-link" href="#" onclick="window.open('http://twitter.com/home?status=Aenean+commodo+ligula+eget+dolorm+aenean+massa.+Cum+sociis+theme+natoque+penatibus+et+magnis+dis+parturient+montes%2C+https://gretnagreen.qodeinteractive.com/dress-ideas/', 'popupwindow', 'scrollbars=yes,width=800,height=400');">
<span class="eltdf-social-network-icon social_twitter"></span>
</a>
</li><li class="eltdf-pinterest-share">
<a itemprop="url" class="eltdf-share-link" href="#" onclick="popUp=window.open('http://pinterest.com/pin/create/button/?url=https%3A%2F%2Fgretnagreen.qodeinteractive.com%2Fdress-ideas%2F&amp;description=Dress Ideas&amp;media=https%3A%2F%2Fgretnagreen.qodeinteractive.com%2Fwp-content%2Fuploads%2F2018%2F04%2Fblog-1-img-3.jpg', 'popupwindow', 'scrollbars=yes,width=800,height=400');popUp.focus();return false;">
<span class="eltdf-social-network-icon social_pinterest"></span>
</a>
</li><li class="eltdf-facebook-share">
<a itemprop="url" class="eltdf-share-link" href="#" onclick="window.open('http://www.facebook.com/sharer.php?u=https%3A%2F%2Fgretnagreen.qodeinteractive.com%2Fdress-ideas%2F', 'sharer', 'toolbar=0,status=0,width=620,height=280');">
<span class="eltdf-social-network-icon social_facebook"></span>
</a>
</li><li class="eltdf-tumblr-share">
<a itemprop="url" class="eltdf-share-link" href="#" onclick="popUp=window.open('http://www.tumblr.com/share/link?url=https%3A%2F%2Fgretnagreen.qodeinteractive.com%2Fdress-ideas%2F&amp;name=Dress+Ideas&amp;description=Aenean+commodo+ligula+eget+dolorm+aenean+massa.+Cum+sociis+theme+natoque+penatibus+et+magnis+dis+parturient+montes%2C+nascetur+ridiculus+mus.+Aliquam+lorem+ante%2C+dapibus+in%2C+viverra+quis%2C+feugiat+a+tellus.+Phasellus+viverra+nulla+ut+metus+varius+laoreet.+Quisque+rutrum.+Aenean+imperdiet+etiam+ultricies+nisi+vel+augue.+Curabitur+ullamcorper+ultricies+nisi+nam+eget+dui.+Etiam+rhoncus+maecenas+tempus%2C+tellus+eget+condimentum+rhoncus%2C+sem+quam+semper+libero%2C+sit+amet+adipiscing+sem+neque+sed+ipsum.+Nam+quam+nunc%2C+blandit+vel%2C+luctus+pulvinar%2C+hendrerit+id%2C+lorem.+', 'popupwindow', 'scrollbars=yes,width=800,height=400');popUp.focus();return false;">
<span class="eltdf-social-network-icon social_tumblr"></span>
</a>
</li> </ul></div> </div>
</div>
</div>
</div>
</div>
</div>
</article><article id="post-2262" class="eltdf-post-has-media post-2262 post type-post status-publish format-gallery has-post-thumbnail hentry category-dancing tag-celebration tag-groom tag-love post_format-post-format-gallery"><div class="eltdf-post-content">
<div class="eltdf-post-heading">
<div class="eltdf-post-image">
<div class="eltdf-blog-gallery eltdf-owl-slider" data-slider-animate-in="fadeIn" data-slider-animate-out="fadeOut">
<div>
<a itemprop="url" href="colorful-bouquets.html">
<img width="1300" height="800" src="images/04-blog-1-img-4.jpg" class="attachment-full size-full" alt="a" srcset="https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-4.jpg 1300w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-4-300x185.jpg 300w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-4-768x473.jpg 768w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-4-1024x630.jpg 1024w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-4-800x492.jpg 800w" sizes="(max-width: 1300px) 100vw, 1300px"></a>
</div>
<div>
<a itemprop="url" href="colorful-bouquets.html">
<img width="1300" height="800" src="images/04-blog-1-img-2.jpg" class="attachment-full size-full" alt="a" srcset="https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-2.jpg 1300w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-2-300x185.jpg 300w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-2-768x473.jpg 768w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-2-1024x630.jpg 1024w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-2-800x492.jpg 800w" sizes="(max-width: 1300px) 100vw, 1300px"></a>
</div>
<div>
<a itemprop="url" href="colorful-bouquets.html">
<img width="1300" height="800" src="images/04-blog-1-img-1.jpg" class="attachment-full size-full" alt="a" srcset="https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-1.jpg 1300w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-1-300x185.jpg 300w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-1-768x473.jpg 768w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-1-1024x630.jpg 1024w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-1-800x492.jpg 800w" sizes="(max-width: 1300px) 100vw, 1300px"></a>
</div>
</div>
</div>
</div>
<div class="eltdf-post-text">
<div class="eltdf-post-text-inner">
<div class="eltdf-post-text-main">
<h2 itemprop="name" class="entry-title eltdf-post-title">
<a itemprop="url" href="colorful-bouquets.html" title="Colorful Bouquets">
Colorful Bouquets </a>
</h2> <div class="eltdf-post-info-category">
<a href="dancing.html" rel="category tag">Dancing</a></div> <div class="eltdf-post-excerpt-holder">
<p itemprop="description" class="eltdf-post-excerpt">
Aenean commodo ligula eget dolorm aenean massa. Cum sociis theme natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam lorem ante, dapibus in, viverra quis, feugiat a tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi nam eget dui. Etiam rhoncus </p>
</div>
</div>
<div class="eltdf-post-info-bottom clearfix">
<div class="eltdf-post-info-bottom-left">
<div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
<a itemprop="url" href="05.html">
10. May 2018. </a>
<meta itemprop="interactionCount" content="UserComments: 0"></div> </div>
<div class="eltdf-post-info-bottom-center">
<div class="eltdf-blog-share">
<div class="eltdf-social-share-holder eltdf-list">
<ul><li class="eltdf-twitter-share">
<a itemprop="url" class="eltdf-share-link" href="#" onclick="window.open('http://twitter.com/home?status=Aenean+commodo+ligula+eget+dolorm+aenean+massa.+Cum+sociis+theme+natoque+penatibus+et+magnis+dis+parturient+montes%2C+https://gretnagreen.qodeinteractive.com/colorful-bouquets/', 'popupwindow', 'scrollbars=yes,width=800,height=400');">
<span class="eltdf-social-network-icon social_twitter"></span>
</a>
</li><li class="eltdf-pinterest-share">
<a itemprop="url" class="eltdf-share-link" href="#" onclick="popUp=window.open('http://pinterest.com/pin/create/button/?url=https%3A%2F%2Fgretnagreen.qodeinteractive.com%2Fcolorful-bouquets%2F&amp;description=Colorful Bouquets&amp;media=https%3A%2F%2Fgretnagreen.qodeinteractive.com%2Fwp-content%2Fuploads%2F2018%2F04%2Fblog-1-img-4.jpg', 'popupwindow', 'scrollbars=yes,width=800,height=400');popUp.focus();return false;">
<span class="eltdf-social-network-icon social_pinterest"></span>
</a>
</li><li class="eltdf-facebook-share">
<a itemprop="url" class="eltdf-share-link" href="#" onclick="window.open('http://www.facebook.com/sharer.php?u=https%3A%2F%2Fgretnagreen.qodeinteractive.com%2Fcolorful-bouquets%2F', 'sharer', 'toolbar=0,status=0,width=620,height=280');">
<span class="eltdf-social-network-icon social_facebook"></span>
</a>
</li><li class="eltdf-tumblr-share">
<a itemprop="url" class="eltdf-share-link" href="#" onclick="popUp=window.open('http://www.tumblr.com/share/link?url=https%3A%2F%2Fgretnagreen.qodeinteractive.com%2Fcolorful-bouquets%2F&amp;name=Colorful+Bouquets&amp;description=Aenean+commodo+ligula+eget+dolorm+aenean+massa.+Cum+sociis+theme+natoque+penatibus+et+magnis+dis+parturient+montes%2C+nascetur+ridiculus+mus.+Aliquam+lorem+ante%2C+dapibus+in%2C+viverra+quis%2C+feugiat+a+tellus.+Phasellus+viverra+nulla+ut+metus+varius+laoreet.+Quisque+rutrum.+Aenean+imperdiet+etiam+ultricies+nisi+vel+augue.+Curabitur+ullamcorper+ultricies+nisi+nam+eget+dui.+Etiam+rhoncus+maecenas+tempus%2C+tellus+eget+condimentum+rhoncus%2C+sem+quam+semper+libero%2C+sit+amet+adipiscing+sem+neque+sed+ipsum.+Nam+quam+nunc%2C+blandit+vel%2C+luctus+pulvinar%2C+hendrerit+id%2C+lorem.+', 'popupwindow', 'scrollbars=yes,width=800,height=400');popUp.focus();return false;">
<span class="eltdf-social-network-icon social_tumblr"></span>
</a>
</li> </ul></div> </div>
</div>
<div class="eltdf-post-info-bottom-right">
<div class="eltdf-post-info-comments-holder">
<a itemprop="url" class="eltdf-post-info-comments" href="https://gretnagreen.qodeinteractive.com/colorful-bouquets/#respond">
0 Comments </a>
</div>
</div>
</div>
</div>
</div>
</div>
</article><article id="post-159" class="eltdf-post-has-media post-159 post type-post status-publish format-link has-post-thumbnail hentry category-bijou-brides tag-bride tag-cake tag-groom post_format-post-format-link"><div class="eltdf-post-content" style="background-image: url( https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-5.jpg)">
<div class="eltdf-post-text">
<div class="eltdf-post-text-inner">
<div class="eltdf-post-info-top">
<div class="eltdf-post-mark">
<span class="icon_link eltdf-link-mark"></span>
</div>
<div class="eltdf-post-info-category">
<a href="bijou-brides.html" rel="category tag">Bijou Brides</a></div> </div>
<div class="eltdf-post-text-main">
<div class="eltdf-post-link-holder">
<div class="eltdf-post-link-holder-inner">
<h5 itemprop="name" class="eltdf-link-title eltdf-post-title">
<a itemprop="url" href="engagement-rings.html" title="Wedding Rings, Jewellery And Accessories" target="_blank">
Wedding Rings, Jewellery And Accessories </a>
</h5>
</div>
</div> </div>
<div class="eltdf-post-info-bottom clearfix">
<div class="eltdf-post-info-bottom-left">
<div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
<a itemprop="url" href="05.html">
10. May 2018. </a>
<meta itemprop="interactionCount" content="UserComments: 0"></div> </div>
<div class="eltdf-post-info-bottom-right">
<div class="eltdf-blog-share">
<div class="eltdf-social-share-holder eltdf-list">
<ul><li class="eltdf-twitter-share">
<a itemprop="url" class="eltdf-share-link" href="#" onclick="window.open('http://twitter.com/home?status=Aenean+commodo+ligula+eget+dolorm+aenean+massa.+Cum+sociis+theme+natoque+penatibus+et+magnis+dis+parturient+montes%2C+https://gretnagreen.qodeinteractive.com/engagement-rings/', 'popupwindow', 'scrollbars=yes,width=800,height=400');">
<span class="eltdf-social-network-icon social_twitter"></span>
</a>
</li><li class="eltdf-pinterest-share">
<a itemprop="url" class="eltdf-share-link" href="#" onclick="popUp=window.open('http://pinterest.com/pin/create/button/?url=https%3A%2F%2Fgretnagreen.qodeinteractive.com%2Fengagement-rings%2F&amp;description=Wedding Rings, Jewellery And Accessories&amp;media=https%3A%2F%2Fgretnagreen.qodeinteractive.com%2Fwp-content%2Fuploads%2F2018%2F04%2Fblog-1-img-5.jpg', 'popupwindow', 'scrollbars=yes,width=800,height=400');popUp.focus();return false;">
<span class="eltdf-social-network-icon social_pinterest"></span>
</a>
</li><li class="eltdf-facebook-share">
<a itemprop="url" class="eltdf-share-link" href="#" onclick="window.open('http://www.facebook.com/sharer.php?u=https%3A%2F%2Fgretnagreen.qodeinteractive.com%2Fengagement-rings%2F', 'sharer', 'toolbar=0,status=0,width=620,height=280');">
<span class="eltdf-social-network-icon social_facebook"></span>
</a>
</li><li class="eltdf-tumblr-share">
<a itemprop="url" class="eltdf-share-link" href="#" onclick="popUp=window.open('http://www.tumblr.com/share/link?url=https%3A%2F%2Fgretnagreen.qodeinteractive.com%2Fengagement-rings%2F&amp;name=Wedding+Rings%2C+Jewellery+And+Accessories&amp;description=Aenean+commodo+ligula+eget+dolorm+aenean+massa.+Cum+sociis+theme+natoque+penatibus+et+magnis+dis+parturient+montes%2C+nascetur+ridiculus+mus.+Aliquam+lorem+ante%2C+dapibus+in%2C+viverra+quis%2C+feugiat+a+tellus.+Phasellus+viverra+nulla+ut+metus+varius+laoreet.+Quisque+rutrum.+Aenean+imperdiet+etiam+ultricies+nisi+vel+augue.+Curabitur+ullamcorper+ultricies+nisi+nam+eget+dui.+Etiam+rhoncus+maecenas+tempus%2C+tellus+eget+condimentum+rhoncus%2C+sem+quam+semper+libero%2C+sit+amet+adipiscing+sem+neque+sed+ipsum.+Nam+quam+nunc%2C+blandit+vel%2C+luctus+pulvinar%2C+hendrerit+id%2C+lorem.+', 'popupwindow', 'scrollbars=yes,width=800,height=400');popUp.focus();return false;">
<span class="eltdf-social-network-icon social_tumblr"></span>
</a>
</li> </ul></div> </div>
</div>
</div>
</div>
</div>
</div>
</article><article id="post-165" class="eltdf-post-has-media post-165 post type-post status-publish format-audio has-post-thumbnail hentry category-wedding tag-bride tag-groom tag-love post_format-post-format-audio"><div class="eltdf-post-content">
<div class="eltdf-post-heading">
<div class="eltdf-post-image">
<a itemprop="url" href="white-wedding.html" title="White Wedding">
<img width="1300" height="800" src="images/04-blog-1-img-6.jpg" class="attachment-full size-full wp-post-image" alt="a" srcset="https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-6.jpg 1300w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-6-300x185.jpg 300w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-6-768x473.jpg 768w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-6-1024x630.jpg 1024w, https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-6-800x492.jpg 800w" sizes="(max-width: 1300px) 100vw, 1300px"></a>
</div>
<div class="eltdf-blog-audio-holder">
<audio class="eltdf-blog-audio" src="https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/Fingers-In-The-Noise-Elixir.mp3" controls="controls">
Your browser don't support audio player </audio></div>
</div>
<div class="eltdf-post-text">
<div class="eltdf-post-text-inner">
<div class="eltdf-post-text-main">
<h2 itemprop="name" class="entry-title eltdf-post-title">
<a itemprop="url" href="white-wedding.html" title="White Wedding">
White Wedding </a>
</h2> <div class="eltdf-post-info-category">
<a href="wedding.html" rel="category tag">Wedding</a></div> <div class="eltdf-post-excerpt-holder">
<p itemprop="description" class="eltdf-post-excerpt">
Aenean commodo ligula eget dolorm aenean massa. Cum sociis theme natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam lorem ante, dapibus in, viverra quis, feugiat a tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi nam eget dui. Etiam rhoncus </p>
</div>
</div>
<div class="eltdf-post-info-bottom clearfix">
<div class="eltdf-post-info-bottom-left">
<div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
<a itemprop="url" href="05.html">
10. May 2018. </a>
<meta itemprop="interactionCount" content="UserComments: 0"></div> </div>
<div class="eltdf-post-info-bottom-center">
<div class="eltdf-blog-share">
<div class="eltdf-social-share-holder eltdf-list">
<ul><li class="eltdf-twitter-share">
<a itemprop="url" class="eltdf-share-link" href="#" onclick="window.open('http://twitter.com/home?status=Aenean+commodo+ligula+eget+dolorm+aenean+massa.+Cum+sociis+theme+natoque+penatibus+et+magnis+dis+parturient+montes%2C+https://gretnagreen.qodeinteractive.com/white-wedding/', 'popupwindow', 'scrollbars=yes,width=800,height=400');">
<span class="eltdf-social-network-icon social_twitter"></span>
</a>
</li><li class="eltdf-pinterest-share">
<a itemprop="url" class="eltdf-share-link" href="#" onclick="popUp=window.open('http://pinterest.com/pin/create/button/?url=https%3A%2F%2Fgretnagreen.qodeinteractive.com%2Fwhite-wedding%2F&amp;description=White Wedding&amp;media=https%3A%2F%2Fgretnagreen.qodeinteractive.com%2Fwp-content%2Fuploads%2F2018%2F04%2Fblog-1-img-6.jpg', 'popupwindow', 'scrollbars=yes,width=800,height=400');popUp.focus();return false;">
<span class="eltdf-social-network-icon social_pinterest"></span>
</a>
</li><li class="eltdf-facebook-share">
<a itemprop="url" class="eltdf-share-link" href="#" onclick="window.open('http://www.facebook.com/sharer.php?u=https%3A%2F%2Fgretnagreen.qodeinteractive.com%2Fwhite-wedding%2F', 'sharer', 'toolbar=0,status=0,width=620,height=280');">
<span class="eltdf-social-network-icon social_facebook"></span>
</a>
</li><li class="eltdf-tumblr-share">
<a itemprop="url" class="eltdf-share-link" href="#" onclick="popUp=window.open('http://www.tumblr.com/share/link?url=https%3A%2F%2Fgretnagreen.qodeinteractive.com%2Fwhite-wedding%2F&amp;name=White+Wedding&amp;description=Aenean+commodo+ligula+eget+dolorm+aenean+massa.+Cum+sociis+theme+natoque+penatibus+et+magnis+dis+parturient+montes%2C+nascetur+ridiculus+mus.+Aliquam+lorem+ante%2C+dapibus+in%2C+viverra+quis%2C+feugiat+a+tellus.+Phasellus+viverra+nulla+ut+metus+varius+laoreet.+Quisque+rutrum.+Aenean+imperdiet+etiam+ultricies+nisi+vel+augue.+Curabitur+ullamcorper+ultricies+nisi+nam+eget+dui.+Etiam+rhoncus+maecenas+tempus%2C+tellus+eget+condimentum+rhoncus%2C+sem+quam+semper+libero%2C+sit+amet+adipiscing+sem+neque+sed+ipsum.+Nam+quam+nunc%2C+blandit+vel%2C+luctus+pulvinar%2C+hendrerit+id%2C+lorem.+', 'popupwindow', 'scrollbars=yes,width=800,height=400');popUp.focus();return false;">
<span class="eltdf-social-network-icon social_tumblr"></span>
</a>
</li> </ul></div> </div>
</div>
<div class="eltdf-post-info-bottom-right">
<div class="eltdf-post-info-comments-holder">
<a itemprop="url" class="eltdf-post-info-comments" href="https://gretnagreen.qodeinteractive.com/white-wedding/#comments">
1 Comment </a>
</div>
</div>
</div>
</div>
</div>
</div>
</article></div>
<div class="eltdf-blog-pagination">
<ul><li class="eltdf-pag-number">
<a class="eltdf-pag-active" href="#">1</a>
</li>
<li class="eltdf-pag-number">
<a itemprop="url" class="eltdf-pag-inactive" href="2.html">2</a>
</li>
<li class="eltdf-pag-next">
<a itemprop="url" href="2.html">
<span class="arrow_carrot-right"></span>
</a>
</li>
</ul></div>
<div class="eltdf-blog-pagination-wp">
</div>
</div> </div>
<div class="eltdf-sidebar-holder eltdf-grid-col-3">
<aside class="eltdf-sidebar"><div class="widget eltdf-author-info-widget ">
<div class="eltdf-aiw-inner">
<a itemprop="url" class="eltdf-aiw-image" href="anne.html">
<img src="images/04-blog-autor-img-1.png" width="250" height="250" alt="Anne Lorren" class="avatar avatar-250 wp-user-avatar wp-user-avatar-250 alignnone photo"></a>
<p itemprop="description" class="eltdf-aiw-text">Wedding planner in love with photography and flower decorating. Blogger.</p>
</div>
</div>
<div class="widget eltdf-separator-widget"><div class="eltdf-separator-holder clearfix  eltdf-separator-center eltdf-separator-normal">
<div class="eltdf-separator" style="border-style: solid;margin-top: -1px"></div>
</div>
</div><div id="categories-3" class="widget widget_categories"><div class="eltdf-widget-title-holder"><h5 class="eltdf-widget-title">Categories</h5></div> <ul><li class="cat-item cat-item-21"><a href="beauty.html">Beauty</a>
</li>
<li class="cat-item cat-item-22"><a href="bijou-brides.html">Bijou Brides</a>
</li>
<li class="cat-item cat-item-19"><a href="dancing.html">Dancing</a>
</li>
<li class="cat-item cat-item-16"><a href="wedding.html">Wedding</a>
</li>
</ul></div><div class="widget eltdf-separator-widget"><div class="eltdf-separator-holder clearfix  eltdf-separator-center eltdf-separator-normal">
<div class="eltdf-separator" style="border-style: solid;margin-top: 2px"></div>
</div>
</div><div id="eltdf_instagram_widget-2" class="widget widget_eltdf_instagram_widget"><div class="eltdf-widget-title-holder"><h5 class="eltdf-widget-title">Follow</h5></div> <ul class="eltdf-instagram-feed clearfix eltdf-col-3 eltdf-instagram-gallery eltdf-tiny-space"><li>
<a href="https://www.instagram.com/p/BiEqqTaDwIB/" target="_blank">
 <span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
<img class="low_resolution" src="images/s320x320-30605393_603811343303045_6325435384654200832_n.jpg" alt="" width="320" height="320"></a>
</li>
<li>
<a href="https://www.instagram.com/p/BiEqongjq9I/" target="_blank">
<span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
<img class="low_resolution" src="images/s320x320-30602665_666976383634205_5416085766075842560_n.jpg" alt="" width="320" height="320"></a>
</li>
<li>
<a href="https://www.instagram.com/p/BiEqm-ajg7b/" target="_blank">
<span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
<img class="low_resolution" src="images/s320x320-31157005_993121947517406_5376739050986143744_n.jpg" alt="" width="320" height="320"></a>
</li>
<li>
<a href="https://www.instagram.com/p/BiEqjxDjmWR/" target="_blank">
<span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
<img class="low_resolution" src="images/s320x320-30601703_167995387231560_7542690190775549952_n.jpg" alt="" width="320" height="320"></a>
</li>
<li>
<a href="https://www.instagram.com/p/BiEqgIIjiTg/" target="_blank">
<span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
<img class="low_resolution" src="images/s320x320-30829389_190080938382728_8815398182943981568_n.jpg" alt="" width="320" height="320"></a>
</li>
<li>
<a href="https://www.instagram.com/p/BiEqeTMDEcT/" target="_blank">
<span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
<img class="low_resolution" src="images/s320x320-31118366_163054664369910_1345918763450499072_n.jpg" alt="" width="320" height="320"></a>
</li>
<li>
<a href="https://www.instagram.com/p/BiEqaMWj5iN/" target="_blank">
<span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
<img class="low_resolution" src="images/s320x320-30903722_171831546852478_5001505462189817856_n.jpg" alt="" width="320" height="320"></a>
</li>
<li>
<a href="https://www.instagram.com/p/BiEqX33DbFs/" target="_blank">
<span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
<img class="low_resolution" src="images/s320x320-30867878_197098437572789_9047513836415877120_n.jpg" alt="" width="320" height="320"></a>
</li>
<li>
<a href="https://www.instagram.com/p/BiEqUnODl3M/" target="_blank">
<span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
<img class="low_resolution" src="images/s320x320-30604239_466299773799687_8025880024529764352_n.jpg" alt="" width="320" height="320"></a>
</li>
</ul></div><div class="widget eltdf-separator-widget"><div class="eltdf-separator-holder clearfix  eltdf-separator-center eltdf-separator-normal">
<div class="eltdf-separator" style="border-style: solid;margin-top: 6px"></div>
</div>
</div><div class="widget eltdf-blog-list-widget"><div class="eltdf-widget-title-holder"><h5 class="eltdf-widget-title">Recent Posts</h5></div><div class="eltdf-blog-list-holder eltdf-grid-list eltdf-disable-bottom-space eltdf-bl-minimal eltdf-one-columns eltdf-large-space eltdf-bl-pag-no-pagination" data-type="minimal" data-number-of-posts="3" data-number-of-columns="one" data-space-between-items="large" data-orderby="date" data-order="ASC" data-image-size="thumbnail" data-title-tag="h5" data-excerpt-length="40" data-post-info-section="yes" data-post-info-image="yes" data-post-info-author="yes" data-post-info-date="yes" data-post-info-category="yes" data-post-info-comments="no" data-post-info-like="no" data-post-info-share="no" data-pagination-type="no-pagination" data-max-num-pages="4" data-next-page="2">
<div class="eltdf-bl-wrapper eltdf-outer-space">
<ul class="eltdf-blog-list"><li class="eltdf-bl-item eltdf-item-space clearfix">
<div class="eltdf-bli-inner">
<div class="eltdf-bli-content">
<h5 itemprop="name" class="entry-title eltdf-post-title">
<a itemprop="url" href="spring-color-mood.html" title="Spring Color Mood">
 Spring Color Mood </a>
</h5> <div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
<a itemprop="url" href="05.html">
10. May 2018. </a>
<meta itemprop="interactionCount" content="UserComments: 0"></div> </div>
</div>
</li><li class="eltdf-bl-item eltdf-item-space clearfix">
<div class="eltdf-bli-inner">
<div class="eltdf-bli-content">
<h5 itemprop="name" class="entry-title eltdf-post-title">
<a itemprop="url" href="double-wedding.html" title="Double Wedding">
Double Wedding </a>
</h5> <div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
<a itemprop="url" href="05.html">
10. May 2018. </a>
<meta itemprop="interactionCount" content="UserComments: 0"></div> </div>
</div>
</li><li class="eltdf-bl-item eltdf-item-space clearfix">
<div class="eltdf-bli-inner">
<div class="eltdf-bli-content">
<h5 itemprop="name" class="entry-title eltdf-post-title">
<a itemprop="url" href="handmade-gowns.html" title="Handmade Gowns">
Handmade Gowns </a>
</h5> <div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
<a itemprop="url" href="05.html">
10. May 2018. </a>
<meta itemprop="interactionCount" content="UserComments: 0"></div> </div>
</div>
</li> </ul></div>
</div></div><div class="widget eltdf-separator-widget"><div class="eltdf-separator-holder clearfix  eltdf-separator-center eltdf-separator-normal">
<div class="eltdf-separator" style="border-style: solid;margin-top: 34px"></div>
</div>
</div><div id="tag_cloud-2" class="widget widget_tag_cloud"><div class="eltdf-widget-title-holder"><h5 class="eltdf-widget-title">Tags</h5></div><div class="tagcloud"><a href="bouquet.html" class="tag-cloud-link tag-link-28 tag-link-position-1" style="font-size: 12pt;" aria-label="Bouquet (3 items)">Bouquet</a>
<a href="bride.html" class="tag-cloud-link tag-link-26 tag-link-position-2" style="font-size: 22pt;" aria-label="Bride (7 items)">Bride</a>
<a href="cake.html" class="tag-cloud-link tag-link-32 tag-link-position-3" style="font-size: 15.333333333333pt;" aria-label="Cake (4 items)">Cake</a>
<a href="celebration.html" class="tag-cloud-link tag-link-33 tag-link-position-4" style="font-size: 8pt;" aria-label="Celebration (2 items)">Celebration</a>
<a href="friends.html" class="tag-cloud-link tag-link-23 tag-link-position-5" style="font-size: 12pt;" aria-label="Friends (3 items)">Friends</a>
<a href="groom.html" class="tag-cloud-link tag-link-27 tag-link-position-6" style="font-size: 18pt;" aria-label="Groom (5 items)">Groom</a>
<a href="kids.html" class="tag-cloud-link tag-link-30 tag-link-position-7" style="font-size: 12pt;" aria-label="Kids (3 items)">Kids</a>
<a href="love.html" class="tag-cloud-link tag-link-24 tag-link-position-8" style="font-size: 18pt;" aria-label="Love (5 items)">Love</a></div>
</div></aside></div>
</div> </div>
</div>
<div class="eltdf-content-bottom">
<div class="eltdf-content-bottom-inner eltdf-full-width">
<div class="widget widget_eltdf_instagram_widget"> <ul class="eltdf-instagram-feed clearfix eltdf-col-7 eltdf-instagram-carousel eltdf-owl-slider" data-number-of-items="7" data-slider-margin="10" data-enable-navigation="no" data-enable-pagination="no"><li>
<a href="https://www.instagram.com/p/BiEqqTaDwIB/" target="_blank">
<span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
<img class="low_resolution" src="images/s320x320-30605393_603811343303045_6325435384654200832_n.jpg" alt="" width="320" height="320"></a>
</li>
<li>
<a href="https://www.instagram.com/p/BiEqongjq9I/" target="_blank">
<span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
<img class="low_resolution" src="images/s320x320-30602665_666976383634205_5416085766075842560_n.jpg" alt="" width="320" height="320"></a>
</li>
<li>
<a href="https://www.instagram.com/p/BiEqm-ajg7b/" target="_blank">
<span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
<img class="low_resolution" src="images/s320x320-31157005_993121947517406_5376739050986143744_n.jpg" alt="" width="320" height="320"></a>
</li>
<li>
<a href="https://www.instagram.com/p/BiEqjxDjmWR/" target="_blank">
<span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
<img class="low_resolution" src="images/s320x320-30601703_167995387231560_7542690190775549952_n.jpg" alt="" width="320" height="320"></a>
</li>
<li>
<a href="https://www.instagram.com/p/BiEqgIIjiTg/" target="_blank">
<span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
<img class="low_resolution" src="images/s320x320-30829389_190080938382728_8815398182943981568_n.jpg" alt="" width="320" height="320"></a>
</li>
<li>
<a href="https://www.instagram.com/p/BiEqeTMDEcT/" target="_blank">
<span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
<img class="low_resolution" src="images/s320x320-31118366_163054664369910_1345918763450499072_n.jpg" alt="" width="320" height="320"></a>
</li>
<li>
<a href="https://www.instagram.com/p/BiEqaMWj5iN/" target="_blank">
<span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
<img class="low_resolution" src="images/s320x320-30903722_171831546852478_5001505462189817856_n.jpg" alt="" width="320" height="320"></a>
</li>
<li>
<a href="https://www.instagram.com/p/BiEqX33DbFs/" target="_blank">
<span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
<img class="low_resolution" src="images/s320x320-30867878_197098437572789_9047513836415877120_n.jpg" alt="" width="320" height="320"></a>
</li>
<li>
<a href="https://www.instagram.com/p/BiEqUnODl3M/" target="_blank">
<span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
<img class="low_resolution" src="images/s320x320-30604239_466299773799687_8025880024529764352_n.jpg" alt="" width="320" height="320"></a>
</li>
</ul></div> </div>
</div></div> 
</div> 
<footer class="eltdf-page-footer "><div class="eltdf-footer-top-holder">
<div class="eltdf-footer-top-inner eltdf-grid">
<div class="eltdf-grid-row eltdf-footer-top-alignment-center">
<div class="eltdf-column-content eltdf-grid-col-4">
<div class="widget eltdf-blog-list-widget"><div class="eltdf-widget-title-holder"><h4 class="eltdf-widget-title">Recent Posts</h4></div><div class="eltdf-blog-list-holder eltdf-grid-list eltdf-disable-bottom-space eltdf-bl-minimal eltdf-one-columns eltdf-normal-space eltdf-bl-pag-no-pagination" data-type="minimal" data-number-of-posts="2" data-number-of-columns="one" data-space-between-items="normal" data-orderby="date" data-order="ASC" data-image-size="thumbnail" data-title-tag="h5" data-excerpt-length="40" data-post-info-section="yes" data-post-info-image="yes" data-post-info-author="yes" data-post-info-date="yes" data-post-info-category="yes" data-post-info-comments="no" data-post-info-like="no" data-post-info-share="no" data-pagination-type="no-pagination" data-max-num-pages="6" data-next-page="2">
<div class="eltdf-bl-wrapper eltdf-outer-space">
<ul class="eltdf-blog-list"><li class="eltdf-bl-item eltdf-item-space clearfix">
<div class="eltdf-bli-inner">
<div class="eltdf-bli-content">
<h5 itemprop="name" class="entry-title eltdf-post-title">
<a itemprop="url" href="spring-color-mood.html" title="Spring Color Mood">
Spring Color Mood </a>
</h5> <div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
<a itemprop="url" href="05.html">
10. May 2018. </a>
<meta itemprop="interactionCount" content="UserComments: 0"></div> </div>
</div>
</li><li class="eltdf-bl-item eltdf-item-space clearfix">
<div class="eltdf-bli-inner">
<div class="eltdf-bli-content">
<h5 itemprop="name" class="entry-title eltdf-post-title">
<a itemprop="url" href="double-wedding.html" title="Double Wedding">
Double Wedding </a>
</h5> <div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
<a itemprop="url" href="05.html">
10. May 2018. </a>
<meta itemprop="interactionCount" content="UserComments: 0"></div> </div>
</div>
</li> </ul></div>
</div></div> </div>
<div class="eltdf-column-content eltdf-grid-col-4">
<div id="media_image-2" class="widget eltdf-footer-column-2 widget_media_image"><a href="gretnagreen.qodeinteractive.html"><img width="298" height="37" src="images/04-footer-logo.png" class="image wp-image-168  attachment-full size-full" alt="a" style="max-width: 100%; height: auto;"></a></div><div id="text-2" class="widget eltdf-footer-column-2 widget_text"> <div class="textwidget"><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor aenean massa et ante tincidunt</p>
</div>
</div><div id="text-4" class="widget eltdf-footer-column-2 widget_text"> <div class="textwidget"><p><em>Email us: <a href="mailto:info@example.com">info@example.com</a></em><br><em> Phone us: <a href="tel:04082667759">0408 266 7759</a></em><br><em> Visit us: </em><a href="https://www.google.rs/maps/place/20+S+Summer+St,+Edgartown,+MA+02539,+USA/@41.3894685,-70.5168058,17z/data=!3m1!4b1!4m5!3m4!1s0x89e52c24d2d7de6b:0xd4c7fdff2990c897!8m2!3d41.3894645!4d-70.5146171" target="_blank" rel="noopener noreferrer"><em>20 South Summer St, USA</em></a></p>
</div>
</div> </div>
<div class="eltdf-column-content eltdf-grid-col-4">
<div id="nav_menu-2" class="widget eltdf-footer-column-3 widget_nav_menu"><div class="eltdf-widget-title-holder"><h4 class="eltdf-widget-title">Quick Links</h4></div><div class="menu-footer-pages-container"><ul id="menu-footer-pages" class="menu"><li id="menu-item-3297" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3297"><a href="our-story.html">Our Story</a></li>
<li id="menu-item-3298" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3298"><a href="meet-the-team.html">Meet The Team</a></li>
<li id="menu-item-3299" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3299"><a href="rsvp.html">RSVP</a></li>
<li id="menu-item-3300" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3300"><a href="our-services.html">Our Services</a></li>
</ul></div></div> </div>
</div>
</div>
</div><div class="eltdf-footer-bottom-holder">
<div class="eltdf-footer-bottom-inner eltdf-grid">
<div class="eltdf-grid-row ">
<div class="eltdf-grid-col-4">
</div>
<div class="eltdf-grid-col-4">
<div id="text-5" class="widget eltdf-footer-bottom-column-2 widget_text"> <div class="textwidget"><p>2018 COPYRIGHT @ <a href="https://qodeinteractive.com/theme-author/elated-wordpress-themes/" target="_blank" rel="noopener noreferrer">ELATED THEMES</a></p>
</div>
</div> </div>
<div class="eltdf-grid-col-4">
</div>
</div>
</div>
</div> </footer></div> 
</div> 
<div class="rbt-toolbar" data-theme="Gretna Green" data-featured="" data-button-position="50%" data-button-horizontal="right" data-button-alt="no"></div>

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KLJLSX7" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
 <script type="text/javascript">
		var c = document.body.className;
		c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
		document.body.className = c;
	</script><script type="text/javascript">
				function revslider_showDoubleJqueryError(sliderID) {
					var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
					errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
					errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body option to true.";
					errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
					errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "";
						jQuery(sliderID).show().html(errorMessage);
				}
			</script><script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/gretnagreen.qodeinteractive.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"cached":"1"};
/* ]]> */
</script><script src="js/minify-0fef6.js"></script><script type="text/javascript" src="js/js-jquery.lazy.min.js"></script><script type="text/javascript" src="js/js-rbt-modules.js"></script><script src="js/minify-fe83b.js"></script><script type="text/javascript">
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};
/* ]]> */
</script><script src="js/minify-63a69.js"></script><script type="text/javascript">
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_a69fe1bfc56bd65cc66c092c7ed77c49","fragment_name":"wc_fragments_a69fe1bfc56bd65cc66c092c7ed77c49"};
/* ]]> */
</script><script src="js/minify-8c5b5.js"></script><script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyAzBOS_lWyH5YrpIEDUdlAkFLOnFud-5G8&amp;ver=5.2.2"></script><script type="text/javascript">
/* <![CDATA[ */
var eltdfGlobalVars = {"vars":{"eltdfAddForAdminBar":0,"eltdfElementAppearAmount":-100,"eltdfAjaxUrl":"https:\/\/gretnagreen.qodeinteractive.com\/wp-admin\/admin-ajax.php","eltdfStickyHeaderHeight":0,"eltdfStickyHeaderTransparencyHeight":0,"eltdfTopBarHeight":0,"eltdfLogoAreaHeight":0,"eltdfMenuAreaHeight":100,"eltdfMobileHeaderHeight":70}};
var eltdfPerPageVars = {"vars":{"eltdfMobileHeaderHeight":70,"eltdfStickyScrollAmount":0,"eltdfHeaderTransparencyHeight":100,"eltdfHeaderVerticalWidth":0}};
/* ]]> */
</script><script src="js/minify-fe889.js"></script></body></html>
