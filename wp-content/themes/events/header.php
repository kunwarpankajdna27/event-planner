<?php
global $current_user;
 wp_get_current_user();
?>
<!DOCTYPE html>
<html lang="en-US">
<head>

    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=yes" />
    <title><?php the_title(); ?> | <?php echo get_bloginfo( 'name' ); ?></title>

    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/bootstrap-theme.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/font-awesome.min.css">
    <link href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/owl.theme.default.min.css">
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.js"></script>

    <link rel="stylesheet" id="wp-block-library-css" href="<?php bloginfo('template_url'); ?>/css/block-library-style.min.css" type="text/css" media="all" />
    <link rel="stylesheet" id="contact-form-7-css" href="<?php bloginfo('template_url'); ?>/css/css-styles.css" type="text/css" media="all" />
    <link rel="stylesheet" id="rs-plugin-settings-css" href="<?php bloginfo('template_url'); ?>/css/css-settings.css" type="text/css" media="all" />
    <link rel="stylesheet" id="gretnagreen-elated-default-style-css" href="<?php bloginfo('template_url'); ?>/css/gretnagreen-style.css" type="text/css" media="all" />
    <link rel="stylesheet" id="gretnagreen-elated-modules-css" href="<?php bloginfo('template_url'); ?>/css/css-modules.min.css" type="text/css" media="all" />
    <link rel="stylesheet" id="eltdf-dripicons-css" href="<?php bloginfo('template_url'); ?>/css/dripicons-dripicons.css" type="text/css" media="all" />
    <link rel="stylesheet" id="eltdf-font_elegant-css" href="<?php bloginfo('template_url'); ?>/css/elegant-icons-style.min.css" type="text/css" media="all" />
    <link rel="stylesheet" id="eltdf-font_awesome-css" href="<?php bloginfo('template_url'); ?>/css/css-fontawesome-all.min.css" type="text/css" media="all" />
    <link rel="stylesheet" id="eltdf-ion_icons-css" href="<?php bloginfo('template_url'); ?>/css/css-ionicons.min.css" type="text/css" media="all" />
    <link rel="stylesheet" id="eltdf-linea_icons-css" href="<?php bloginfo('template_url'); ?>/css/linea-icons-style.css" type="text/css" media="all" />
    <link rel="stylesheet" id="eltdf-linear_icons-css" href="<?php bloginfo('template_url'); ?>/css/linear-icons-style.css" type="text/css" media="all" />
    <link rel="stylesheet" id="eltdf-simple_line_icons-css" href="<?php bloginfo('template_url'); ?>/css/simple-line-icons-simple-line-icons.css" type="text/css" media="all" />
    <link rel="stylesheet" id="mediaelement-css" href="<?php bloginfo('template_url'); ?>/css/mediaelement-mediaelementplayer-legacy.min.css" type="text/css" media="all" />
    <link rel="stylesheet" id="wp-mediaelement-css" href="<?php bloginfo('template_url'); ?>/css/mediaelement-wp-mediaelement.min.css" type="text/css" media="all" />
    <link rel="stylesheet" id="gretnagreen-elated-woo-css" href="<?php bloginfo('template_url'); ?>/css/css-woocommerce.min.css" type="text/css" media="all" />
    <link rel="stylesheet" id="gretnagreen-elated-woo-responsive-css" href="<?php bloginfo('template_url'); ?>/css/css-woocommerce-responsive.min.css" type="text/css" media="all" />
    <link rel="stylesheet" id="gretnagreen-elated-style-dynamic-css" href="<?php bloginfo('template_url'); ?>/css/css-style_dynamic.css" type="text/css" media="all" />
    <link rel="stylesheet" id="gretnagreen-elated-modules-responsive-css" href="<?php bloginfo('template_url'); ?>/css/css-modules-responsive.min.css" type="text/css" media="all" />
    <link rel="stylesheet" id="gretnagreen-elated-style-dynamic-responsive-css" href="<?php bloginfo('template_url'); ?>/css/css-style_dynamic_responsive.css" type="text/css" media="all" />

    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/owl.theme.default.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/animate.css">
   

        <link rel="stylesheet" id="js_composer_front-css" href="<?php bloginfo('template_url'); ?>/css/css-js_composer.min.css" type="text/css" media="all" />
        
        <script src="<?php bloginfo('template_url'); ?>/js/minify-eff97.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/minify-f1253.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/minify-864c2.js"></script>
        <noscript>
            <style>
            .woocommerce-product-gallery {
                opacity: 1 !important;
            }
            .wpb_animate_when_almost_visible {
                opacity: 1;
            }
        </style>
    </noscript>


    <script src="<?php bloginfo('template_url'); ?>/js/revolution.extension.layeranimation.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/revolution.extension.parallax.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/revolution.extension.slideanims.min.js"></script>

    <script src="<?php bloginfo('template_url'); ?>/js/custom.js"></script>

    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/style.css">
    <?php 
    // wp_head(); 
    ?>
</head>
<body
class="home page-template page-template-full-width page-template-full-width-php page page-id-6 gretnagreen-core-1.0 woocommerce-no-js event planner-ver-1.0 eltdf-grid-1100 eltdf-content-is-behind-header eltdf-wide-dropdown-menu-content-in-grid eltdf-light-header eltdf-sticky-header-on-scroll-down-up eltdf-dropdown-default eltdf-header-standard eltdf-menu-area-shadow-disable eltdf-menu-area-in-grid-shadow-disable eltdf-menu-area-border-disable eltdf-menu-area-in-grid-border-disable eltdf-logo-area-border-disable eltdf-header-vertical-shadow-disable eltdf-header-vertical-border-disable eltdf-side-menu-slide-from-right eltdf-woocommerce-columns-3 eltdf-woo-small-space eltdf-woo-pl-info-below-image eltdf-woo-single-thumb-below-image eltdf-woo-single-has-pretty-photo eltdf-default-mobile-header eltdf-sticky-up-mobile-header eltdf-fullscreen-search eltdf-search-fade wpb-js-composer js-comp-ver-6.0.5 vc_responsive"
>
<div class="eltdf-wrapper">
    <div class="eltdf-wrapper-inner">
        <header class="eltdf-page-header" id="navbar">
            <div class="eltdf-menu-area home-header eltdf-menu-center">
                <div class="eltdf-vertical-align-containers">
                    <div class="eltdf-position-left">
                        <div class="eltdf-position-left-inner">

                            <?php

                                $menu = wp_get_nav_menu_object('header');
                                $logo = get_field('home_logo', $menu);

                            ?>

                                <a itemprop="url" href="<?php echo get_site_url(); ?>">
                                    <img itemprop="image" class="eltdf-light-logo webLogo" src="<?php echo $logo; ?>" width="200" alt="light logo" />
                                </a>
                        </div>
                    </div>
                    <div class="eltdf-position-right">
                        <div class="eltdf-position-right-inner">
                            <div class="eltdf-shopping-cart-holder" style="margin: 0 25px 0 0;">
                                    <nav class="eltdf-main-menu eltdf-drop-down eltdf-default-nav">

                                        <?php 
                                        wp_nav_menu(array(
                                            'theme_location' => 'header',
                                            'menu_class'    => '',
                                            'container'     => '',
                                            'container_class'=>'',
                                            'container_id' => ''
                                        ))
                                        ?>
                                    </nav>
                                  <div class="login-user"> 
                                     <?php if ( is_user_logged_in() ) { ?> 
                                        <a href="<?php echo the_permalink(145); ?>">  <?php echo  $current_user->user_login; ?> </a>
                                        <ul class="user-sub-menu">
                                            <li>
                                                <a href="<?php echo the_permalink(145); ?>">User Eevnts</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo the_permalink(196); ?>">User Gallery</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo wp_logout_url(get_permalink(170))?>"> Log Out</a>
                                            </li>
                                        </ul>


                            <?php } else { ?>
                                             <a href="<?php echo the_permalink(170); ?>"> Login
                                             </a>
                                        <?php } ?>
                                      </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="eltdf-sticky-header">
                <div class="eltdf-sticky-holder eltdf-menu-center">
                    <div class="eltdf-vertical-align-containers">
                        <div class="eltdf-position-left">
                            <div class="eltdf-position-left-inner">
                                <div class="eltdf-logo-wrapper">
                                    <a itemprop="url" href="#" style="height: 27px;">
                                        <img itemprop="image" class="eltdf-normal-logo" src="<?php bloginfo('template_url'); ?>/images/05-logo-sticky-header.png" width="437" height="54" alt="logo" />
                                        <img itemprop="image" class="eltdf-dark-logo" src="<?php bloginfo('template_url'); ?>/images/05-logo-standard-header-dark.png" width="437" height="54" alt="dark logo" />
                                        <img itemprop="image" class="eltdf-light-logo" src="<?php bloginfo('template_url'); ?>/images/05-logo-standard-header-light.png" width="430" height="54" alt="light logo" />
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="eltdf-position-center">
                            <div class="eltdf-position-center-inner">
                                <nav class="eltdf-main-menu eltdf-drop-down eltdf-sticky-nav">
                                    <?php 
                                    wp_nav_menu(array(
                                        'theme_location' => 'header',
                                        'menu_class'    => '',
                                        'container'     => '',
                                        'container_class'=>'',
                                        'container_id' => ''
                                    ))
                                    ?>
                                </nav>
                            </div>
                        </div>
                        <div class="eltdf-position-right">
                            <div class="eltdf-position-right-inner">
                                 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <header class="eltdf-mobile-header">
            <div class="eltdf-mobile-header-inner">
                <div class="eltdf-mobile-header-holder">
                    <div class="eltdf-grid">
                        <div class="eltdf-vertical-align-containers">
                            <div class="eltdf-vertical-align-containers">
                                <div class="eltdf-mobile-menu-opener eltdf-mobile-menu-opener-icon-pack">
                                    <a href="javascript:void(0)">
                                        <span class="eltdf-mobile-menu-icon"> <span aria-hidden="true" class="eltdf-icon-font-elegant icon_menu"></span> </span>
                                    </a>
                                </div>
                                <div class="eltdf-position-center">
                                    <div class="eltdf-position-center-inner">
                                        <div class="eltdf-mobile-logo-wrapper">

                                            <?php

                                                $menu = wp_get_nav_menu_object('header');
                                                $logo = get_field('home_logo', $menu);

                                            ?>

                                            <a itemprop="url" href="#" style="height: 21px;">
                                                <img itemprop="image" src="<?php echo $logo; ?>" style="height: 43px !important" width="350" height="43" alt="Mobile Logo" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="eltdf-position-right">
                                    <div class="eltdf-position-right-inner"></div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <nav class="eltdf-mobile-nav">
                    <?php 
                        wp_nav_menu(array(
                            'theme_location' => 'header',
                            'menu_class'    => '',
                            'container'     => '',
                            'container_class'=>'',
                            'container_id' => ''
                        ))
                    ?>
                  <div class="m-login">
                    <?php if ( is_user_logged_in() ) { ?>  
                        <!-- <a href="<?php echo the_permalink(145); ?>">  <?php echo  $current_user->user_login; ?> </a> -->
                        <ul>
                            <li>
                                <a href="<?php echo the_permalink(145); ?>">User Eevnts</a>
                            </li>
                            <li>
                                <a href="<?php echo the_permalink(196); ?>">User Gallery</a>
                            </li>
                            <li>
                                <a href="<?php echo wp_logout_url(get_permalink(170))?>"> Log Out</a>
                            </li>
                        </ul>

                            <?php } else { ?>
                                             <a href="<?php echo the_permalink(170); ?>"> Login
                                             </a>
                                        <?php } ?>
                  </div>      
                </nav>

            </div>
        </header>