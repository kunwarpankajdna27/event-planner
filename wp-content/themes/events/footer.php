                                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1526023310740 eltdf-content-aligment-center">
                                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                                <div class="vc_column-inner">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_center">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper vc_box_border_grey">
                                                                    <img width="294" height="190" src="<?php bloginfo('template_url'); ?>/images/04-h1-img-15.jpg" class="vc_single_image-img attachment-medium" alt="a" />
                                                                </div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="eltdf-content-bottom">
                            <div class="eltdf-content-bottom-inner eltdf-full-width">
                                <!-- <div class="widget widget_text">
                                    <div class="textwidget">
                                        <div class="eltdf-section-title-holder eltdf-st-standard">
                                            <div class="eltdf-st-inner">
                                                <h1 class="eltdf-st-title">
                                                    Follow us on Instagram
                                                </h1>
                                                <h6 class="eltdf-st-separator-subtitle">happiness</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="widget eltdf-separator-widget">
                                    <div class="eltdf-separator-holder clearfix eltdf-separator-center eltdf-separator-normal">
                                        <div class="eltdf-separator" style="border-style: solid; margin-top: 9px;"></div>
                                    </div>
                                </div>
                                <div class="widget widget_eltdf_instagram_widget">
                                    <ul class="eltdf-instagram-feed clearfix eltdf-col-7 eltdf-instagram-carousel eltdf-owl-slider" data-number-of-items="7" data-slider-margin="10" data-enable-navigation="no" data-enable-pagination="no">
                                        <li>
                                            <a href="https://www.instagram.com/p/BiEqqTaDwIB/" target="_blank">
                                                <span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
                                                <img class="low_resolution" src="<?php bloginfo('template_url'); ?>/images/s320x320-30605393_603811343303045_6325435384654200832_n.jpg" alt="" width="320" height="320" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.instagram.com/p/BiEqongjq9I/" target="_blank">
                                                <span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
                                                <img class="low_resolution" src="<?php bloginfo('template_url'); ?>/images/s320x320-30602665_666976383634205_5416085766075842560_n.jpg" alt="" width="320" height="320" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.instagram.com/p/BiEqm-ajg7b/" target="_blank">
                                                <span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
                                                <img class="low_resolution" src="<?php bloginfo('template_url'); ?>/images/s320x320-31157005_993121947517406_5376739050986143744_n.jpg" alt="" width="320" height="320" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.instagram.com/p/BiEqjxDjmWR/" target="_blank">
                                                <span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
                                                <img class="low_resolution" src="<?php bloginfo('template_url'); ?>/images/s320x320-30601703_167995387231560_7542690190775549952_n.jpg" alt="" width="320" height="320" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.instagram.com/p/BiEqgIIjiTg/" target="_blank">
                                                <span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
                                                <img class="low_resolution" src="<?php bloginfo('template_url'); ?>/images/s320x320-30829389_190080938382728_8815398182943981568_n.jpg" alt="" width="320" height="320" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.instagram.com/p/BiEqeTMDEcT/" target="_blank">
                                                <span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
                                                <img class="low_resolution" src="<?php bloginfo('template_url'); ?>/images/s320x320-31118366_163054664369910_1345918763450499072_n.jpg" alt="" width="320" height="320" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.instagram.com/p/BiEqaMWj5iN/" target="_blank">
                                                <span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
                                                <img class="low_resolution" src="<?php bloginfo('template_url'); ?>/images/s320x320-30903722_171831546852478_5001505462189817856_n.jpg" alt="" width="320" height="320" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.instagram.com/p/BiEqX33DbFs/" target="_blank">
                                                <span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
                                                <img class="low_resolution" src="<?php bloginfo('template_url'); ?>/images/s320x320-30867878_197098437572789_9047513836415877120_n.jpg" alt="" width="320" height="320" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.instagram.com/p/BiEqUnODl3M/" target="_blank">
                                                <span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
                                                <img class="low_resolution" src="<?php bloginfo('template_url'); ?>/images/s320x320-30604239_466299773799687_8025880024529764352_n.jpg" alt="" width="320" height="320" />
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="widget eltdf-separator-widget">
                                    <div class="eltdf-separator-holder clearfix eltdf-separator-center eltdf-separator-normal">
                                        <div class="eltdf-separator" style="border-style: solid; margin-top: -2px;"></div>
                                    </div>
                                </div>
                                <a
                                    class="eltdf-icon-widget-holder eltdf-icon-has-hover"
                                    data-hover-color="#fbb8ac"
                                    data-color="#9f9f9f"
                                    href="<?php echo do_shortcode('[contact type="facebook"]'); ?>"
                                    target="_blank"
                                    style="color: #9f9f9f; margin: 0px 46px 0px 0px;"
                                >
                                    <span class="eltdf-icon-element fab fa-facebook-f"></span> <span class="eltdf-icon-text">Facebook</span>
                                </a>
                                <a
                                    class="eltdf-icon-widget-holder eltdf-icon-has-hover"
                                    data-hover-color="#fbb8ac"
                                    data-color="#9f9f9f"
                                    href="<?php echo do_shortcode('[contact type="google"]'); ?>"
                                    target="_blank"
                                    style="color: #9f9f9f; margin: 0px 46px 0px 0px;"
                                >
                                    <span class="eltdf-icon-element fab fa-instagram"></span> <span class="eltdf-icon-text">Instagram</span>
                                </a>
                                <a
                                    class="eltdf-icon-widget-holder eltdf-icon-has-hover"
                                    data-hover-color="#fbb8ac"
                                    data-color="#9f9f9f"
                                    href="<?php echo do_shortcode('[contact type="pinterest"]'); ?>"
                                    target="_blank"
                                    style="color: #9f9f9f; margin: 0px 47px 0px 0px;"
                                >
                                    <span class="eltdf-icon-element fab fa-pinterest-p"></span> <span class="eltdf-icon-text" style="font-size: 15px;">Pinterest</span>
                                </a>
                                <a
                                    class="eltdf-icon-widget-holder eltdf-icon-has-hover"
                                    data-hover-color="#fbb8ac"
                                    data-color="#9f9f9f"
                                    href="<?php echo do_shortcode('[contact type="twitter"]'); ?>"
                                    target="_blank"
                                    style="color: #9f9f9f; margin: 0px 52px 0px 0px;"
                                >
                                    <span class="eltdf-icon-element fab fa-twitter"></span> <span class="eltdf-icon-text">Twitter</span>
                                </a>
                                <div class="widget eltdf-separator-widget">
                                    <div class="eltdf-separator-holder clearfix eltdf-separator-center eltdf-separator-normal">
                                        <div class="eltdf-separator" style="border-style: solid; margin-bottom: 22px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="rbt-toolbar" data-theme="Gretna Green" data-featured="" data-button-position="50%" data-button-horizontal="right" data-button-alt="no"></div>

        <script type="text/javascript">
            var c = document.body.className;
            c = c.replace(/woocommerce-no-js/, "woocommerce-js");
            document.body.className = c;
        </script>

        <script>
            window.onscroll = function() {myFunction()};

            var navbar = document.getElementById("navbar");
            var sticky = navbar.offsetTop;

            function myFunction() {
              if (window.pageYOffset > sticky) {
                navbar.classList.add("sticky")
              }
              else if (window.pageYOffset < sticky)
              {
                navbar.classList.remove("sticky");
              }
              else {
                navbar.classList.remove("sticky");
              }
            }
        </script>

        <script type="text/javascript">
            function revslider_showDoubleJqueryError(sliderID) {
                var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
                errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
                errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body option to true.";
                errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
                errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "";
                jQuery(sliderID).show().html(errorMessage);
            }
        </script>
        
        <script src="<?php bloginfo('template_url'); ?>/js/minify-0fef6.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/js-jquery.lazy.min.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/js-rbt-modules.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/minify-fe83b.js"></script>
        <script type="text/javascript">
            /* <![CDATA[ */
            var woocommerce_params = { ajax_url: "\/wp-admin\/admin-ajax.php", wc_ajax_url: "\/?wc-ajax=%%endpoint%%" };
            /* ]]> */
        </script>
        <script src="<?php bloginfo('template_url'); ?>/js/minify-63a69.js"></script>
       
        <script src="<?php bloginfo('template_url'); ?>/js/minify-8c5b5.js"></script>

        <script src="<?php bloginfo('template_url'); ?>/js/minify-be7fe.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/owl.carousel.js"></script>
        <script type="text/javascript">
/* <![CDATA[ */
var eltdfGlobalVars = {
    "vars":{
        // "eltdfAddForAdminBar":0,
        // "eltdfElementAppearAmount":-100,
        // "eltdfStickyHeaderHeight":0,
        // "eltdfStickyHeaderTransparencyHeight":77,
        // "eltdfTopBarHeight":0,
        // "eltdfLogoAreaHeight":0,
        // "eltdfMenuAreaHeight":100,
        // "eltdfMobileHeaderHeight":70
    }
};
var eltdfPerPageVars = {
    "vars":{
        // "eltdfMobileHeaderHeight":70,
        // "eltdfStickyScrollAmount":1150,
        // "eltdfHeaderTransparencyHeight":0,
        // "eltdfHeaderVerticalWidth":0
    }
};
/* ]]> */
</script>
        <?php wp_footer(); ?>
    </body>
</html>
