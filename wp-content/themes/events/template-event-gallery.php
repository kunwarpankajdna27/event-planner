<?php

// Template Name:Event Gallery

if( !is_user_logged_in() ) {
    wp_redirect('login');
    exit;
}

get_header('inner'); 

the_post();

$user_id = get_current_user_id();
$user = get_userdata($user_id);

?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css" integrity="sha512-nNlU0WK2QfKsuEmdcTwkeh+lhGs6uyOxuUs+n+0oXSYDok5qy0EI0lt01ZynHq6+p/tbgpZ7P+yUb+r71wqdXg==" crossorigin="anonymous" />

<a id="eltdf-back-to-top" href="#">
    <span class="eltdf-text-stack"> top </span>
</a>

<section class="inner-banner">
  <div class="container-fluid">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
          <h1><?php the_title(); ?></h1>
        </div>
      </div>
    </div>
  </div>
</section>


<?php

    $arraydata = array(
      'post_type' => 'user_events' ,
      'post_status' => 'publish' ,
      'order' => 'DESC' ,
      'orderby' => 'data',
      'meta_query' => array(
                  array(
                      'key' => 'username', 
                      'value' => $user_id, 
                      'compare'   => '=',
                  ),           
              ),
       );

    $post_data = new WP_Query($arraydata);
    while( $post_data->have_posts() ) 
    {
      $post_data->the_post();

      $events = get_field('detail',get_the_id());
      
        foreach ($events as $key => $event) 
        {
?>
 
<section class="gallery-block">
  <div class="container-fluid">
    <div class="container">
      <div class="row">
                <h3><?php echo $event['title'] ?></h3>
        <div class="eltdf-portfolio-list-holder eltdf-grid-list eltdf-grid-masonry-list eltdf-disable-bottom-space eltdf-pl-masonry eltdf-four-columns eltdf-normal-space eltdf-pl-gallery-overlay eltdf-pl-pag-no-pagination" data-type="masonry" data-number-of-columns="four" data-space-between-items="normal" data-number-of-items="13" data-image-proportions="full" data-enable-fixed-proportions="no" data-enable-image-shadow="no" data-category="decoration" data-orderby="date" data-order="ASC" data-item-style="gallery-overlay" data-enable-title="yes" data-title-tag="h4" data-enable-category="no" data-enable-count-images="yes" data-enable-excerpt="no" data-excerpt-length="20" data-pagination-type="no-pagination" data-filter="no" data-filter-order-by="name" data-enable-article-animation="no" data-portfolio-slider-on="no" data-enable-loop="yes" data-enable-autoplay="yes" data-slider-speed="5000" data-slider-speed-animation="600" data-enable-navigation="yes" data-enable-pagination="yes" data-max-num-pages="2" data-next-page="2">
        <div class="eltdf-pl-inner eltdf-outer-space eltdf-masonry-list-wrapper clearfix">
                    <div class="eltdf-masonry-grid-sizer"></div>
                    <div class="eltdf-masonry-grid-gutter"></div>

                    <?php

                        foreach ($event['gallery_images'] as $key => $image) 
                        {
                    ?>

                    <article
                        class="eltdf-pl-item eltdf-item-space post-1402 portfolio-item type-portfolio-item status-publish has-post-thumbnail hentry portfolio-category-decoration portfolio-tag-ceremony portfolio-tag-dresses"
                    >
                        <div class="eltdf-pl-item-inner">
                            <div class="eltdf-pli-image">
                                <a class="fancybox" rel="group" href="<?php echo $image['event_image']; ?>">
                                    <img
                                        width="800"
                                        height="1200"
                                        src="<?php echo $image['event_image']; ?>"
                                        class="attachment-full size-full wp-post-image"
                                        alt="<?php the_title(); ?>"
                                        sizes="(max-width: 800px) 100vw, 800px"
                                    />
                                </a>
                            </div>
                        </div>
                    </article>

                  <?php } ?>

                  
                </div>
            </div>
        
      </div>
    </div>
  </div>
</section>

                       
<?php
 
 } }

get_footer();


?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js" integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA==" crossorigin="anonymous"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/helpers/jquery.fancybox-buttons.js"></script>

<script type="text/javascript">
    $(document).ready(function() {

      $.fancybox.defaults.btnTpl.exif = '<button id="whatsappli" data-fancybox-exif class="fancybox-button fancybox-button--exif" title="Whatsapp"  onClick="javascript:fbShareSlide(this)" ><i class="fab fa-whatsapp"></i></button>';


        $(".fancybox").fancybox({
            'transitionIn'  :   'elastic',
            'transitionOut' :   'elastic',
            'speedIn'   :   600, 
            'speedOut'  :   200, 
            'overlayShow'   :   false,
            'cyclic'    :   true,
            'showNavArrows' :   true,
            buttons : [
                'share',
                'fullScreen',
                'close',
                'share',
                'download',
                'exif'
            ]

        });
    });

    function fbShareSlide(e) {

      var href = $('#whatsappli').parent('.fancybox-toolbar').find('a').attr('href');
       if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) 
       {
        var message = encodeURIComponent(href);
        var whatsapp_url = "whatsapp://send?text=" + message;
        window.location.href = whatsapp_url;
      } else {
        alert("Please use an Mobile Device to Share this picture.");
      }
    }

</script>
