<?php
// Template Name: home
session_start();
if(isset($_POST['send'])){
  $id = wp_insert_post(array(
    'post_title'=>$_POST['uname'], 
    'post_type'=>'user-enquiry',
    'post_status' => 'publish',
    'post_content'=>$_POST['msg']
  ));

  update_post_meta($id, 'number', $_POST['number']);
  update_post_meta($id, 'packages', $_POST['packages']);
  $url = site_url().'/#footer';
  $_SESSION['MSG']=1;
  wp_redirect($url);
  exit();
}


get_header();

the_post();

$homeVideo = get_field("home_video",get_the_id());

?>
<a id="eltdf-back-to-top" href="#">
  <span class="eltdf-text-stack"> top </span>
</a>
<div class="eltdf-content" style="margin-top: -100px;">
  <div class="eltdf-content-inner">
    <div class="eltdf-full-width">
      <div class="eltdf-full-width-inner">
        <div class="eltdf-grid-row">
          <div class="eltdf-page-content-holder eltdf-grid-col-12">
            <div class="vc_row wpb_row vc_row-fluid" style="background-color: #fbb8ac;">
              <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                  <div class="wpb_wrapper">
                    <div class="wpb_revslider_element wpb_content_element">
                      <link href="https://fonts.googleapis.com/css?family=Oswald:300%7CPlayfair+Display:400" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
                      <div id="rev_slider_11_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-source="gallery" style="background: transparent; padding: 0px;">
                        <div id="rev_slider_11_1" class="rev_slider fullscreenbanner" style="display: none;" data-version="5.4.7.2">
                          <ul>
                            <li data-index="rs-22" data-transition="crossfade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-rotate="0" data-fstransition="crossfade" data-fsmasterspeed="300" data-fsslotamount="7" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description=""> 
                             <img src="<?php bloginfo('template_url'); ?>/images/05-h1-slider-1-background-img.jpg"     alt="a"     title="h1-slider-1-background-img"     width="1920"     height="1100"     data-bgposition="center center"     data-bgfit="cover"     data-bgrepeat="no-repeat"     class="rev-slidebg"     data-no-retina/>
                           </li>

                           <li data-index="rs-23" data-transition="crossfade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">

                             <img src="<?php bloginfo('template_url'); ?>/images/images-transparent.png" data-bgcolor="#bdba70"     style="background: #bdba70;" alt="" title="Main Home"     data-bgposition="center center" data-bgfit="cover"     data-bgrepeat="no-repeat"     class="rev-slidebg"     data-no-retina/>

                           </li>

                           <li data-index="rs-24" data-transition="crossfade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">

                             <img src="<?php bloginfo('template_url'); ?>/images/images-transparent.png" data-bgcolor="#dfc9ab" style="background: #dfc9ab;" alt="" title="Main Home" data-bgposition="center center" data-bgfit="cover"data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina/>
                           </li>

                         </ul>
                         <div style="" class="tp-static-layers">
                          <div class="tp-caption tp-resizeme tp-static-layer" id="slider-11-layer-12" data-x="['left','left','left','left']" data-hoffset="['0','0','0','-500']" data-y="['top','top','top','top']" data-voffset="['0','0','0','7']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['on','on','on','off']" data-type="image" data-basealign="slide" data-responsive_offset="on" data-startslide="0" data-endslide="2" data-frames='[{"delay":400,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5;">
                            <img src="<?php bloginfo('template_url'); ?>/images/05-h1-slider-1-img-4.png" alt="a" data-ww="['939px','550px','450px','450px']" data-hh="['872px','511px','418px','418px']" width="939" height="872" data-no-retina />
                          </div>

                          <div class="tp-caption tp-resizeme tp-static-layer" id="slider-11-layer-13" data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['0','0','0','0']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="image" data-basealign="slide" data-responsive_offset="on" data-startslide="0" data-endslide="2" data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6;">
                            <img src="<?php bloginfo('template_url'); ?>/images/05-h1-slider-1-img-2.png" alt="a" data-ww="['805px','540px','420px','300px']" data-hh="['322px','216px','168px','120px']" width="805" height="322" data-no-retina />
                          </div>

                          <div class="tp-caption tp-resizeme tp-static-layer" id="slider-11-layer-4" data-x="['left','left','left','left']" data-hoffset="['100','120','0','-451']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['0','0','0','56']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['on','on','on','off']" data-type="image" data-basealign="slide" data-responsive_offset="on" data-startslide="0" data-endslide="2" data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7;"> 

                           <img src="<?php bloginfo('template_url'); ?>/images/05-h1-slider-1-img-5.png" alt="a" data-ww="['494px','291px','291px','300px']" data-hh="['373px','220px','220px','227px']" width="494" height="373" data-no-retina />
                         </div>

                         <div class="tp-caption tp-resizeme tp-static-layer" id="slider-11-layer-5" data-x="['right','right','right','right']" data-hoffset="['0','0','0','-573']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['0','0','0','61']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['on','on','on','off']" data-type="image" data-basealign="slide" data-responsive_offset="on" data-startslide="0" data-endslide="2" data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 8;"> 
                           <img     src="<?php bloginfo('template_url'); ?>/images/05-h1-slider-1-img-7.png"     alt="a"     data-ww="['622px','372px','372px','372px']"     data-hh="['368px','220px','220px','220px']"     width="622"     height="368"     data-no-retina />
                         </div>

                         <div class="tp-caption tp-resizeme tp-static-layer" id="slider-11-layer-6" data-x="['center','center','center','center']" data-hoffset="['-25','-25','-575','0']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['0','0','191','0']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['on','on','off','on']" data-type="image" data-basealign="slide" data-responsive_offset="on" data-startslide="0" data-endslide="2" data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 9;"> 
                           <img     src="<?php bloginfo('template_url'); ?>/images/05-h1-slider-1-img-6.png"     alt="a"     data-ww="['694px','469px','469px','300px']"     data-hh="['222px','150px','150px','96px']"     width="694"     height="222"     data-no-retina />
                         </div>

                         <div class="tp-caption tp-resizeme tp-static-layer" id="slider-11-layer-7" data-x="['right','right','right','right']" data-hoffset="['0','0','-252','-252']" data-y="['middle','middle','middle','middle']" data-voffset="['-30','-20','-79','-79']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['on','on','off','off']" data-type="image" data-basealign="slide" data-responsive_offset="on" data-startslide="0" data-endslide="2" data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 10;"> 
                           <img     src="<?php bloginfo('template_url'); ?>/images/05-h1-slider-1-img-3.png"     alt="a"     data-ww="['201px','115px','115px','115px']"     data-hh="['349px','200px','200px','200px']"     width="201"     height="349"     data-no-retina />
                         </div>

                         <div class="tp-caption tp-resizeme tp-static-layer" id="slider-11-layer-8" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-173','-160','-97','-20']" data-fontsize="['22','18','18','16']" data-lineheight="['58','50','50','45']" data-letterspacing="['26.4','21.6','21.6','19']" data-width="['275','227','226','200']" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-startslide="0" data-endslide="2" data-frames='[{"delay":300,"speed":700,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="     z-index: 11;     min-width: 275px;     max-width: 275px;     white-space: nowrap;     font-size: 22px;     line-height: 58px;     font-weight: 300;     color: #ffffff;     letter-spacing: 26.4px;     font-family: Oswald; "> WEDDINGS</div>
                         <div class="tp-caption tp-resizeme tp-static-layer" id="slider-11-layer-9" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-84','-89','-39','20']" data-fontsize="['170','110','100','45']" data-lineheight="['175','115','105','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-startslide="0" data-endslide="2" data-frames='[{"delay":500,"speed":700,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 12;     white-space: nowrap;     font-size: 170px;     line-height: 175px;     font-weight: 400;     color: #ffffff;     letter-spacing: 0px;     font-family: Playfair Display; ">
                           <?php echo get_bloginfo( 'name' ); ?>
                         </div>

                                                 <!-- <div class="tp-caption tp-resizeme tp-static-layer" id="slider-11-layer-10" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['59','20','56','80']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="image" data-responsive_offset="on" data-startslide="0" data-endslide="2" data-frames='[{"delay":700,"speed":700,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 13;"> 
                                                     <img     src="<?php bloginfo('template_url'); ?>/images/05-h1-slider-1-img-1.png"     alt="a"     data-ww="['530px','450px','420px','290px']"     data-hh="['77px','65px','61px','42px']"     width="530"     height="77"     data-no-retina />
                                                 </div>

                                                 <div class="tp-caption tp-resizeme tp-static-layer" id="slider-11-layer-11" data-x="['center','center','center','center']" data-hoffset="['0','0','0','-513']" data-y="['middle','middle','middle','middle']" data-voffset="['165','113','144','-97']" data-width="['689','604','604','604']" data-height="none" data-whitespace="normal" data-visibility="['on','on','on','off']" data-type="text" data-responsive_offset="on" data-startslide="0" data-endslide="2" data-frames='[{"delay":800,"speed":700,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textalign="['center','center','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="     z-index: 14;     min-width: 689px;     max-width: 689px;     white-space: normal;     font-size: 16px;     line-height: 26px;     font-weight: 400;     color: #ffffff;     letter-spacing: 0px;     font-family: Playfair Display; ">
                                                    Let the magic happens! Prepare for the big day with a team of experts that will make sure you enjoy every second of it. Celebrate your love in the stylish manner
                                                    with the best wedding organization in the city all at great price.
                                                  </div> -->

                                                </div>
                                                <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>


                                  <section class="locations">
                                    <div class="container-fluid"> 
                                      <div class="container">
                                        <div class="row">
                                          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12"> 
                                            <!-- <h3>Locations Type</h3> -->
                                            <ul>
                                              <div class="owl-carousel location-slide owl-theme test_slider">

                                                <?php

                                                $terms = get_terms( array(
                                                  'taxonomy' => 'location-category',
                                                  'hide_empty' => false,
                                  // 'number'  => 7
                                                ) );

                                                foreach($terms as $catdata) {

                                                  $taxonomy_images = get_option( 'taxonomy_image_plugin' );

                                                  $attachment_id = $taxonomy_images[$catdata->term_id];
                                                  $image = wp_get_attachment_image_src($attachment_id);

                                                  ?>

                                                  <li>
                                                    <img src="<?php echo $image[0] ?>" alt="<?php echo $catdata->name; ?>" class="img-responsive img-circle">
                                                    <span><a href="<?php echo get_term_link( $catdata->term_id ); ?>"><?php echo $catdata->name; ?></a></span>
                                                  </li>

                                                <?php } ?>
                                              </div>
                                            </ul>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </section>

                                  <section class="recent-eve timer">
                                    <div class="container-fluid"> 
                                      <div class="container">
                                        <div class="row">
                                          <div class="col-md-12">
                                            <div class="owl-carousel eve-slide owl-theme test_slider">

                                              <?php

                                              $today = date("Y-m-d");
                                              $arraydata = array(
                                                'post_type' => 'user_events' ,
                                                'post_status' => 'publish' ,
                                                'order' => 'DESC' ,
                                                'orderby' => 'data',
                                                'meta_query' => array(
                                                  array(
                                                    'key' => 'date', 
                                                    'value' => $today, 
                                                    'compare'   => '>=',
                                                    'type'    => 'DATE'
                                                  ),           
                                                ),
                                              );

                                              $post_data = new WP_Query($arraydata);
                                              $key = 0;
                                              while( $post_data->have_posts() ) 
                                              {
                                                $post_data->the_post();
                                                $events = get_field('detail',get_the_id());
                                                $key++;
                                                $EFullDate = explode('/',date('Y/m/d',strtotime($events[0]['date'])));
                                                ?>

                                                <div class="wpb_wrapper">
                                                  <div class="eltdf-invitation-holder">
                                                    <div class="eltdf-invitation-image">
                                                      <img
                                                      width="800"
                                                      height="1201"
                                                      src="<?php echo $events[0]['image'] ?>"
                                                      class="attachment-full size-full home-ev-img"
                                                      alt="a"
                                                      sizes="(max-width: 800px) 100vw, 800px"
                                                      />
                                                    </div>

                                                    <div class="eltdf-invitation-text-holder">
                                                      <div class="eltdf-invitation-text-holder-inner">
                                                        <span> <img width="37" style="width: 37px !important; margin:auto;" height="27" src="<?php echo bloginfo('template_url') ?>/images/04-h1-custom-icon-img-1.png" class="attachment-full size-full" alt="a" /></span>

                                                        <h1 class="eltdf-invitation-title" style="line-height:70px; ">
                                                          <a href="<?php echo the_permalink(145); ?>"><?php the_title(); ?></a>
                                                        </h1>
                                                        <p class="eltdf-invitation-date">
                                                          <?php echo $events[0]['date'] ?>
                                                          <span id="current_timer<?php echo $key; ?>"></span>
                                                        </p>
                                                        <p class="eltdf-invitation-text">
                                                          <?php echo $events[0]['description'] ?>
                                                        </p>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>

                                              <?php } ?>

                                            </div>
                                          </div>    
                                        </div>
                                      </div>
                                    </div>
                                  </section>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="eltdf-row-grid-section-wrapper eltdf-parallax-row-holder"  data-parallax-bg-image="<?php bloginfo('template_url'); ?>/images/h1-parallax-1.jpg" data-parallax-bg-speed="1">
                          <div class="eltdf-row-grid-section">
                            <div class="vc_row wpb_row vc_row-fluid vc_custom_1526457648096">
                              <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner">
                                  <div class="wpb_wrapper">
                                    <div class="eltdf-countdown eltdf-light-skin" id="eventcountdown12321" data-year="<?php echo $EFullDate[0]; ?>" data-month="<?php echo $EFullDate[1]; ?>" data-day="<?php echo $EFullDate[2]; ?>" data-hour="12" data-minute="0" data-month-label="Months" data-day-label="Days" data-hour-label="Hours" data-minute-label="Minutes" data-second-label="Seconds"></div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <section class="our-services-home">
                          <div class="vc_row wpb_row vc_row-fluid vc_custom_1527595239564">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                              <div classs="vc_column-inner">
                                <h3>Our Service</h3>
                                <div class="wpb_wrapper">
                                  <div class="eltdf-clients-carousel-holder eltdf-cc-hover-zoom-image">
                                    <div class="eltdf-cc-inner eltdf-owl-slider" data-number-of-items="5" data-enable-loop="yes" data-enable-autoplay="yes" data-slider-speed="5000" data-slider-speed-animation="600" data-enable-navigation="no" data-enable-pagination="no">

                                      <?php

                                      $arraydata = array(
                                        'post_type' => 'our service' ,
                                        'post_status' => 'publish' ,
                        // 'posts_per_pages' => 3 ,
                                        'order' => 'ASC' ,
                                        'orderby' => 'data',

                                      );

                                      $post_data = new WP_Query($arraydata);

                                      while( $post_data->have_posts() ) 
                                      {
                                        $post_data->the_post();

                                        $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()),'full');
                                        ?>

                                        <div class="eltdf-cc-item eltdf-item-space eltdf-cci-has-link">
                                          <div class="col-xs-12 col-md-12 col-lg-12 col-sm-12" style="display: table;">
                                            <div class="ServSingle" style="background: linear-gradient(360deg, rgba(0,0,0,0.7) 10%,rgba(0,0,0,0.0)25%), url('<?php echo $image[0] ?>') center; background-size: cover;">
                                              <h4><?php the_title(); ?></h4>
                                            </div>
                                          </div>
                                        </div>

                                      <?php } ?>

                                    </div>
                                    <div class="clearfix"></div>
                                    <button class="btn btn-info btnViewMore2"><a href="<?php echo get_site_url(); ?>/our-services/">View More</a></button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </section>

                        <section class="our-event">

                          <div class="tp-mask-wrap" style="position: absolute; right: 0px; display: block; overflow: visible;"><div class="tp-caption tp-resizeme tp-static-layer" id="slider-11-layer-13" data-x="['right','right','right','right']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['0','0','0','0']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="image" data-basealign="slide" data-responsive_offset="on" data-startslide="0" data-endslide="2" data-frames="[{&quot;delay&quot;:500,&quot;speed&quot;:1000,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;opacity:0;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]" data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; visibility: inherit; transition: none 0s ease 0s; text-align: inherit; line-height: 0px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 0px; font-weight: 400; font-size: 10px; white-space: nowrap; min-height: 0px; min-width: 0px; max-height: none; max-width: none; opacity: 1; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform-origin: 50% 50% 0px;">
                            <img src="http://localhost/events/wp-content/themes/events/images/05-h1-slider-1-img-2.png" alt="a" data-ww="['805px','540px','420px','300px']" data-hh="['322px','216px','168px','120px']" width="805" height="322" data-no-retina="" style="width: 487.293px; height: 194.917px; transition: none 0s ease 0s; text-align: inherit; line-height: 0px; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 0px; font-weight: 400; font-size: 9px;">
                          </div></div>

                          <div class="container-fluid"> 
                            <div class="container">
                              <div class="row">
                                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12"> 
                                  <h3>Recent Events</h3>

                                  <?php

                                  $arraydata = array(
                                    'post_type' => 'events' ,
                                    'post_status' => 'publish' ,
                                    'posts_per_pages' => 3 ,
                                    'order' => 'DESC' ,
                                    'orderby' => 'data',

                                  );

                                  $post_data = new WP_Query($arraydata);

                                  while( $post_data->have_posts() ) 
                                  {
                                    $post_data->the_post();

                                    $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()),'full');
                                    ?>

                                    <div class="col-md-4">
                                      <div class="event-cover-img" style="background: linear-gradient(rgba(0,0,0,0.3),rgba(0,0,0,0.3)), url('<?php echo $image[0] ?>') center; background-size: cover;">
                                        <h4><a href="<?php echo get_site_url(); ?>/events/#<?php the_title(); ?>"><?php the_title(); ?></a></h4>
                                      </div>
                                    </div>

                                  <?php } ?>
                                </div>
                                <div class="clearfix"></div>
                                <button class="btn btn-info btnViewMore"><a href="<?php echo get_site_url(); ?>/events">View More</a></button>
                              </div>
                            </div>
                          </div>
                        </section>

                        <section class="recent-gallery">
                          <div class="container-fluid">
                            <h3>Recent Gallery</h3>

                            <!-- slide 1 -->

                            <div class="eltdf-image-gallery eltdf-ig-carousel-type eltdf-three-columns eltdf-small-space eltdf-image-behavior-zoom eltdf-ig-carousel-with-auto-width">
                              <div class="eltdf-ig-slider eltdf-owl-slider" data-number-of-items="2" data-enable-loop="yes"  data-enable-autoplay="no" data-slider-speed="5000" data-slider-speed-animation="600" data-slider-padding="no" data-enable-navigation="no" data-enable-pagination="no" >

                                <?php

                                $arraydata = array(
                                  'post_type' => 'gallery' ,
                                  'post_status' => 'publish' ,
                                  'posts_per_pages' => 5 ,
                                  'order' => 'DESC' ,
                                  'orderby' => 'data',

                                );

                                $post_data = new WP_Query($arraydata);

                                while( $post_data->have_posts() ) 
                                {
                                  $post_data->the_post();

                                  $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()),'full');
                                  ?>

                                  <div data-enable-auto-width="yes" style="height: 500px">
                                    <img src="<?php echo $image[0] ?>" class="img-responsive" alt="<?php the_title(); ?>" width="100%" height="500px" style="object-fit: cover; height: 500px !important"/>
                                  </div>

                                <?php } ?>

                              </div>
                              <div class="clearfix"></div>
                              <button class="btn btn-info btnViewMore2"><a href="<?php echo get_site_url(); ?>/gallery">View More</a></button>
                            </div>

                            <!-- slide 2 -->

      <!-- <div class="m-t-2 eltdf-image-gallery eltdf-ig-carousel-type eltdf-three-columns eltdf-small-space eltdf-image-behavior-zoom eltdf-ig-carousel-with-auto-width">
        <div class="eltdf-ig-slider eltdf-owl-slider" data-number-of-items="4" data-enable-loop="yes" data-enable-auto-width="yes" data-enable-autoplay="no" data-slider-speed="5000" data-slider-speed-animation="600" data-slider-padding="no" data-enable-navigation="no" data-enable-pagination="no" >
            <div class="eltdf-ig-image">
                <img width="673" height="520" src="<?php bloginfo('template_url'); ?>/images/05-h1-img-2.jpg" class="attachment-full size-full" alt="a" sizes="(max-width: 673px) 100vw, 673px"/>
            </div>

            <div class="eltdf-ig-image">
                <img width="347" height="520" src="<?php bloginfo('template_url'); ?>/images/05-h1-img-3.jpg" class="attachment-full size-full" alt="a" sizes="(max-width: 347px) 100vw, 347px" />
            </div>

            <div class="eltdf-ig-image">
                <img width="347" height="520" src="<?php bloginfo('template_url'); ?>/images/05-h1-img-4.jpg" class="attachment-full size-full" alt="a" sizes="(max-width: 347px) 100vw, 347px" />
            </div>

            <div class="eltdf-ig-image">
                <img width="505" height="520" src="<?php bloginfo('template_url'); ?>/images/05-h1-img-5.jpg" class="attachment-full size-full" alt="a" sizes="(max-width: 505px) 100vw, 505px" />
            </div>
        </div>
      </div> -->

    </div>
  </section>

<!-- 
<div class="vc_row wpb_row our-service vc_row-fluid vc_custom_1525691547730">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner">
            <div class="wpb_wrapper">
                <div class="eltdf-image-gallery eltdf-ig-carousel-type eltdf-three-columns eltdf-small-space eltdf-image-behavior-zoom eltdf-ig-carousel-with-auto-width">
                    <div class="eltdf-ig-slider eltdf-owl-slider" data-number-of-items="4" data-enable-loop="yes" data-enable-auto-width="yes" data-enable-autoplay="no" data-slider-speed="5000" data-slider-speed-animation="600" data-slider-padding="no" data-enable-navigation="no" data-enable-pagination="no" >
                        <div class="eltdf-ig-image">
                            <img width="673" height="520" src="<?php bloginfo('template_url'); ?>/images/05-h1-img-2.jpg" class="attachment-full size-full" alt="a" sizes="(max-width: 673px) 100vw, 673px"/>
                        </div>

                        <div class="eltdf-ig-image">
                            <img width="347" height="520" src="<?php bloginfo('template_url'); ?>/images/05-h1-img-3.jpg" class="attachment-full size-full" alt="a" sizes="(max-width: 347px) 100vw, 347px" />
                        </div>

                        <div class="eltdf-ig-image">
                            <img width="347" height="520" src="<?php bloginfo('template_url'); ?>/images/05-h1-img-4.jpg" class="attachment-full size-full" alt="a" sizes="(max-width: 347px) 100vw, 347px" />
                        </div>

                        <div class="eltdf-ig-image">
                            <img width="505" height="520" src="<?php bloginfo('template_url'); ?>/images/05-h1-img-5.jpg" class="attachment-full size-full" alt="a" sizes="(max-width: 505px) 100vw, 505px" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="vc_row wpb_row vc_row-fluid vc_custom_1525691547730">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner">
            <div class="wpb_wrapper">
                <div class="eltdf-image-gallery eltdf-ig-carousel-type eltdf-three-columns eltdf-small-space eltdf-image-behavior-zoom eltdf-ig-carousel-with-auto-width">
                    <div class="eltdf-ig-slider eltdf-owl-slider" data-number-of-items="4" data-enable-loop="yes" data-enable-auto-width="yes" data-enable-autoplay="no" data-slider-speed="5000" data-slider-speed-animation="600" data-slider-padding="no" data-enable-navigation="no" data-enable-pagination="no">
                        <div class="eltdf-ig-image">
                            <img width="506" height="520" src="<?php bloginfo('template_url'); ?>/images/05-h1-img-6.jpg" class="attachment-full size-full" alt="a" sizes="(max-width: 506px) 100vw, 506px" />
                        </div>

                        <div class="eltdf-ig-image">
                            <img width="346" height="520" src="<?php bloginfo('template_url'); ?>/images/05-h1-img-7.jpg" class="attachment-full size-full" alt="a" sizes="(max-width: 346px) 100vw, 346px" />
                        </div>

                        <div class="eltdf-ig-image">
                            <img width="777" height="520" src="<?php bloginfo('template_url'); ?>/images/05-h1-img-8.jpg" class="attachment-full size-full" alt="a" sizes="(max-width: 777px) 100vw, 777px"/>
                        </div>

                        <div class="eltdf-ig-image">
                            <img width="348" height="520" src="<?php bloginfo('template_url'); ?>/images/05-h1-img-9.jpg" class="attachment-full size-full" alt="a" sizes="(max-width: 348px) 100vw, 348px"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div> -->

  <section class="testimonial">
    <div class="container-fluid">
      <div class="container">
        <div class="row">
          <h3>Testimonials</h3>
        </div>
      </div>
    </div>
    <div class="testimonials">
      <div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/testimonials.jpg" data-speed="0.8"></div>
      <div class="testimonials_overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col">
            <div class="testimonials_slider_container">

              <!-- Testimonials Slider -->
              <div class="owl-carousel testimonial-slide owl-theme test_slider">


                <?php

                $arraydata = array(
                  'post_type' => 'testimonial' ,
                  'post_status' => 'publish' ,
                  'posts_per_pages' => 3 ,
                  'order' => 'ASC' ,
                  'orderby' => 'data',
                );

                $post_data = new WP_Query($arraydata);

                while( $post_data->have_posts() ) 
                {
                  $post_data->the_post();

                  $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()),'full');
                  ?>

                  <!-- Slide -->
                  <div  class="test_slider_item text-center">
                    <div class="rating rating_5 d-flex flex-row align-items-start justify-content-center">

                      <?php
                      $rating = get_field("rating",get_the_id()); 
                      for ($i=0; $i <$rating ; print("<i></i>"), $i++); 
                        ?>

                    </div>
                    <div class="testimonial_title"><?php the_title(); ?></div>
                    <div class="testimonial_text">
                      <?php 
                      $subCont = get_the_content();
                      echo strip_tags(substr($subCont,0,200)).".....";
                    // echo "<p>".$subCont."</p>";
                      ?>
                    </div>
                    <div class="testimonial_image"><img src="<?php echo $image[0] ?>" alt="<?php the_title(); ?>" class="img-responsive img-fluid"></div>
                    <div class="testimonial_author"><a href="#"><?php echo get_field("author_name",get_the_id()) ?></a></div>
                  </div>

                <?php } ?>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>







<!-- 
<div class="eltdf-row-grid-section-wrapper eltdf-content-aligment-center">
    <div class="eltdf-row-grid-section">
        <div class="vc_row wpb_row vc_row-fluid vc_custom_1526459390824">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="eltdf-elements-holder eltdf-two-columns eltdf-responsive-mode-1024">
                            <div class="eltdf-eh-item" data-item-class="eltdf-eh-custom-8074" data-768-1024="0 75px" data-680-768="0 50px" data-680="0 0">
                                <div class="eltdf-eh-item-inner">
                                    <div class="eltdf-eh-item-content eltdf-eh-custom-8074">
                                        <div class="eltdf-section-title-holder eltdf-st-standard" style="padding: 0 4%;">
                                            <div class="eltdf-st-inner">
                                                <h1 class="eltdf-st-title">
                                                    Now &amp; Forever
                                                </h1>
                                                <h6 class="eltdf-st-separator-subtitle">happiness</h6>
                                                <p class="eltdf-st-text" style="margin-top: 13px;">
                                                    Your special day deserves a special organization. With years of experience in wedding planning, our team will make your big day unforgettable. Say
                                                    YES to perfection!
                                                </p>
                                            </div>
                                        </div>
                                        <div class="vc_empty_space" style="height: 26px;"><span class="vc_empty_space_inner"></span></div>
                                        <a itemprop="url" href="our-story.html" target="_self" class="eltdf-btn eltdf-btn-medium eltdf-btn-solid">
                                            <span class="eltdf-btn-text">submit</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="eltdf-eh-item" data-item-class="eltdf-eh-custom-5512">
                                <div class="eltdf-eh-item-inner">
                                    <div class="eltdf-eh-item-content eltdf-eh-custom-5512">
                                        <div class="wpb_single_image wpb_content_element vc_align_center">
                                            <figure class="wpb_wrapper vc_figure">
                                                <div class="vc_single_image-wrapper vc_box_border_grey">
                                                    <img width="960" height="642" src="<?php bloginfo('template_url'); ?>/images/04-h1-img-10.jpg" class="vc_single_image-img attachment-full" alt="a" sizes="(max-width: 960px) 100vw, 960px" />
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="eltdf-row-grid-section-wrapper eltdf-parallax-row-holder" data-parallax-bg-image="<?php bloginfo('template_url'); ?>/images/h1-parallax-1.jpg" data-parallax-bg-speed="1"> 
   <div class="eltdf-row-grid-section">
    <div class="vc_row wpb_row vc_row-fluid vc_custom_1526457648096">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="eltdf-countdown eltdf-light-skin" id="countdown5585" data-year="2021" data-month="10" data-day="15" data-hour="12" data-minute="0" data-month-label="Months" data-day-label="Days" data-hour-label="Hours" data-minute-label="Minutes" data-second-label="Seconds"
                    ></div>
                </div>
            </div>
        </div>
    </div>
</div>
</div> -->
<!-- <div class="eltdf-row-grid-section-wrapper eltdf-content-aligment-center">
    <div class="eltdf-row-grid-section">
        <div class="vc_row wpb_row vc_row-fluid vc_custom_1527586692634">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="eltdf-section-title-holder eltdf-st-standard"> 
                            <div class="eltdf-st-inner">
                                <h1 class="eltdf-st-title">
                                    Event &amp; Guests
                                </h1>
                                <h6 class="eltdf-st-separator-subtitle">weddings</h6>
                            </div>
                        </div>
                        <div class="vc_empty_space" style="height: 53px;"><span class="vc_empty_space_inner"></span></div>
                        <div class="vc_row wpb_row vc_inner vc_row-fluid row-with-map-links eltdf-content-aligment-center">
                            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-offset-0 vc_col-lg-4 vc_col-md-offset-0 vc_col-md-4 vc_col-sm-offset-0 vc_col-xs-12">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="eltdf-image-with-text-holder">
                                            <div class="eltdf-iwt-image">
                                                <img width="800" height="1195" src="<?php bloginfo('template_url'); ?>/images/05-h1-img-11.jpg" class="attachment-full size-full" alt="a" sizes="(max-width: 800px) 100vw, 800px" />
                                            </div>

                                            <div class="eltdf-iwt-text-holder">
                                                <h4 class="eltdf-iwt-title" style="margin-top: 39px;">Church St Marco</h4>
                                                <p class="eltdf-iwt-text">Royal Villa Macadamiana second at Marianberg, Hilltop Dr Menomonee Falls, WI 53051 US</p>
                                            </div>
                                        </div>
                                        <div class="vc_empty_space" style="height: 17px;"><span class="vc_empty_space_inner"></span></div>
                                        <a
                                        itemprop="url"
                                        href="https://www.google.rs/maps/place/Hilltop+Dr,+Menomonee+Falls,+WI+53051,+USA/@43.1623818,-88.1004267,17z/data=!3m1!4b1!4m5!3m4!1s0x8804fe033b89cdb9:0x7e975b12876d2d38!8m2!3d43.1623779!4d-88.098238"
                                        target="_blank"
                                        style="color: #160808; font-size: 13px; font-weight: 500;"
                                        class="eltdf-btn eltdf-btn-medium eltdf-btn-condensed"
                                        >
                                        <span class="eltdf-btn-text">LOCATION</span>
                                    </a>
                                    <div class="vc_empty_space" style="height: 66px;"><span class="vc_empty_space_inner"></span></div>
                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-offset-0 vc_col-lg-4 vc_col-md-offset-0 vc_col-md-4 vc_col-sm-offset-0 vc_col-xs-12">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div class="eltdf-image-with-text-holder">
                                        <div class="eltdf-iwt-image">
                                            <img width="800" height="1195" src="<?php bloginfo('template_url'); ?>/images/05-h1-img-12.jpg" class="attachment-full size-full" alt="a" sizes="(max-width: 800px) 100vw, 800px" />
                                        </div>

                                        <div class="eltdf-iwt-text-holder">
                                            <h4 class="eltdf-iwt-title" style="margin-top: 39px;">Arrival &amp; Check In</h4>
                                            <p class="eltdf-iwt-text">Royal Villa Macadamiana second at Marianberg, Hilltop Dr Menomonee Falls, WI 53051 US</p>
                                        </div>
                                    </div>
                                    <div class="vc_empty_space" style="height: 17px;"><span class="vc_empty_space_inner"></span></div>
                                    <a itemprop="url" href="https://www.google.rs/maps/place/Hilltop+Dr,+Menomonee+Falls,+WI+53051,+USA/@43.1623818,-88.1004267,17z/data=!3m1!4b1!4m5!3m4!1s0x8804fe033b89cdb9:0x7e975b12876d2d38!8m2!3d43.1623779!4d-88.098238" target="_blank" style="color: #160808; font-size: 13px; font-weight: 500;" class="eltdf-btn eltdf-btn-medium eltdf-btn-condensed" >
                                        <span class="eltdf-btn-text">LOCATION</span>
                                    </a>
                                    <div class="vc_empty_space" style="height: 66px;"><span class="vc_empty_space_inner"></span></div>
                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-offset-0 vc_col-lg-4 vc_col-md-offset-0 vc_col-md-4 vc_col-sm-offset-0 vc_col-xs-12">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div class="eltdf-image-with-text-holder">
                                        <div class="eltdf-iwt-image">
                                            <img width="800" height="1195" src="<?php bloginfo('template_url'); ?>/images/05-h1-img-13.jpg" class="attachment-full size-full" alt="a" sizes="(max-width: 800px) 100vw, 800px"/>
                                        </div>

                                        <div class="eltdf-iwt-text-holder">
                                            <h4 class="eltdf-iwt-title" style="margin-top: 39px;">Lovely Dinner</h4>
                                            <p class="eltdf-iwt-text">Royal Villa Macadamiana second at Marianberg, Hilltop Dr Menomonee Falls, WI 53051 US</p>
                                        </div>
                                    </div>
                                    <div class="vc_empty_space" style="height: 17px;"><span class="vc_empty_space_inner"></span></div>
                                    <a itemprop="url" href="https://www.google.rs/maps/place/Hilltop+Dr,+Menomonee+Falls,+WI+53051,+USA/@43.1623818,-88.1004267,17z/data=!3m1!4b1!4m5!3m4!1s0x8804fe033b89cdb9:0x7e975b12876d2d38!8m2!3d43.1623779!4d-88.098238" target="_blank" style="color: #160808; font-size: 13px; font-weight: 500;" class="eltdf-btn eltdf-btn-medium eltdf-btn-condensed">
                                        <span class="eltdf-btn-text">LOCATION</span>
                                    </a>
                                    <div class="vc_empty_space" style="height: 66px;"><span class="vc_empty_space_inner"></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div> -->

<section class="home-video">
  <div class="container-fluid single-video">

<!--       <iframe src="https://www.youtube.com/embed/<?php echo $homeVideo ?>?autoplay=1" allowfullscreen
  sandbox="allow-scripts allow-presentation allow-same-origin"
  allow="autoplay; fullscreen; picture-in-picture; xr-spatial-tracking; encrypted-media"></iframe> -->

</div>
</section>


<section class="home-blog-secetion">
  <div class="container-fluid single-video">
    <div class="container">
      <div class="row">
        <h3>Recent Blogs</h3>
        <div class="col-sm-12 col-xs-12 col-lg-12 col-md-12">

          <?php

          $arraydata = array(
            'post_type' => 'post' ,
            'post_status' => 'publish' ,
            'posts_per_pages' => 3 ,
            'order' => 'DESC' ,
            'orderby' => 'data',
          );

          $post_data = new WP_Query($arraydata);

          while( $post_data->have_posts() ) 
          {
            $post_data->the_post();
            $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()),'full');

            ?>

            <div class="col-sm-12 col-xs-12 col-lg-4 col-md-4">
              <div class="home-blog-img">
                <a href="<?php the_permalink(); ?>"><img src="<?php echo $image[0] ?>" class="img-responsive" alt=""></a>
              </div>
              <div class="home-blog-content">
                <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                <span><a href="<?php the_permalink(); ?>">Dancing</a></span>
                <a href="<?php the_permalink(); ?>"><p><?php the_excerpt(); ?></p></a>
                <a href="<?php the_permalink(); ?>"><i><?php echo get_the_date(); ?></i></a>
              </div>
            </div>
          <?php } ?>

        </div>
      </div>
    </div>
  </div>
</section>

<section class="home-enquiry-section" id="footer">
  <div class="container-fluid">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-xs-12 col-sm-12 form-section">
          <?php if($_SESSION['MSG']==1) { ?>
            <p class="text-success">Your enquiry has been successfully sent.</p>
            <?php 
            unset($_SESSION['MSG']);
          } ?>
          <form method="post" >
            <div class="col-lg-12 col-xs-12 col-sm-6">
              <div class="form-group">
                <label for="name">Name</label>
                <input type="text" name="uname" class="form-control" id="name" placeholder="Name" required>
              </div>

              <div class="form-group">
                <label for="number">Phone Number</label>
                <input type="number" name="number" class="form-control" id="number" placeholder="Phone Number" required>
              </div>

              <div class="form-group">
                <label for="packages">Packages</label>
                <select id="packages" name="packages" required class="form-control">
                  <option>Select package</option>

                  <?php

                  $arraydata = array(
                    'post_type' => 'our_packages' ,
                    'post_status' => 'publish' ,
                    'order' => 'DESC' ,
                    'orderby' => 'data',

                  );

                  $post_data = new WP_Query($arraydata);
                  $i =1;
                  while( $post_data->have_posts() ) 
                  {
                    $post_data->the_post();

                    ?>

                    <option value="<?php the_title(); ?>"><?php the_title(); ?></option>

                    <?php 
                  }
                  ?>

                </select>
                <span><a href="our-packages/">View our packages</a></span>
              </div>

              <div class="form-group">
                <label for="msg">Message</label>
                <textarea id="msg" name="msg" class="form-control" required></textarea>
              </div>
              <div>
                <button class="btn pull-right home-form-btn" type="submit" name="send" value="send">Send</button>
              </div>
            </div>
          </form>
        </div>
        <div class="col-md-6 img-section">

        </div>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>

<script>
  var owl = $('.testimonial-slide');
  owl.owlCarousel({
    loop: true,
    margin: 10,
    autoplay: true,
    autoplayTimeout: 2000,
    autoplayHoverPause: true,
    margin: 10,
    loop: true,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 2
      },
      1000: {
        items: 3
      }
    }
  })
</script>
<script>
  var owl = $('.location-slide');
  owl.owlCarousel({
    loop: true,
    margin: 10,
    autoplay: true,
    autoplayTimeout: 2000,
    autoplayHoverPause: true,
    margin: 10,
    loop: true,
    nav: true,
    dots:true,
    responsive: {
      0: {
        items: 3
      },
      600: {
        items: 3
      },
      1000: {
        items: 7
      }
    }
  })
</script>
<script>
  var owl = $('.eve-slide');
  owl.owlCarousel({
    loop: true,
    margin: 10,
    autoplay: true,
    autoplayTimeout: 1500,
    autoplayHoverPause: true,
    margin: 10,
    loop: true,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 1
      }
    }
  })
</script>

<!-- <script type="text/javascript">
  const second = 1000,
      minute = second * 60,
      hour = minute * 60,
      day = hour * 24;
      let countDown = new Date('Sep 30, 2020 00:00:00').getTime(),
      x = setInterval(function() {    

      let now = new Date().getTime(), 
      distance = countDown - now;

      document.getElementById('days').innerText = Math.floor(distance / (day)),
        document.getElementById('hours').innerText = Math.floor((distance % (day)) / (hour)),
        document.getElementById('minutes').innerText = Math.floor((distance % (hour)) / (minute)),
        document.getElementById('seconds').innerText = Math.floor((distance % (minute)) / second);
    }, second)
  </script> -->

<!-- <script>
  $(function(){
    $('#current_timer{{ $key }}').countdowntimer({
      dateAndTime : "{{ date('Y/m/d H:i:s',strtotime($offer->offer_valid_to)) }}",
      size : "lg",
      regexpMatchFormat: "([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})",
      regexpReplaceWith: "$1 <b>Days</b> : $2 <b> Hrs.</b> : $3 <b> Min.</b> : $4 <b> Sec.</b>",
      // timeUp : timeisUp
    });

    // function timeisUp(){
    //   ChangeStatusOffer('{{$offer->offer_id }}');
    //   $('#current_timer{{ $key }}').html('<span><strong>This offer is expired..</strong></span>');
    //   $('#current_click{{ $key }}').removeAttr("href");
    //   $('#offerBlock{{ $key }}').hide();
    // }
  });
</script> -->