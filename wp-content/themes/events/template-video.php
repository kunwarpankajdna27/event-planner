<?php

// Template Name:Video Gallery

get_header('inner');

?>

<a id="eltdf-back-to-top" href="#">
    <span class="eltdf-text-stack"> top </span>
</a>

<section class="inner-banner">
	<div class="container-fluid">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="videos-block">
	<div class="container-fluid">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
				
				<?php

                  $arraydata = array(
                    'post_type' => 'video gallery' ,
                    'post_status' => 'publish',
                    'order' => 'ASC' ,
                    'orderby' => 'data',

                  );

                  $post_data = new WP_Query($arraydata);

                  while( $post_data->have_posts() ) {
                    $post_data->the_post();
                ?>


					<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 singleVideo">
						<iframe width="100%" controlslist="nodownload" style="height: 300px !important" height="300px" src="https://www.youtube.com/embed/<?php echo get_field('video_url',get_the_id()); ?>" ></iframe>
					</div>

				<?php } ?>

				</div>
			</div>
		</div>
	</div>
</section>

<?php

get_footer();

?>