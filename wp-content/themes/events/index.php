<?php

get_header('inner');

?>

<a id="eltdf-back-to-top" href="#">
    <span class="eltdf-text-stack"> top </span>
</a>

<section class="inner-banner">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                    <h1>Blog</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="blog-block">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                 <div class="eltdf-row-grid-section-wrapper">
                    <div class="eltdf-row-grid-section">
                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1525862262422">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="eltdf-blog-list-holder eltdf-grid-list eltdf-disable-bottom-space eltdf-bl-standard eltdf-three-columns eltdf-normal-space eltdf-bl-pag-no-pagination" data-type="standard" data-number-of-posts="3" data-number-of-columns="three" data-space-between-items="normal" data-category="dancing" data-orderby="date" data-order="ASC" data-image-size="full" data-title-tag="h4" data-excerpt-length="18" data-post-info-section="yes" data-post-info-image="yes" data-post-info-author="no" data-post-info-date="yes" data-post-info-category="yes" data-post-info-comments="no" data-post-info-like="no" data-post-info-share="no" data-pagination-type="no-pagination" data-max-num-pages="2" data-next-page="2">
                                            <div class="eltdf-bl-wrapper eltdf-outer-space">
                                                <ul class="eltdf-blog-list">
                                                        
                                                    <?php
                                
                                                       while(have_posts())
                                                          {
                                                            the_post();
                                                            $image = wp_get_attachment_image_src(get_post_thumbnail_id( get_the_id() ), 'full' );

                                                    ?>


                                                    <li class="eltdf-bl-item eltdf-item-space">
                                                        <div class="eltdf-bli-inner">
                                                            <div class="eltdf-post-image">
                                                                <a itemprop="url" href="<?php the_permalink(); ?>" title="Wedding in Nature">
                                                                    <img src="<?php echo $image[0] ?>" class="attachment-full size-full wp-post-image" alt="<?php the_title(); ?>"/>
                                                                </a>
                                                            </div>
                                                            <div class="eltdf-bli-content">
                                                                <h4 itemprop="name" class="entry-title eltdf-post-title">
                                                                    <a itemprop="url" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
                                                                </h4>
                                                                <div class="eltdf-post-info-category">
                                                                    <a href="" rel="category tag">Dancing</a>
                                                                </div>
                                                                <div class="eltdf-bli-excerpt">
                                                                    <div class="eltdf-post-excerpt-holder">
                                                                        <p itemprop="description" class="eltdf-post-excerpt">
                                                                            <?php the_excerpt(); ?>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                                <div class="eltdf-bli-info">
                                                                    <div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
                                                                        <a itemprop="url" href="<?php the_permalink(); ?>"><?php echo get_the_date(); ?></a>
                                                                        <meta itemprop="interactionCount" content="UserComments: 0" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>

                                                    <?php } ?>

                                                </ul>
                                                <div class="eltdf-blog-pagination">
                                                    <?php wp_pagenavi(); ?>
                                                </div>
                                                <div class="eltdf-blog-pagination-wp"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>               

<?php

get_footer();

?>