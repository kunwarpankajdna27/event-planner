<?php
global $current_user;
 wp_get_current_user();
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=yes" />
    <title><?php the_title(); ?></title>

    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/bootstrap-theme.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/font-awesome.min.css">
    <link href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/owl.theme.default.min.css">
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.js"></script>

    <link rel="stylesheet" id="js_composer_front-css" href="<?php bloginfo('template_url'); ?>/css/css-js_composer.min.css" type="text/css" media="all" />
    <link rel="stylesheet" id="wp-block-library-css" href="<?php bloginfo('template_url'); ?>/css/block-library-style.min.css" type="text/css" media="all" />
    <link rel="stylesheet" id="contact-form-7-css" href="<?php bloginfo('template_url'); ?>/css/css-styles.css" type="text/css" media="all" />
    <link rel="stylesheet" id="rs-plugin-settings-css" href="<?php bloginfo('template_url'); ?>/css/css-settings.css" type="text/css" media="all" />

    <link rel="stylesheet" id="gretnagreen-elated-default-style-css" href="<?php bloginfo('template_url'); ?>/css/gretnagreen-style.css" type="text/css" media="all" />
    <link rel="stylesheet" id="gretnagreen-elated-modules-css" href="<?php bloginfo('template_url'); ?>/css/css-modules.min.css" type="text/css" media="all" />
    <link rel="stylesheet" id="eltdf-dripicons-css" href="<?php bloginfo('template_url'); ?>/css/dripicons-dripicons.css" type="text/css" media="all" />
    <link rel="stylesheet" id="eltdf-font_elegant-css" href="<?php bloginfo('template_url'); ?>/css/elegant-icons-style.min.css" type="text/css" media="all" />
    <link rel="stylesheet" id="eltdf-font_awesome-css" href="<?php bloginfo('template_url'); ?>/css/css-fontawesome-all.min.css" type="text/css" media="all" />
    <link rel="stylesheet" id="eltdf-ion_icons-css" href="<?php bloginfo('template_url'); ?>/css/css-ionicons.min.css" type="text/css" media="all" />
    <link rel="stylesheet" id="eltdf-linea_icons-css" href="<?php bloginfo('template_url'); ?>/css/linea-icons-style.css" type="text/css" media="all" />
    <link rel="stylesheet" id="eltdf-linear_icons-css" href="<?php bloginfo('template_url'); ?>/css/linear-icons-style.css" type="text/css" media="all" />
    <link rel="stylesheet" id="eltdf-simple_line_icons-css" href="<?php bloginfo('template_url'); ?>/css/simple-line-icons-simple-line-icons.css" type="text/css" media="all" />
    <link rel="stylesheet" id="mediaelement-css" href="<?php bloginfo('template_url'); ?>/css/mediaelement-mediaelementplayer-legacy.min.css" type="text/css" media="all" />
    <link rel="stylesheet" id="wp-mediaelement-css" href="<?php bloginfo('template_url'); ?>/css/mediaelement-wp-mediaelement.min.css" type="text/css" media="all" />
    <link rel="stylesheet" id="gretnagreen-elated-woo-css" href="<?php bloginfo('template_url'); ?>/css/css-woocommerce.min.css" type="text/css" media="all" />

    <link rel="stylesheet" id="gretnagreen-elated-woo-responsive-css" href="<?php bloginfo('template_url'); ?>/css/css-woocommerce-responsive.min.css" type="text/css" media="all" />
    <link rel="stylesheet" id="gretnagreen-elated-style-dynamic-css" href="<?php bloginfo('template_url'); ?>/css/css-style_dynamic.css" type="text/css" media="all" />
    <link rel="stylesheet" id="gretnagreen-elated-modules-responsive-css" href="<?php bloginfo('template_url'); ?>/css/css-modules-responsive.min.css" type="text/css" media="all" />
    <link rel="stylesheet" id="gretnagreen-elated-style-dynamic-responsive-css" href="<?php bloginfo('template_url'); ?>/css/css-style_dynamic_responsive.css" type="text/css" media="all" />

    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style.css" type="text/css" media="all" />
        <script src="<?php bloginfo('template_url'); ?>/js/minify-eff97.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/minify-f1253.js"></script>
        
        <script src="<?php bloginfo('template_url'); ?>/js/minify-864c2.js"></script>
        
        <noscript>
            <style>
            .woocommerce-product-gallery {
                opacity: 1 !important;
            }
        </style>
    </noscript>
    <link rel="icon" href="favicons/05-favicon.png" sizes="32x32" />
    <link rel="icon" href="favicons/05-favicon.png" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="favicons/05-favicon.png" />

    <script src="<?php bloginfo('template_url'); ?>/js/custom.js"></script>
    <noscript>
        <style>
        .wpb_animate_when_almost_visible {
            opacity: 1;
        }
    </style>
</noscript>
</head>
<body>
    <div class="eltdf-wrapper">
        <div class="eltdf-wrapper-inner">
            <header class="eltdf-page-header" id="navbar">
                <div class="eltdf-menu-area eltdf-menu-center">
                    <div class="eltdf-vertical-align-containers">
                        <div class="eltdf-position-left">
                            <div class="eltdf-position-left-inner">

                                <?php
                                    $menu = wp_get_nav_menu_object('header');
                                    $logo = get_field('inner_logo', $menu);
                                ?>

                                <a itemprop="url" href="<?php echo get_site_url(); ?>" style="height: 27px;">
                                    <img itemprop="image" class="eltdf-normal-logo" src="<?php echo $logo; ?>" style="height: 80px !important" width="200" alt="logo" />
                                </a>
                            </div>
                        </div>
                        
                        <div class="eltdf-position-right">
                            <div class="eltdf-position-right-inner">
                                <div class="eltdf-shopping-cart-holder" style="margin: 0 25px 0 0;">
                                    <div class="eltdf-shopping-cart-inner">
                                        <nav class="eltdf-main-menu eltdf-drop-down eltdf-default-nav">
                                            <?php 
                                            wp_nav_menu(array(
                                                'theme_location' => 'header',
                                                'menu_class'    => '',
                                                'container'     => '',
                                                'container_class'=>'',
                                                'container_id' => ''
                                            ))
                                            ?>
                                        </nav>
                                         
                                    </div>

                                </div>
                                <div class="login-user-inner"> 
                                             <?php if ( is_user_logged_in() ) { ?>  
                                             <a href="<?php echo the_permalink(145); ?>">    <?php echo  $current_user->user_login; ?> </a>
                                             <ul class="user-sub-menu">
                                            <li>
                                                <a href="<?php echo the_permalink(145); ?>">User Eevnts</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo the_permalink(196); ?>">User Gallery</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo wp_logout_url(get_permalink(170))?>"> Log Out</a>
                                            </li>
                                        </ul>
                            <?php } else { ?>
                                             <a href="<?php echo the_permalink(170); ?>"> Login
                                             </a>
                                        <?php } ?>
                                        </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="eltdf-sticky-header">
                    <div class="eltdf-sticky-holder eltdf-menu-center">
                        <div class="eltdf-vertical-align-containers">
                            <div class="eltdf-position-left">
                                <div class="eltdf-position-left-inner">
                                    <div class="eltdf-logo-wrapper">
                                        <a itemprop="url" href="#" style="height: 27px;">
                                            <img itemprop="image" class="eltdf-normal-logo" src="<?php bloginfo('template_url'); ?>/images/05-logo-sticky-header.png" width="437" height="54" alt="logo" />
                                            <img itemprop="image" class="eltdf-dark-logo" src="<?php bloginfo('template_url'); ?>/images/05-logo-standard-header-dark.png" width="437" height="54" alt="dark logo" />
                                            <img itemprop="image" class="eltdf-light-logo" src="<?php bloginfo('template_url'); ?>/images/05-logo-standard-header-light.png" width="430" height="54" alt="light logo" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="eltdf-position-center">
                                <div class="eltdf-position-center-inner">
                                    <nav class="eltdf-main-menu eltdf-drop-down eltdf-sticky-nav">
                                        <?php 
                                        wp_nav_menu(array(
                                            'theme_location' => 'header',
                                            'menu_class'    => '',
                                            'container'     => '',
                                            'container_class'=>'',
                                            'container_id' => ''
                                        ))
                                        ?>
                                    </nav>
                                </div>
                            </div>
                            <div class="eltdf-position-right">
                                <div class="eltdf-position-right-inner">
                                    <a style="margin: 0 25px 0 0;" class="eltdf-search-opener eltdf-icon-has-hover eltdf-search-opener-icon-pack" href="javascript:void(0)">
                                        <span class="eltdf-search-opener-wrapper"> <span aria-hidden="true" class="eltdf-icon-font-elegant icon_search"></span> </span>
                                    </a>
                                    <a class="eltdf-side-menu-button-opener eltdf-icon-has-hover eltdf-side-menu-button-opener-svg-path" href="javascript:void(0)">
                                        <span class="eltdf-side-menu-icon">
                                            <svg
                                            class="eltdf-burger"
                                            version="1.1"
                                            xmlns="http://www.w3.org/2000/svg"
                                            xmlns:xlink="http://www.w3.org/1999/xlink"
                                            x="0px"
                                            y="0px"
                                            viewbox="0 0 20 16"
                                            enable-background="new 0 0 20 16"
                                            xml:space="preserve"
                                            >
                                            <rect class="eltdf-burger-bar-1" y="0" width="20" height="2"></rect>
                                            <rect class="eltdf-burger-bar-2" y="7" width="20" height="2"></rect>
                                            <rect class="eltdf-burger-bar-3" y="14" width="20" height="2"></rect>
                                        </svg>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <header class="eltdf-mobile-header">
            <div class="eltdf-mobile-header-inner">
                <div class="eltdf-mobile-header-holder">
                    <div class="eltdf-grid">
                        <div class="eltdf-vertical-align-containers">
                            <div class="eltdf-vertical-align-containers">
                                <div class="eltdf-mobile-menu-opener eltdf-mobile-menu-opener-icon-pack">
                                    <a href="javascript:void(0)">
                                        <span class="eltdf-mobile-menu-icon"> <span aria-hidden="true" class="eltdf-icon-font-elegant icon_menu"></span> </span>
                                    </a>
                                </div>
                                <div class="eltdf-position-center">
                                    <div class="eltdf-position-center-inner">
                                        <div class="eltdf-mobile-logo-wrapper">

                                            <?php

                                                $menu = wp_get_nav_menu_object('header');
                                                $logo = get_field('inner_logo', $menu);

                                            ?>

                                            <a itemprop="url" href="#" style="height: 21px;">
                                                <img itemprop="image" src="<?php echo $logo; ?>" width="350" height="43" alt="Mobile Logo" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="eltdf-position-right">
                                    <div class="eltdf-position-right-inner"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <nav class="eltdf-mobile-nav">
                    <?php 
                        wp_nav_menu(array(
                            'theme_location' => 'header',
                            'menu_class'    => '',
                            'container'     => '',
                            'container_class'=>'',
                            'container_id' => ''
                        ))
                    ?>
                    <div class="login-user-inner-m"> 
                        <?php if ( is_user_logged_in() ) { ?>  
                               <!-- <a href="<?php echo the_permalink(145); ?>"> <?php echo  $current_user->user_login; ?></a> -->
                               <ul>
                                    <li>
                                        <a href="<?php echo the_permalink(145); ?>">User Eevnts</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo the_permalink(196); ?>">User Gallery</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo wp_logout_url(get_permalink(170))?>"> Log Out</a>
                                    </li>
                                </ul>
                            <?php } else { ?>
                                             <a href="<?php echo the_permalink(170); ?>"> Login
                                             </a>
                                        <?php } ?>
                                        </div>
                </nav>

            </div>
        </header>