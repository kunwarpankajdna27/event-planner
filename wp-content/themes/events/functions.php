<?php

register_nav_menus(
    array(
        'header' => __('Header Menu','headermenu'),
        'footer' => __('Footer Menu','footermenu'),
        // 'footer_help' => __('Footer Help Menu','footerhelpmenu')
    )
);


add_theme_support('post-thumbnails');
$defaults = array(
  'default-image'          => '',
  'width'                  => 0,
  'height'                 => 0,
  'flex-height'            => false,
  'flex-width'             => false,
  'uploads'                => true,
  'random-default'         => false,
  'header-text'            => true,
  'default-text-color'     => '',
  'wp-head-callback'       => '',
  'admin-head-callback'    => '',
  'admin-preview-callback' => '',
);
add_theme_support( 'custom-header', $defaults );


 register_sidebar(
    array(
        'name'          => __( 'Primary Sidebar', 'Primary Sidebar' ),
        'id'            => 'primary-sidebar',
        'before_widget' => '<div class="sidebar-box ftco-animate">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="heading">',
        'after_title'   => '</h3>',
    )
);

 function wpb_widgets_init() {
 
    register_sidebar( array(
        'name'          => 'Footer Widget Area',
        'id'            => 'footer-widget',
        'before_widget' => '<div class="chw-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="chw-title">',
        'after_title'   => '</h2>',
    ) );
 
}
add_action('widgets_init','wpb_widgets_init');

?>