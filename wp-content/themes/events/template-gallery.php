<?php

// Template Name:Gallery

get_header('inner');

?>

<a id="eltdf-back-to-top" href="#">
    <span class="eltdf-text-stack"> top </span>
</a>

<section class="inner-banner">
	<div class="container-fluid">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="gallery-block">
	<div class="container-fluid">
		<div class="container">
			<div class="row">
				<div class="eltdf-portfolio-list-holder eltdf-grid-list eltdf-grid-masonry-list eltdf-disable-bottom-space eltdf-pl-masonry eltdf-four-columns eltdf-normal-space eltdf-pl-gallery-overlay eltdf-pl-pag-no-pagination" data-type="masonry" data-number-of-columns="four" data-space-between-items="normal" data-number-of-items="13" data-image-proportions="full" data-enable-fixed-proportions="no" data-enable-image-shadow="no" data-category="decoration" data-orderby="date" data-order="ASC" data-item-style="gallery-overlay" data-enable-title="yes" data-title-tag="h4" data-enable-category="no" data-enable-count-images="yes" data-enable-excerpt="no" data-excerpt-length="20" data-pagination-type="no-pagination" data-filter="no" data-filter-order-by="name" data-enable-article-animation="no" data-portfolio-slider-on="no" data-enable-loop="yes" data-enable-autoplay="yes" data-slider-speed="5000" data-slider-speed-animation="600" data-enable-navigation="yes" data-enable-pagination="yes" data-max-num-pages="2" data-next-page="2">
				<div class="eltdf-pl-inner eltdf-outer-space eltdf-masonry-list-wrapper clearfix">
                    <div class="eltdf-masonry-grid-sizer"></div>
                    <div class="eltdf-masonry-grid-gutter"></div>
                    	
                    <?php

		                $arraydata = array(
		                  'post_type' => 'gallery' ,
		                  'post_status' => 'publish' ,
		                  'order' => 'DESC' ,
		                  'orderby' => 'data',

		                );

		                $post_data = new WP_Query($arraydata);
		                $i =1;
		                while( $post_data->have_posts() ) 
		                {
		                  $post_data->the_post();

		                  $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()),'full');

	                ?>

                    <article
                        class="eltdf-pl-item eltdf-item-space post-1402 portfolio-item type-portfolio-item status-publish has-post-thumbnail hentry portfolio-category-decoration portfolio-tag-ceremony portfolio-tag-dresses"
                    >
                        <div class="eltdf-pl-item-inner">
                            <div class="eltdf-pli-image">
                                <img
                                    width="800"
                                    height="1200"
                                    src="<?php echo $image[0]; ?>"
                                    class="attachment-full size-full wp-post-image"
                                    alt="<?php the_title(); ?>"
                                    sizes="(max-width: 800px) 100vw, 800px"
                                />
                            </div>

                            <div class="eltdf-pli-text-holder">
                                <div class="eltdf-pli-text-wrapper">
                                    <div class="eltdf-pli-text">
                                        <h4 itemprop="name" class="eltdf-pli-title entry-title">
                                            <?php the_title(); ?>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>

                	<?php } ?>

                  
                </div>
            </div>
				
			</div>
		</div>
	</div>
</section>
                       
<?php

get_footer();

?>