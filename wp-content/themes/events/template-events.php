<?php 

// Template Name: Events 

get_header('inner'); 

the_post();

?>

<a id="eltdf-back-to-top" href="#">
    <span class="eltdf-text-stack"> top </span>
</a>

<section class="inner-banner">
	<div class="container-fluid">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="events-info">
 <div class="container-fluid">
	<div class="container">
		<div class="row">
			<p><?php the_content(); ?></p>
		</div>
	</div>
</div>
</section>

<section class="events-grp">
	<div class="container-fluid">
		<div class="container">
			<div class="row">

				<?php

	                $arraydata = array(
	                  'post_type' => 'events' ,
	                  'post_status' => 'publish' ,
	                  'posts_per_pages' => 5 ,
	                  'order' => 'DESC' ,
	                  'orderby' => 'data',

	                );

	                $post_data = new WP_Query($arraydata);
	                $i =1;
	                while( $post_data->have_posts() ) 
	                {
	                  $post_data->the_post();

	                  $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()),'full');

	                  if ($i%2!=0) 
	                  {
	                  	$div = '<div class="col-xs-12 col-sm-12 col-lg-4 col-md-4">
                  					<img src="'.$image[0].'" alt="'.get_the_title().'" class="img-responsive" width="100%" />
                  				</div>
                  				<div class="col-xs-12 col-sm-12 col-lg-8 col-md-8">
		                  			<h4>'.get_the_title().'</h4>
									'.get_the_content().'
                  				</div>';
	                  }
	                  else{
	                  	$div = '<div class="col-xs-12 col-sm-12 col-lg-8 col-md-8">
		                  			<h4><?php get_the_title(); ?></h4>
									<h4>'.get_the_title().'</h4>
										'.get_the_content().'
                  				</div>
	                  			<div class="col-xs-12 col-sm-12 col-lg-4 col-md-4">
                  					<img src="'.$image[0].'" alt="'.get_the_title().'" class="img-responsive" width="100%" />
                  				</div>';
	                  }
	            	?>
		            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 sing-event" id="<?php the_title() ?>">
						<?php echo $div; ?>
					</div>

				<?php $i++; } ?>

			</div>
		</div>
	</div>
</section>



<?php get_footer(); ?>