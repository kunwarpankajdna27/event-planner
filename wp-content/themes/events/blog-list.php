<?php

get_header('inner');

?>
                <a id="eltdf-back-to-top" href="#">
                    <span class="eltdf-text-stack"> top </span>
                </a>
                <div class="eltdf-content">
                    <div class="eltdf-content-inner">
                        <div
                            class="eltdf-title-holder eltdf-centered-type eltdf-title-va-header-bottom eltdf-preload-background eltdf-has-bg-image eltdf-bg-parallax"
                            style="height: 385px; background-color: #fbb8ac; background-image: url(https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/05/elements-title-img.jpg);"
                            data-height="385"
                        >
                            <div class="eltdf-title-image">
                                <img itemprop="image" src="images/05-elements-title-img.jpg" alt="a" />
                            </div>

                            <div class="eltdf-title-wrapper" style="height: 385px;">
                                <div class="eltdf-title-inner">
                                    <div class="eltdf-grid">
                                        <h1 class="eltdf-page-title entry-title">Blog List</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="eltdf-full-width">
                            <div class="eltdf-full-width-inner">
                                <div class="eltdf-grid-row">
                                    <div class="eltdf-page-content-holder eltdf-grid-col-12">
                                        <div class="eltdf-row-grid-section-wrapper">
                                            <div class="eltdf-row-grid-section">
                                                <div class="vc_row wpb_row vc_row-fluid vc_custom_1525862262422">
                                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                                        <div class="vc_column-inner">
                                                            <div class="wpb_wrapper">
                                                                <div
                                                                    class="eltdf-blog-list-holder eltdf-grid-list eltdf-disable-bottom-space eltdf-bl-standard eltdf-three-columns eltdf-normal-space eltdf-bl-pag-no-pagination"
                                                                    data-type="standard"
                                                                    data-number-of-posts="3"
                                                                    data-number-of-columns="three"
                                                                    data-space-between-items="normal"
                                                                    data-category="dancing"
                                                                    data-orderby="date"
                                                                    data-order="ASC"
                                                                    data-image-size="full"
                                                                    data-title-tag="h4"
                                                                    data-excerpt-length="18"
                                                                    data-post-info-section="yes"
                                                                    data-post-info-image="yes"
                                                                    data-post-info-author="no"
                                                                    data-post-info-date="yes"
                                                                    data-post-info-category="yes"
                                                                    data-post-info-comments="no"
                                                                    data-post-info-like="no"
                                                                    data-post-info-share="no"
                                                                    data-pagination-type="no-pagination"
                                                                    data-max-num-pages="2"
                                                                    data-next-page="2"
                                                                >
                                                                    <div class="eltdf-bl-wrapper eltdf-outer-space">
                                                                        <ul class="eltdf-blog-list">
                                                                            <li class="eltdf-bl-item eltdf-item-space">
                                                                                <div class="eltdf-bli-inner">
                                                                                    <div class="eltdf-post-image">
                                                                                        <a itemprop="url" href="wedding-in-nature.html" title="Wedding in Nature">
                                                                                            <img
                                                                                                width="1300"
                                                                                                height="800"
                                                                                                src="images/05-blog-1-img-7.jpg"
                                                                                                class="attachment-full size-full wp-post-image"
                                                                                                alt="a"
                                                                                                srcset="
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/05/blog-1-img-7.jpg          1300w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/05/blog-1-img-7-300x185.jpg   300w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/05/blog-1-img-7-768x473.jpg   768w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/05/blog-1-img-7-1024x630.jpg 1024w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/05/blog-1-img-7-800x492.jpg   800w
                                                                                                "
                                                                                                sizes="(max-width: 1300px) 100vw, 1300px"
                                                                                            />
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="eltdf-bli-content">
                                                                                        <h4 itemprop="name" class="entry-title eltdf-post-title">
                                                                                            <a itemprop="url" href="wedding-in-nature.html" title="Wedding in Nature"> Wedding in Nature </a>
                                                                                        </h4>
                                                                                        <div class="eltdf-post-info-category">
                                                                                            <a href="dancing.html" rel="category tag">Dancing</a>
                                                                                        </div>
                                                                                        <div class="eltdf-bli-excerpt">
                                                                                            <div class="eltdf-post-excerpt-holder">
                                                                                                <p itemprop="description" class="eltdf-post-excerpt">
                                                                                                    Aenean commodo ligula eget dolorm aenean massa. Cum sociis theme natoque penatibus et magnis dis parturient montes, nascetur
                                                                                                </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="eltdf-bli-info">
                                                                                            <div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
                                                                                                <a itemprop="url" href="05.html"> 10. May 2018. </a>
                                                                                                <meta itemprop="interactionCount" content="UserComments: 0" />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li class="eltdf-bl-item eltdf-item-space">
                                                                                <div class="eltdf-bli-inner">
                                                                                    <div class="eltdf-post-image">
                                                                                        <a itemprop="url" href="colorful-bouquets.html" title="Colorful Bouquets">
                                                                                            <img
                                                                                                width="1300"
                                                                                                height="800"
                                                                                                src="images/04-blog-1-img-4.jpg"
                                                                                                class="attachment-full size-full wp-post-image"
                                                                                                alt="a"
                                                                                                srcset="
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-4.jpg          1300w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-4-300x185.jpg   300w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-4-768x473.jpg   768w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-4-1024x630.jpg 1024w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-4-800x492.jpg   800w
                                                                                                "
                                                                                                sizes="(max-width: 1300px) 100vw, 1300px"
                                                                                            />
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="eltdf-bli-content">
                                                                                        <h4 itemprop="name" class="entry-title eltdf-post-title">
                                                                                            <a itemprop="url" href="colorful-bouquets.html" title="Colorful Bouquets"> Colorful Bouquets </a>
                                                                                        </h4>
                                                                                        <div class="eltdf-post-info-category">
                                                                                            <a href="dancing.html" rel="category tag">Dancing</a>
                                                                                        </div>
                                                                                        <div class="eltdf-bli-excerpt">
                                                                                            <div class="eltdf-post-excerpt-holder">
                                                                                                <p itemprop="description" class="eltdf-post-excerpt">
                                                                                                    Aenean commodo ligula eget dolorm aenean massa. Cum sociis theme natoque penatibus et magnis dis parturient montes, nascetur
                                                                                                </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="eltdf-bli-info">
                                                                                            <div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
                                                                                                <a itemprop="url" href="05.html"> 10. May 2018. </a>
                                                                                                <meta itemprop="interactionCount" content="UserComments: 0" />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li class="eltdf-bl-item eltdf-item-space">
                                                                                <div class="eltdf-bli-inner">
                                                                                    <div class="eltdf-post-image">
                                                                                        <a itemprop="url" href="wedding-venues.html" title="Wedding Venues">
                                                                                            <img
                                                                                                width="1300"
                                                                                                height="800"
                                                                                                src="images/04-blog-1-img-2.jpg"
                                                                                                class="attachment-full size-full wp-post-image"
                                                                                                alt="a"
                                                                                                srcset="
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-2.jpg          1300w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-2-300x185.jpg   300w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-2-768x473.jpg   768w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-2-1024x630.jpg 1024w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-2-800x492.jpg   800w
                                                                                                "
                                                                                                sizes="(max-width: 1300px) 100vw, 1300px"
                                                                                            />
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="eltdf-bli-content">
                                                                                        <h4 itemprop="name" class="entry-title eltdf-post-title">
                                                                                            <a itemprop="url" href="wedding-venues.html" title="Wedding Venues"> Wedding Venues </a>
                                                                                        </h4>
                                                                                        <div class="eltdf-post-info-category">
                                                                                            <a href="dancing.html" rel="category tag">Dancing</a>
                                                                                        </div>
                                                                                        <div class="eltdf-bli-excerpt">
                                                                                            <div class="eltdf-post-excerpt-holder">
                                                                                                <p itemprop="description" class="eltdf-post-excerpt">
                                                                                                    Aenean commodo ligula eget dolorm aenean massa. Cum sociis theme natoque penatibus et magnis dis parturient montes, nascetur
                                                                                                </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="eltdf-bli-info">
                                                                                            <div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
                                                                                                <a itemprop="url" href="05.html"> 10. May 2018. </a>
                                                                                                <meta itemprop="interactionCount" content="UserComments: 0" />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="eltdf-row-grid-section-wrapper" style="background-color: #f4f2ef;">
                                            <div class="eltdf-row-grid-section">
                                                <div class="vc_row wpb_row vc_row-fluid vc_custom_1525440815587">
                                                    <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-offset-0 vc_col-lg-12 vc_col-md-offset-0 vc_col-md-12 vc_col-sm-offset-0 vc_col-xs-12">
                                                        <div class="vc_column-inner">
                                                            <div class="wpb_wrapper">
                                                                <div
                                                                    class="eltdf-blog-list-holder eltdf-grid-list eltdf-disable-bottom-space eltdf-bl-boxed eltdf-three-columns eltdf-normal-space eltdf-bl-pag-no-pagination"
                                                                    data-type="boxed"
                                                                    data-number-of-posts="3"
                                                                    data-number-of-columns="three"
                                                                    data-space-between-items="normal"
                                                                    data-category="wedding"
                                                                    data-orderby="date"
                                                                    data-order="ASC"
                                                                    data-image-size="full"
                                                                    data-title-tag="h4"
                                                                    data-excerpt-length="18"
                                                                    data-post-info-section="yes"
                                                                    data-post-info-image="yes"
                                                                    data-post-info-author="no"
                                                                    data-post-info-date="yes"
                                                                    data-post-info-category="yes"
                                                                    data-post-info-comments="no"
                                                                    data-post-info-like="no"
                                                                    data-post-info-share="no"
                                                                    data-pagination-type="no-pagination"
                                                                    data-max-num-pages="1"
                                                                    data-next-page="2"
                                                                >
                                                                    <div class="eltdf-bl-wrapper eltdf-outer-space">
                                                                        <ul class="eltdf-blog-list">
                                                                            <li class="eltdf-bl-item eltdf-item-space">
                                                                                <div class="eltdf-bli-inner">
                                                                                    <div class="eltdf-post-image">
                                                                                        <a itemprop="url" href="double-wedding.html" title="Double Wedding">
                                                                                            <img
                                                                                                width="1300"
                                                                                                height="800"
                                                                                                src="images/04-blog-1-img-4.jpg"
                                                                                                class="attachment-full size-full"
                                                                                                alt="a"
                                                                                                srcset="
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-4.jpg          1300w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-4-300x185.jpg   300w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-4-768x473.jpg   768w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-4-1024x630.jpg 1024w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-4-800x492.jpg   800w
                                                                                                "
                                                                                                sizes="(max-width: 1300px) 100vw, 1300px"
                                                                                            />
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="eltdf-bli-content">
                                                                                        <h4 itemprop="name" class="entry-title eltdf-post-title">
                                                                                            <a itemprop="url" href="double-wedding.html" title="Double Wedding"> Double Wedding </a>
                                                                                        </h4>
                                                                                        <div class="eltdf-post-info-category">
                                                                                            <a href="wedding.html" rel="category tag">Wedding</a>
                                                                                        </div>
                                                                                        <div class="eltdf-bli-excerpt">
                                                                                            <div class="eltdf-post-excerpt-holder">
                                                                                                <p itemprop="description" class="eltdf-post-excerpt">
                                                                                                    Aenean commodo ligula eget dolorm aenean massa. Cum sociis theme natoque penatibus et magnis dis parturient montes, nascetur
                                                                                                </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="eltdf-bli-info">
                                                                                            <div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
                                                                                                <a itemprop="url" href="05.html"> 10. May 2018. </a>
                                                                                                <meta itemprop="interactionCount" content="UserComments: 0" />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li class="eltdf-bl-item eltdf-item-space">
                                                                                <div class="eltdf-bli-inner">
                                                                                    <div class="eltdf-post-image">
                                                                                        <a itemprop="url" href="outdoors-ceremony.html" title="Outdoors Ceremony">
                                                                                            <img
                                                                                                width="1300"
                                                                                                height="800"
                                                                                                src="images/05-blog-1-img-12.jpg"
                                                                                                class="attachment-full size-full wp-post-image"
                                                                                                alt="a"
                                                                                                srcset="
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/05/blog-1-img-12.jpg          1300w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/05/blog-1-img-12-300x185.jpg   300w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/05/blog-1-img-12-768x473.jpg   768w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/05/blog-1-img-12-1024x630.jpg 1024w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/05/blog-1-img-12-800x492.jpg   800w
                                                                                                "
                                                                                                sizes="(max-width: 1300px) 100vw, 1300px"
                                                                                            />
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="eltdf-bli-content">
                                                                                        <h4 itemprop="name" class="entry-title eltdf-post-title">
                                                                                            <a itemprop="url" href="outdoors-ceremony.html" title="Outdoors Ceremony"> Outdoors Ceremony </a>
                                                                                        </h4>
                                                                                        <div class="eltdf-post-info-category">
                                                                                            <a href="wedding.html" rel="category tag">Wedding</a>
                                                                                        </div>
                                                                                        <div class="eltdf-bli-excerpt">
                                                                                            <div class="eltdf-post-excerpt-holder">
                                                                                                <p itemprop="description" class="eltdf-post-excerpt">
                                                                                                    Aenean commodo ligula eget dolorm aenean massa. Cum sociis theme natoque penatibus et magnis dis parturient montes, nascetur
                                                                                                </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="eltdf-bli-info">
                                                                                            <div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
                                                                                                <a itemprop="url" href="05.html"> 10. May 2018. </a>
                                                                                                <meta itemprop="interactionCount" content="UserComments: 0" />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li class="eltdf-bl-item eltdf-item-space">
                                                                                <div class="eltdf-bli-inner">
                                                                                    <div class="eltdf-post-image">
                                                                                        <a itemprop="url" href="white-wedding.html" title="White Wedding">
                                                                                            <img
                                                                                                width="1300"
                                                                                                height="800"
                                                                                                src="images/04-blog-1-img-6.jpg"
                                                                                                class="attachment-full size-full wp-post-image"
                                                                                                alt="a"
                                                                                                srcset="
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-6.jpg          1300w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-6-300x185.jpg   300w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-6-768x473.jpg   768w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-6-1024x630.jpg 1024w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-6-800x492.jpg   800w
                                                                                                "
                                                                                                sizes="(max-width: 1300px) 100vw, 1300px"
                                                                                            />
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="eltdf-bli-content">
                                                                                        <h4 itemprop="name" class="entry-title eltdf-post-title">
                                                                                            <a itemprop="url" href="white-wedding.html" title="White Wedding"> White Wedding </a>
                                                                                        </h4>
                                                                                        <div class="eltdf-post-info-category">
                                                                                            <a href="wedding.html" rel="category tag">Wedding</a>
                                                                                        </div>
                                                                                        <div class="eltdf-bli-excerpt">
                                                                                            <div class="eltdf-post-excerpt-holder">
                                                                                                <p itemprop="description" class="eltdf-post-excerpt">
                                                                                                    Aenean commodo ligula eget dolorm aenean massa. Cum sociis theme natoque penatibus et magnis dis parturient montes, nascetur
                                                                                                </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="eltdf-bli-info">
                                                                                            <div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
                                                                                                <a itemprop="url" href="05.html"> 10. May 2018. </a>
                                                                                                <meta itemprop="interactionCount" content="UserComments: 0" />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="eltdf-row-grid-section-wrapper eltdf-content-aligment-left">
                                            <div class="eltdf-row-grid-section">
                                                <div class="vc_row wpb_row vc_row-fluid vc_custom_1526642110670">
                                                    <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-offset-0 vc_col-lg-12 vc_col-md-offset-0 vc_col-md-12 vc_col-sm-offset-0 vc_col-xs-12">
                                                        <div class="vc_column-inner">
                                                            <div class="wpb_wrapper">
                                                                <div
                                                                    class="eltdf-blog-list-holder eltdf-grid-list eltdf-disable-bottom-space eltdf-bl-simple eltdf-four-columns eltdf-normal-space eltdf-bl-pag-no-pagination"
                                                                    data-type="simple"
                                                                    data-number-of-posts="4"
                                                                    data-number-of-columns="four"
                                                                    data-space-between-items="normal"
                                                                    data-category="dancing"
                                                                    data-orderby="date"
                                                                    data-order="ASC"
                                                                    data-image-size="full"
                                                                    data-title-tag="h4"
                                                                    data-excerpt-length="40"
                                                                    data-post-info-section="yes"
                                                                    data-post-info-image="yes"
                                                                    data-post-info-author="yes"
                                                                    data-post-info-date="yes"
                                                                    data-post-info-category="yes"
                                                                    data-post-info-comments="no"
                                                                    data-post-info-like="no"
                                                                    data-post-info-share="no"
                                                                    data-pagination-type="no-pagination"
                                                                    data-max-num-pages="1"
                                                                    data-next-page="2"
                                                                >
                                                                    <div class="eltdf-bl-wrapper eltdf-outer-space">
                                                                        <ul class="eltdf-blog-list">
                                                                            <li class="eltdf-bl-item eltdf-item-space clearfix">
                                                                                <div class="eltdf-bli-inner">
                                                                                    <div class="eltdf-post-image">
                                                                                        <a itemprop="url" href="wedding-in-nature.html" title="Wedding in Nature">
                                                                                            <img
                                                                                                width="1300"
                                                                                                height="800"
                                                                                                src="images/05-blog-1-img-7.jpg"
                                                                                                class="attachment-full size-full wp-post-image"
                                                                                                alt="a"
                                                                                                srcset="
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/05/blog-1-img-7.jpg          1300w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/05/blog-1-img-7-300x185.jpg   300w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/05/blog-1-img-7-768x473.jpg   768w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/05/blog-1-img-7-1024x630.jpg 1024w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/05/blog-1-img-7-800x492.jpg   800w
                                                                                                "
                                                                                                sizes="(max-width: 1300px) 100vw, 1300px"
                                                                                            />
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="eltdf-bli-content">
                                                                                        <h4 itemprop="name" class="entry-title eltdf-post-title">
                                                                                            <a itemprop="url" href="wedding-in-nature.html" title="Wedding in Nature"> Wedding in Nature </a>
                                                                                        </h4>
                                                                                        <div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
                                                                                            <a itemprop="url" href="05.html"> 10. May 2018. </a>
                                                                                            <meta itemprop="interactionCount" content="UserComments: 0" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li class="eltdf-bl-item eltdf-item-space clearfix">
                                                                                <div class="eltdf-bli-inner">
                                                                                    <div class="eltdf-post-image">
                                                                                        <a itemprop="url" href="colorful-bouquets.html" title="Colorful Bouquets">
                                                                                            <img
                                                                                                width="1300"
                                                                                                height="800"
                                                                                                src="images/04-blog-1-img-4.jpg"
                                                                                                class="attachment-full size-full wp-post-image"
                                                                                                alt="a"
                                                                                                srcset="
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-4.jpg          1300w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-4-300x185.jpg   300w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-4-768x473.jpg   768w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-4-1024x630.jpg 1024w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-4-800x492.jpg   800w
                                                                                                "
                                                                                                sizes="(max-width: 1300px) 100vw, 1300px"
                                                                                            />
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="eltdf-bli-content">
                                                                                        <h4 itemprop="name" class="entry-title eltdf-post-title">
                                                                                            <a itemprop="url" href="colorful-bouquets.html" title="Colorful Bouquets"> Colorful Bouquets </a>
                                                                                        </h4>
                                                                                        <div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
                                                                                            <a itemprop="url" href="05.html"> 10. May 2018. </a>
                                                                                            <meta itemprop="interactionCount" content="UserComments: 0" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li class="eltdf-bl-item eltdf-item-space clearfix">
                                                                                <div class="eltdf-bli-inner">
                                                                                    <div class="eltdf-post-image">
                                                                                        <a itemprop="url" href="wedding-venues.html" title="Wedding Venues">
                                                                                            <img
                                                                                                width="1300"
                                                                                                height="800"
                                                                                                src="images/04-blog-1-img-2.jpg"
                                                                                                class="attachment-full size-full wp-post-image"
                                                                                                alt="a"
                                                                                                srcset="
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-2.jpg          1300w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-2-300x185.jpg   300w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-2-768x473.jpg   768w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-2-1024x630.jpg 1024w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-2-800x492.jpg   800w
                                                                                                "
                                                                                                sizes="(max-width: 1300px) 100vw, 1300px"
                                                                                            />
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="eltdf-bli-content">
                                                                                        <h4 itemprop="name" class="entry-title eltdf-post-title">
                                                                                            <a itemprop="url" href="wedding-venues.html" title="Wedding Venues"> Wedding Venues </a>
                                                                                        </h4>
                                                                                        <div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
                                                                                            <a itemprop="url" href="05.html"> 10. May 2018. </a>
                                                                                            <meta itemprop="interactionCount" content="UserComments: 0" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li class="eltdf-bl-item eltdf-item-space clearfix">
                                                                                <div class="eltdf-bli-inner">
                                                                                    <div class="eltdf-post-image">
                                                                                        <a itemprop="url" href="wedding-ceremony.html" title="Wedding Ceremony">
                                                                                            <img
                                                                                                width="1300"
                                                                                                height="800"
                                                                                                src="images/04-blog-1-img-1.jpg"
                                                                                                class="attachment-full size-full wp-post-image"
                                                                                                alt="a"
                                                                                                srcset="
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-1.jpg          1300w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-1-300x185.jpg   300w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-1-768x473.jpg   768w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-1-1024x630.jpg 1024w,
                                                                                                    https://gretnagreen.qodeinteractive.com/wp-content/uploads/2018/04/blog-1-img-1-800x492.jpg   800w
                                                                                                "
                                                                                                sizes="(max-width: 1300px) 100vw, 1300px"
                                                                                            />
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="eltdf-bli-content">
                                                                                        <h4 itemprop="name" class="entry-title eltdf-post-title">
                                                                                            <a itemprop="url" href="wedding-ceremony.html" title="Wedding Ceremony"> Wedding Ceremony </a>
                                                                                        </h4>
                                                                                        <div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
                                                                                            <a itemprop="url" href="05.html"> 10. May 2018. </a>
                                                                                            <meta itemprop="interactionCount" content="UserComments: 0" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="eltdf-row-grid-section-wrapper eltdf-content-aligment-center" style="background-color: #f4f2ef;">
                                            <div class="eltdf-row-grid-section">
                                                <div class="vc_row wpb_row vc_row-fluid vc_custom_1526471656591">
                                                    <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-offset-0 vc_col-lg-12 vc_col-md-offset-0 vc_col-md-12 vc_col-sm-offset-0 vc_col-xs-12">
                                                        <div class="vc_column-inner">
                                                            <div class="wpb_wrapper">
                                                                <div
                                                                    class="eltdf-blog-list-holder eltdf-grid-list eltdf-disable-bottom-space eltdf-bl-minimal eltdf-four-columns eltdf-normal-space eltdf-bl-pag-no-pagination"
                                                                    data-type="minimal"
                                                                    data-number-of-posts="4"
                                                                    data-number-of-columns="four"
                                                                    data-space-between-items="normal"
                                                                    data-category="dancing"
                                                                    data-orderby="date"
                                                                    data-order="ASC"
                                                                    data-image-size="full"
                                                                    data-title-tag="h4"
                                                                    data-excerpt-length="40"
                                                                    data-post-info-section="yes"
                                                                    data-post-info-image="yes"
                                                                    data-post-info-author="yes"
                                                                    data-post-info-date="yes"
                                                                    data-post-info-category="yes"
                                                                    data-post-info-comments="no"
                                                                    data-post-info-like="no"
                                                                    data-post-info-share="no"
                                                                    data-pagination-type="no-pagination"
                                                                    data-max-num-pages="1"
                                                                    data-next-page="2"
                                                                >
                                                                    <div class="eltdf-bl-wrapper eltdf-outer-space">
                                                                        <ul class="eltdf-blog-list">
                                                                            <li class="eltdf-bl-item eltdf-item-space clearfix">
                                                                                <div class="eltdf-bli-inner">
                                                                                    <div class="eltdf-bli-content">
                                                                                        <h4 itemprop="name" class="entry-title eltdf-post-title">
                                                                                            <a itemprop="url" href="wedding-in-nature.html" title="Wedding in Nature"> Wedding in Nature </a>
                                                                                        </h4>
                                                                                        <div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
                                                                                            <a itemprop="url" href="05.html"> 10. May 2018. </a>
                                                                                            <meta itemprop="interactionCount" content="UserComments: 0" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li class="eltdf-bl-item eltdf-item-space clearfix">
                                                                                <div class="eltdf-bli-inner">
                                                                                    <div class="eltdf-bli-content">
                                                                                        <h4 itemprop="name" class="entry-title eltdf-post-title">
                                                                                            <a itemprop="url" href="colorful-bouquets.html" title="Colorful Bouquets"> Colorful Bouquets </a>
                                                                                        </h4>
                                                                                        <div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
                                                                                            <a itemprop="url" href="05.html"> 10. May 2018. </a>
                                                                                            <meta itemprop="interactionCount" content="UserComments: 0" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li class="eltdf-bl-item eltdf-item-space clearfix">
                                                                                <div class="eltdf-bli-inner">
                                                                                    <div class="eltdf-bli-content">
                                                                                        <h4 itemprop="name" class="entry-title eltdf-post-title">
                                                                                            <a itemprop="url" href="wedding-venues.html" title="Wedding Venues"> Wedding Venues </a>
                                                                                        </h4>
                                                                                        <div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
                                                                                            <a itemprop="url" href="05.html"> 10. May 2018. </a>
                                                                                            <meta itemprop="interactionCount" content="UserComments: 0" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li class="eltdf-bl-item eltdf-item-space clearfix">
                                                                                <div class="eltdf-bli-inner">
                                                                                    <div class="eltdf-bli-content">
                                                                                        <h4 itemprop="name" class="entry-title eltdf-post-title">
                                                                                            <a itemprop="url" href="wedding-ceremony.html" title="Wedding Ceremony"> Wedding Ceremony </a>
                                                                                        </h4>
                                                                                        <div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
                                                                                            <a itemprop="url" href="05.html"> 10. May 2018. </a>
                                                                                            <meta itemprop="interactionCount" content="UserComments: 0" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<?php

get_footer();

?>