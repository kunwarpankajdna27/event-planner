<?php
	get_header('inner');
?>
	<div class="container text-center">
		<!-- <div class="brand">
			<span class="glyphicon glyphicon-king" aria-hidden="true"></span>
		</div> -->
		<h1 class="head"><span>404</span></h1>
		<p>Oops! The Page you requested was not found!</p>
		<a href="<?php echo get_site_url() ?>" class="btn-outline"> Back to Home</a>
	</div>
<?php
	get_footer();
?>