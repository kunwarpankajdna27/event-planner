<?php

// Template Name:Our Service

get_header('inner');

?>

<section class="inner-banner">
	<div class="container-fluid">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="our-service-block">
	<div class="container-fluid">
		<div class="container">
			<div class="row">

				<?php

	                $arraydata = array(
	                  'post_type' => 'our service' ,
	                  'post_status' => 'publish' ,
	                  'order' => 'DESC' ,
	                  'orderby' => 'data',

	                );

	                $post_data = new WP_Query($arraydata);
	                $i =1;
	                while( $post_data->have_posts() ) 
	                {
	                  $post_data->the_post();

	                  $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()),'full');

                ?>
				
				<div class="col-md-4 col-xs-12 col-lg-4 col-sm-12">
					<div class="singleService">
						<div class="serviceCoverImg">
							<img src="<?php echo $image[0] ?>" alt="<?php the_title(); ?>" class="img-responsive img-fluid">
						</div>
						<div class="serviceContent">
							<h4><?php the_title(); ?></h4>
						</div>
					</div>
				</div>

				<?php } ?>

			</div>
		</div>
	</div>
</section>

<?php

get_footer();

?>