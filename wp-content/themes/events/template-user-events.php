<?php 

// Template Name: User Events 

if( !is_user_logged_in() ) {
    wp_redirect('login');
    exit;
}

get_header('inner'); 

the_post();

$user_id = get_current_user_id();
$user = get_userdata($user_id);

?>

<a id="eltdf-back-to-top" href="#">
    <span class="eltdf-text-stack"> top </span>
</a>

<section class="inner-banner" style="background: linear-gradient(rgba(0,0,0,0.3),rgba(0,0,0,0.3)), url('<?php echo bloginfo('template_url');?>/images/05-elements-title-img.jpg'); background-size: cover;">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">

                  <?php

                    $arraydata = array(
                    'post_type' => 'user_events' ,
                    'post_status' => 'publish' ,
                    'order' => 'DESC' ,
                    'orderby' => 'data',
                    'posts_per_page' => 1,
                    'meta_query' => array(
                                array(
                                    'key' => 'username', 
                                    'value' => $user_id, 
                                    'compare'   => '=',
                                ),           
                            ),
                     );

                  $post_data = new WP_Query($arraydata);

                  while( $post_data->have_posts() ) 
                  {
                    $post_data->the_post();
                    $events = get_field('detail',get_the_id());
                  }

                ?>
                  <h1><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- <section class="events-info">
 <div class="container-fluid">
	<div class="container">
		<div class="row">
			<p><?php // the_content(); ?></p>
		</div>
	</div>
</div>
</section> -->


<section class="user-event-grp m-t-2" >
	<div class="container-fluid">
		<div class="container">

			<?php


        foreach ($events as $key => $event) 
        {
        	if ($key%2!=0){ 
        		$i = 2;
        		$img = "float-right pull-right";
        		$cont = "float-left pull-left";
        	} else {
        		$i = "";
        		$img = "";
        		$cont= "";
        	}
      ?>

			<div class="row single-event-row<?php echo $i ?>">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6 user-event-img <?php echo $img ?>">
                	<img src="<?php echo $event['image'] ?>" alt="<?php echo $event['title'] ?>"/>
                </div>
                <div class="col-xs-12 col-sm-12 col-lg-6 col-md-6 user-event-content <?php echo $cont ?>">
                	<span> <img width="37" height="27" src="<?php echo bloginfo('template_url') ?>/images/04-h1-custom-icon-img-1.png" class="attachment-full size-full" alt="a" /></span>
	      			<h4><?php echo $event['title'] ?></h4>
	      			<i><?php echo $event['date'] ?></i>
					<p><?php echo $event['description'] ?></p>
				</div>
      </div>

      <?php } ?>

        </div>
	</div>
</section>


<section>
  <div class="container-fluid">
    <div class="container">
      <div class="row">
          <a href="<?php echo get_site_url(); ?>/event-gallery" style="text-decoration: none;">
            <button class="btn btn-info pull-center btnViewMore user-gallery">View Gallery</button>
          </a>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>