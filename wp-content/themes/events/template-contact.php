<?php
// Template Name: contact
get_header('inner');

?>
<a id="eltdf-back-to-top" href="#">
    <span class="eltdf-text-stack"> top </span>
</a>

<section class="inner-banner" style="background: linear-gradient(rgba(0,0,0,0.3),rgba(0,0,0,0.3)), url('<?php echo bloginfo('template_url');?>/images/05-elements-title-img.jpg'); background-size: cover;">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                    <h1><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contact-info">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                    <i class="fa fa-phone"></i>
                    <span><?php echo do_shortcode('[contact type="mobile"]'); ?></span>
                    <span><?php echo do_shortcode('[contact type="mobile2"]'); ?></span>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                    <i class="fa fa-map-marker"></i>
                    <span><?php echo do_shortcode('[contact type="address"]'); ?></span>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                    <i class="fa fa-envelope"></i>
                    <span><?php echo do_shortcode('[contact type="email_address"]'); ?></span>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contact-map">
    <div class="container-fluid">
        <div class="container">
            <iframe src="<?php echo do_shortcode('[contact type="usermap"]'); ?>" width="100%" height="350" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
    </div>
</section>

<section class="contact-form">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <?php  echo do_shortcode('[contact-form-7 id="80" title="Contact form 1"]'); ?>
            </div>
        </div>
    </div>
</section>


<?php
get_footer();
?>