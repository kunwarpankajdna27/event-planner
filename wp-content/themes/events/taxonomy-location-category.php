<?php

get_header('inner');

?>

<a id="eltdf-back-to-top" href="#">
    <span class="eltdf-text-stack"> top </span>
</a>
<?php

$taxonomy_images = get_option( 'taxonomy_image_plugin' );
$attachment_id = $taxonomy_images[get_queried_object_id()];
$image = wp_get_attachment_image_src($attachment_id,'full');
?>

<section class="inner-banner" style="background: linear-gradient( rgba(0,0,0,0.5), rgba(0,0,0,0.5)), url(<?php echo $image[0] ?>) no-repeat center center; background-size: cover;">
	<div class="container-fluid">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
					<h1><?php  single_cat_title(); ?></h1>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="locations-section">
	<div class="container-fluid">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<?php
			            while(have_posts())
			            {
			                the_post();
			                $image = wp_get_attachment_image_src(get_post_thumbnail_id( get_the_id() ), 'full' );
			        ?>
					<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
						<div class="single-location" style="background: url('<?php echo $image[0] ?>') no-repeat center; background-size: cover;" >
							<div class="location-image">
								<h4>
									<?php the_title(); ?>
								</h4>
								<!-- <img src="<?php echo $image[0] ?>" alt="<?php the_title(); ?>" class="img-responsive"> -->
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>



<?php

get_footer();

?>