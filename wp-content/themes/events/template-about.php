<?php

// Template Name:About Us

get_header('inner'); 

the_post();

$image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()),'full');

?>

<a id="eltdf-back-to-top" href="#">
    <span class="eltdf-text-stack"> top </span>
</a>

<section class="inner-banner">
	<div class="container-fluid">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="about-us-block">
	<div class="container-fluid">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-xs-12 col-lg-12 col-md-12">
					<div class="col-md-7 col-lg-7 col-xs-12 col-sm-12">
						<?php the_content(); ?>
					</div>
					<div class="col-md-5 col-lg-5 col-xs-12 col-sm-12">
						<img src="<?php echo $image[0] ?>" alt="about us" class="img-responsive">
					</div>
				</div>	
			</div>
		</div>
	</div>
</section>

<?php

get_footer();

?>