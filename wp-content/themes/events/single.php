<?php 

get_header('inner'); 

the_post();

$image = wp_get_attachment_image_src(get_post_thumbnail_id( get_the_id() ), 'full' );
?>

<a id="eltdf-back-to-top" href="#">
    <span class="eltdf-text-stack"> top </span>
</a>

<section class="inner-banner">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                    <h1><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="blog-block">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                    <div class="col-md-9 ">


                        <div class="eltdf-blog-holder eltdf-blog-centered eltdf-blog-pagination-standard" data-blog-type="centered" data-next-page="2" data-max-num-pages="2" data-post-number="6" data-excerpt-length="55">

                            <article id="post-9" class="eltdf-post-has-media post-9 post type-post status-publish format-standard has-post-thumbnail hentry category-dancing tag-bouquet tag-bride tag-cake">
                                <div class="eltdf-post-content">
                                    <div class="eltdf-post-heading">
                                        <div class="eltdf-post-image">
                                            <img src="<?php echo $image[0] ?>" class="attachment-full size-full wp-post-image" alt="<?php the_title(); ?>" />
                                        </div>
                                    </div>


                                    <div class="eltdf-post-text">
                                        <div class="eltdf-post-text-inner">
                                            <div class="eltdf-post-text-main">
                                                <h2 itemprop="name" class="entry-title eltdf-post-title">
                                                    <?php the_title(); ?>
                                                </h2>
                                                <div class="eltdf-post-info-category">
                                                    <?php the_category( get_the_id() ) ?>
                                                </div>
                                                <div class="eltdf-post-excerpt-holder">
                                                    <p itemprop="description" class="eltdf-post-excerpt">
                                                        <?php the_content(); ?>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="eltdf-post-info-bottom clearfix">
                                                <div class="eltdf-post-info-bottom-left">
                                                    <div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
                                                        <?php echo get_the_date(); ?>
                                                    </div>
                                                </div>
                                                <div class="eltdf-post-info-bottom-center">
                                                    <div class="eltdf-blog-share">
                                                        <div class="eltdf-social-share-holder eltdf-list">
                                                            <ul>
                                                                <li class="eltdf-twitter-share">
                                                                    <a target="_blank" class="eltdf-share-link" href="#">
                                                                        <span class="eltdf-social-network-icon social_facebook"></span>
                                                                    </a>
                                                                </li>
                                                                <li class="eltdf-pinterest-share">
                                                                    <a target="_blank" class="eltdf-share-link" href="#">
                                                                        <span class="eltdf-social-network-icon social_twitter"></span>
                                                                    </a>
                                                                </li>
                                                                <li class="eltdf-facebook-share">
                                                                    <a target="_blank" class="eltdf-share-link" href="#">
                                                                        <span class="eltdf-social-network-icon social_instagram"></span>
                                                                    </a>
                                                                </li>
                                                                <li class="eltdf-tumblr-share">
                                                                    <a target="_blank" class="eltdf-share-link" href="#">
                                                                        <span class="eltdf-social-network-icon social_pinterest"></span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
<!--                                                 <div class="eltdf-post-info-bottom-right">
                                                    <div class="eltdf-post-info-comments-holder">
                                                        <a itemprop="url" class="eltdf-post-info-comments" id="cmt-show"> Comments </a>
                                                    </div>
                                                </div> -->
                                            </div>

                                            <section class="cmt-section">
	                                            <div class="pt-5">
									              <h3 class="mb-5">Comments</h3>
									                <ul class="commentlist">
									                <?php
									                  $comments = get_comments(array(
									                    'post_id' => get_the_id(),
									                    'status'  => 'approve'
									                  ));

									                  wp_list_comments(array(
									                    'per_page'  => 10,
									                    'reverse_top_level' => false
									                  ),$comments);
									                ?>
									              </ul>

									              <?php comment_form(); ?>
									            </div>
								            </section>

                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>


                    </div>





















                    <div class="eltdf-sidebar-holder eltdf-grid-col-3">
                        <aside class="eltdf-sidebar">
                            <div class="widget eltdf-author-info-widget">
                                <div class="eltdf-aiw-inner">
                                    <a itemprop="url" class="eltdf-aiw-image" href="#">
                                        <img src="<?php bloginfo('template_url'); ?>/images/04-blog-autor-img-1.png" width="250" height="250" alt="Anne Lorren" class="avatar avatar-250 wp-user-avatar wp-user-avatar-250 alignnone photo" />
                                    </a>

                                    <p itemprop="description" class="eltdf-aiw-text">Wedding planner in love with photography and flower decorating. Blogger.</p>
                                </div>
                            </div>
                            <div class="widget eltdf-separator-widget">
                                <div class="eltdf-separator-holder clearfix eltdf-separator-center eltdf-separator-normal">
                                    <div class="eltdf-separator" style="border-style: solid; margin-top: -1px;"></div>
                                </div>
                            </div>
                            <div id="categories-3" class="widget widget_categories">
                                <div class="eltdf-widget-title-holder"><h5 class="eltdf-widget-title">Categories</h5></div>
                                <ul>
                                    <li class="cat-item cat-item-21"><a href="beauty.html">Beauty</a></li>
                                    <li class="cat-item cat-item-22"><a href="bijou-brides.html">Bijou Brides</a></li>
                                    <li class="cat-item cat-item-19"><a href="dancing.html">Dancing</a></li>
                                    <li class="cat-item cat-item-16"><a href="wedding.html">Wedding</a></li>
                                </ul>
                            </div>
                            <div class="widget eltdf-separator-widget">
                                <div class="eltdf-separator-holder clearfix eltdf-separator-center eltdf-separator-normal">
                                    <div class="eltdf-separator" style="border-style: solid; margin-top: 2px;"></div>
                                </div>
                            </div>
                            <div id="eltdf_instagram_widget-2" class="widget widget_eltdf_instagram_widget">
                                <div class="eltdf-widget-title-holder"><h5 class="eltdf-widget-title">Follow</h5></div>
                                <ul class="eltdf-instagram-feed clearfix eltdf-col-3 eltdf-instagram-gallery eltdf-tiny-space">
                                    <li>
                                        <a href="https://www.instagram.com/p/BiEqqTaDwIB/" target="_blank">
                                            <span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
                                            <img class="low_resolution" src="<?php bloginfo('template_url'); ?>/images/s320x320-30605393_603811343303045_6325435384654200832_n.jpg" alt="" width="320" height="320" />
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.instagram.com/p/BiEqongjq9I/" target="_blank">
                                            <span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
                                            <img class="low_resolution" src="<?php bloginfo('template_url'); ?>/images/s320x320-30602665_666976383634205_5416085766075842560_n.jpg" alt="" width="320" height="320" />
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.instagram.com/p/BiEqm-ajg7b/" target="_blank">
                                            <span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
                                            <img class="low_resolution" src="<?php bloginfo('template_url'); ?>/images/s320x320-31157005_993121947517406_5376739050986143744_n.jpg" alt="" width="320" height="320" />
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.instagram.com/p/BiEqjxDjmWR/" target="_blank">
                                            <span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
                                            <img class="low_resolution" src="<?php bloginfo('template_url'); ?>/images/s320x320-30601703_167995387231560_7542690190775549952_n.jpg" alt="" width="320" height="320" />
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.instagram.com/p/BiEqgIIjiTg/" target="_blank">
                                            <span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
                                            <img class="low_resolution" src="<?php bloginfo('template_url'); ?>/images/s320x320-30829389_190080938382728_8815398182943981568_n.jpg" alt="" width="320" height="320" />
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.instagram.com/p/BiEqeTMDEcT/" target="_blank">
                                            <span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
                                            <img class="low_resolution" src="<?php bloginfo('template_url'); ?>/images/s320x320-31118366_163054664369910_1345918763450499072_n.jpg" alt="" width="320" height="320" />
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.instagram.com/p/BiEqaMWj5iN/" target="_blank">
                                            <span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
                                            <img class="low_resolution" src="<?php bloginfo('template_url'); ?>/images/s320x320-30903722_171831546852478_5001505462189817856_n.jpg" alt="" width="320" height="320" />
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.instagram.com/p/BiEqX33DbFs/" target="_blank">
                                            <span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
                                            <img class="low_resolution" src="<?php bloginfo('template_url'); ?>/images/s320x320-30867878_197098437572789_9047513836415877120_n.jpg" alt="" width="320" height="320" />
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.instagram.com/p/BiEqUnODl3M/" target="_blank">
                                            <span class="eltdf-instagram-icon"><i class="social_instagram"></i></span>
                                            <img class="low_resolution" src="<?php bloginfo('template_url'); ?>/images/s320x320-30604239_466299773799687_8025880024529764352_n.jpg" alt="" width="320" height="320" />
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="widget eltdf-separator-widget">
                                <div class="eltdf-separator-holder clearfix eltdf-separator-center eltdf-separator-normal">
                                    <div class="eltdf-separator" style="border-style: solid; margin-top: 6px;"></div>
                                </div>
                            </div>
                            <div class="widget eltdf-blog-list-widget">
                                <div class="eltdf-widget-title-holder"><h5 class="eltdf-widget-title">Recent Posts</h5></div>
                                <div
                                    class="eltdf-blog-list-holder eltdf-grid-list eltdf-disable-bottom-space eltdf-bl-minimal eltdf-one-columns eltdf-large-space eltdf-bl-pag-no-pagination"
                                    data-type="minimal"
                                    data-number-of-posts="3"
                                    data-number-of-columns="one"
                                    data-space-between-items="large"
                                    data-orderby="date"
                                    data-order="ASC"
                                    data-image-size="thumbnail"
                                    data-title-tag="h5"
                                    data-excerpt-length="40"
                                    data-post-info-section="yes"
                                    data-post-info-image="yes"
                                    data-post-info-author="yes"
                                    data-post-info-date="yes"
                                    data-post-info-category="yes"
                                    data-post-info-comments="no"
                                    data-post-info-like="no"
                                    data-post-info-share="no"
                                    data-pagination-type="no-pagination"
                                    data-max-num-pages="4"
                                    data-next-page="2"
                                >
                                    <div class="eltdf-bl-wrapper eltdf-outer-space">
                                        <ul class="eltdf-blog-list">
                                            <li class="eltdf-bl-item eltdf-item-space clearfix">
                                                <div class="eltdf-bli-inner">
                                                    <div class="eltdf-bli-content">
                                                        <h5 itemprop="name" class="entry-title eltdf-post-title">
                                                            <a itemprop="url" href="spring-color-mood.html" title="Spring Color Mood"> Spring Color Mood </a>
                                                        </h5>
                                                        <div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
                                                            <a itemprop="url" href="05.html"> 10. May 2018. </a>
                                                            <meta itemprop="interactionCount" content="UserComments: 0" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="eltdf-bl-item eltdf-item-space clearfix">
                                                <div class="eltdf-bli-inner">
                                                    <div class="eltdf-bli-content">
                                                        <h5 itemprop="name" class="entry-title eltdf-post-title">
                                                            <a itemprop="url" href="double-wedding.html" title="Double Wedding"> Double Wedding </a>
                                                        </h5>
                                                        <div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
                                                            <a itemprop="url" href="05.html"> 10. May 2018. </a>
                                                            <meta itemprop="interactionCount" content="UserComments: 0" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="eltdf-bl-item eltdf-item-space clearfix">
                                                <div class="eltdf-bli-inner">
                                                    <div class="eltdf-bli-content">
                                                        <h5 itemprop="name" class="entry-title eltdf-post-title">
                                                            <a itemprop="url" href="handmade-gowns.html" title="Handmade Gowns"> Handmade Gowns </a>
                                                        </h5>
                                                        <div itemprop="dateCreated" class="eltdf-post-info-date entry-date published updated">
                                                            <a itemprop="url" href="05.html"> 10. May 2018. </a>
                                                            <meta itemprop="interactionCount" content="UserComments: 0" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="widget eltdf-separator-widget">
                                <div class="eltdf-separator-holder clearfix eltdf-separator-center eltdf-separator-normal">
                                    <div class="eltdf-separator" style="border-style: solid; margin-top: 34px;"></div>
                                </div>
                            </div>
                            <div id="tag_cloud-2" class="widget widget_tag_cloud">
                                <div class="eltdf-widget-title-holder"><h5 class="eltdf-widget-title">Tags</h5></div>
                                <div class="tagcloud">
                                    <a href="bouquet.html" class="tag-cloud-link tag-link-28 tag-link-position-1" style="font-size: 12pt;" aria-label="Bouquet (3 items)">Bouquet</a>
                                    <a href="bride.html" class="tag-cloud-link tag-link-26 tag-link-position-2" style="font-size: 22pt;" aria-label="Bride (7 items)">Bride</a>
                                    <a href="cake.html" class="tag-cloud-link tag-link-32 tag-link-position-3" style="font-size: 15.333333333333pt;" aria-label="Cake (4 items)">Cake</a>
                                    <a href="celebration.html" class="tag-cloud-link tag-link-33 tag-link-position-4" style="font-size: 8pt;" aria-label="Celebration (2 items)">Celebration</a>
                                    <a href="friends.html" class="tag-cloud-link tag-link-23 tag-link-position-5" style="font-size: 12pt;" aria-label="Friends (3 items)">Friends</a>
                                    <a href="groom.html" class="tag-cloud-link tag-link-27 tag-link-position-6" style="font-size: 18pt;" aria-label="Groom (5 items)">Groom</a>
                                    <a href="kids.html" class="tag-cloud-link tag-link-30 tag-link-position-7" style="font-size: 12pt;" aria-label="Kids (3 items)">Kids</a>
                                    <a href="love.html" class="tag-cloud-link tag-link-24 tag-link-position-8" style="font-size: 18pt;" aria-label="Love (5 items)">Love</a>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>

</section>
               
<?php get_footer(); ?>

<script type="text/javascript">
	
	$(document).ready(function(){
		// $('#cmt-show').toggle(function(){
		// 	$('.cmt-section').css('display','block'),
		// 	$('.cmt-section').css('display','none')
		// })
	})

</script>