<?php
/*
Plugin Name: User Events

Description: For User Events

*/


/**
 * Enqueue scripts and styles.
 */

/**
 * Property Plugin
 * Custom post type and category
 */
function staff_register3() {
	$labels = array(
		'name' 				 => _x('User Events', 'post type general name'),
		'singular_name' 	 => _x('User Events Info', 'post type singular name'),
		'edit_item' 		 => "Edit User Events ",
		'view_item' 		 => __('User Events'),
		'search_items' 		 => __('Search User Events'),
		'not_found' 		 =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon'  => ''
	);

	$args = array(
		'labels' 			 => $labels,
		'public' 			 => true,
		'publicly_queryable' => true,
		'show_ui' 			 => true,
		'query_var' 		 => true,
		'menu_icon' 		 => plugins_url() . '/news/images/custom_pin.png',
		'rewrite' 			 => true,
		'capability_type' 	 => 'post',
		'hierarchical' 		 => false,
		'menu_icon'           => 'dashicons-admin-post',
		'supports' 			 => array('title'),
		'menu_position' 	 => null,
		'has_archive'		=> true,
	  ); 

	register_post_type( 'user_events' , $args );
}
add_action('init', 'staff_register3');



/**
 * General Info.
 */
function staff_metaboxes() {
	add_meta_box('metabox-award', __('Users', 'user_events'), 'staff_metabox_award', 'user_events');
}
add_action('add_meta_boxes', 'staff_metaboxes');
function staff_metabox_award() {
	global $post;
	$post_id = $post->ID;
	$custom = get_post_custom($post->ID);
	
	echo '<div><select name="username" id="username" style="width: 100%;">';
	echo '<option value="">Select User</option>';
	
	$args1 = array(
	 'role' => 'subscriber',
	 'orderby' => 'user_nicename',
	 'order' => 'ASC'
	);
 	$subscribers = get_users($args1);
 	foreach ($subscribers as $user) {

	$selected = $user->ID == $custom['username'][0] ? " selected = 'selected'" : "";
	printf('<option value="%s"%s>%s</option>',$user->ID, $selected, $user->display_name);
	}
		echo '</select></div>';

}

function save_staff2(){
	global $post;
	
	update_post_meta($post->ID, 'username', $_POST['username']);
}
add_action('save_post', 'save_staff2');