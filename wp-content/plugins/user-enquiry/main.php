<?php
/*
Plugin Name: User Enquiry

Description: For User Enquiry

*/

function user_enquiry() {
	$labels = array(
		'name' 				 => _x('User Enquiry', 'post type general name'),
		'singular_name' 	 => _x('User Enquiry Info', 'post type singular name'),
		'edit_item' 		 => "Edit User Enquiry ",
		'view_item' 		 => __('User Enquiry'),
		'search_items' 		 => __('Search User Enquiry'),
		'not_found' 		 =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon'  => ''
	);

	$args = array(
		'labels' 			 => $labels,
		'public' 			 => true,
		'publicly_queryable' => true,
		'show_ui' 			 => true,
		'query_var' 		 => true,
		'menu_icon' 		 => plugins_url() . '/news/images/custom_pin.png',
		'rewrite' 			 => true,
		'capability_type' 	 => 'post',
		'hierarchical' 		 => false,
		'menu_icon'           => 'dashicons-admin-post',
		'supports' 			 => array('title','editor'),
		'menu_position' 	 => null,
		'has_archive'		=> true,
	  ); 

	register_post_type( 'user-enquiry' , $args );
}
add_action('init', 'user_enquiry');



/**
 * General Info.
 */
function user_metaboxes() {
	add_meta_box('metabox-award', __('Users Others Details', 'user-enquiry'), 'user_metabox_award', 'user-enquiry');
}
add_action('add_meta_boxes', 'user_metaboxes');
function user_metabox_award() {
	global $post;
	$post_id = $post->ID;
	$custom = get_post_custom($post->ID);
	?>
	<div style="width: 100%">
		<label>Phone Number</label>
		<div>
			<input type="text" name="number" value="<?php echo  $custom['number'][0] ?>" style="width: 100%;height: 40px" />
		</div>

		<label>Packages </label>
		<div>
			<input type="text" name="packages" value="<?php echo  $custom['packages'][0] ?>" style="width: 100%;height: 40px"  />
		</div>
	</div>


	<?php
	// echo '<div><select name="username" id="username" style="width: 100%;">';
	// echo '<option value="">Select User</option>';
	
	// $args1 = array(
	//  'role' => 'subscriber',
	//  'orderby' => 'user_nicename',
	//  'order' => 'ASC'
	// );
 // 	$subscribers = get_users($args1);
 // 	foreach ($subscribers as $user) {

	// $selected = $user->ID == $custom['username'][0] ? " selected = 'selected'" : "";
	// printf('<option value="%s"%s>%s</option>',$user->ID, $selected, $user->display_name);
	// }
	// 	echo '</select></div>';

}

function enquiry_save(){
	global $post;
	
	update_post_meta($post->ID, 'number', $_POST['number']);
	update_post_meta($post->ID, 'packages', $_POST['packages']);
}
add_action('save_post', 'enquiry_save');