<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'eventplanerinjodhpur' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '7UY7|t~RMtz]Lq`.3[wp :XG_se>p`lnF%u$ I$D-Yy>W-F^7kXHC^8hfP[z2|Ic' );
define( 'SECURE_AUTH_KEY',  'u[<Dd1%/twv5TkSUsID8F&Bdnjlu=ZiIRyN#;|%~SmdI5Z]k{qdk%k|1Ov`e{]C)' );
define( 'LOGGED_IN_KEY',    '39!oB[/R9aZP0o,^$k=~E$Ac(Sn 8<!)&|}^5D*ur5UDYAd7@,/OS3(z]fmm9X!q' );
define( 'NONCE_KEY',        '>DZR=i#- >*[:yt4yc<|+hAG;j8QYu&?BMG_AkLXBZc;DGY(M|_<%&{2&z5c^ rp' );
define( 'AUTH_SALT',        'rCi;LodJ|iQ$j3Z+L*{mR9C_n[~l%iF men/_^=E3[gfTOjWpa5BT6Cu$z;[[s)z' );
define( 'SECURE_AUTH_SALT', '`}}XR`2LqHVz5. R$0)KQo2hDb7oj]hVme-qe9?5A@~[`Ts|PK6F5EUYhJVw|QT#' );
define( 'LOGGED_IN_SALT',   '3~SlI;q!1P$5i~o%N16@C103)Au[OmmfC.J!*5R(`Wd[Wug3F| M}9rUG<6mA~[Z' );
define( 'NONCE_SALT',       'v/h`8jE3<zoFo.;=SWyy$tYV{z0yquC3s`]N)uY{!-}mY;hjB>UR(&P6{#T(RD:T' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
